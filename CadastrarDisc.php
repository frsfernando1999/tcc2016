<?php

require_once 'core/Disciplinas.class.php';
$disc = new Disciplinas();
$error = Array();
// Se o usuário clicou no botão cadastrar efetua as ações
if ($_POST['cadastrar']) {

    $nomeDisc = filter_input(INPUT_POST, 'nomeDisc', FILTER_SANITIZE_STRING);
    if ($_FILES['foto']) {
        $foto = $_FILES['foto'];
} 
    if($nomeDisc==''){
        $error[]='Preencha todos os campos';
        header('location:index.php?p=cadastrarDisciplina&erro=1');
    }

    $array = $disc->select("and nome_disc='$nomeDisc'");
    $total = count($array);
    if ($total !== 0) {
        $error[] = "Disciplina já cadastrada.";
        header('location:index.php?p=cadastrarDisciplina&erro=2');
    }
    if (!empty($foto["name"])){

        // Largura máxima em pixels

        // Verifica se o arquivo é uma imagem
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/", $foto["type"])) {
            $error[] = "Isso não é uma imagem.";
            header('location:index.php?p=cadastrarDisciplina&erro=3');
        }

        // Pega as dimensões da imagem
        $dimensoes = getimagesize($foto["tmp_name"]);

        // Se não houver nenhum erro
        if (count($error) == 0) {

            // Pega extensão da imagem
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);

            // Gera um nome único para a imagem
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];

            // Caminho de onde ficará a imagem
            $caminho_imagem = "imagem/" . $nome_imagem;

            // Faz o upload da imagem para seu respectivo caminho
            move_uploaded_file($foto["tmp_name"], $caminho_imagem);

            // Insere os dados no banco



    // Insere os dados no banco
    $disc->setNome_disc($nomeDisc);
    $disc->setImg_disc($caminho_imagem);
    $result = $disc->insert();
    if ($result) {
                header("location:index.php?p=areaAdm&sucesso=1");
    }
}}

if (count($error) != 0) {
    foreach ($error as $erro) {
        echo $erro . '<br/>';
    }
}
}


