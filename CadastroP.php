<?php

require_once 'core/Usuario.class.php';
$usuario = new Usuario();
$error = Array();
$erro = array();
// Se o usuário clicou no botão cadastrar efetua as ações
if ($_POST['cadastrar']) {

    // Recupera os dados dos campos
    $nome = filter_input(INPUT_POST, 'nomeP', FILTER_SANITIZE_STRING);
    $email = filter_input(INPUT_POST, 'emailP', FILTER_VALIDATE_EMAIL);
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    $sobrenome = filter_input(INPUT_POST, 'sobrenomeP', FILTER_SANITIZE_STRING);
    $senha = filter_input(INPUT_POST, 'senhaP', FILTER_SANITIZE_STRING);
    $dataNascimento = $_POST['dataNascP'];
    $acesso_usuario = 'P';
    $cpf = filter_input(INPUT_POST, 'cpfP', FILTER_SANITIZE_STRING);
    $tipoUsuario = '2';
    $ativoUsuario = TRUE;
    $sexo = $_POST['sexoP'];
    if ($_FILES['foto']) {
        $foto = $_FILES['foto'];
    }

    $array = $usuario->select("and email_usuario='$email'");
    $total = count($array);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $error[5] = "E-mail Inválido";
        header('location:frmLogin.php?erro=1');
    }

    if ($total !== 0) {
        $error[6] = "Email já cadastrado.";
        header('location:frmLogin.php?erro=7');
    }
    // Se a foto estiver sido selecionada
    if (!empty($foto["name"])) {

        // Largura máxima em pixels
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:frmLogin.php?erro=2');
        }
        // Se não houver nenhum erro
        if (count($error) == 0) {

            // Pega extensão da imagem
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);

            // Gera um nome único para a imagem
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];

            // Caminho de onde ficará a imagem
            $caminho_imagem = "fotos/" . $nome_imagem;

            // Faz o upload da imagem para seu respectivo caminho
            move_uploaded_file($foto["tmp_name"], $caminho_imagem);

            // Insere os dados no banco
            $usuario->setNome_usuario($nome);
            $usuario->setSobrenome_usuario($sobrenome);
            $usuario->setSenha_usuario($senha);
            $usuario->setUrl_foto_usuario("fotos/" . $nome_imagem);
            $usuario->setData_nasc_usuario($dataNascimento);
            $usuario->setAcesso_usuario($acesso_usuario);
            $usuario->setEmail_usuario($email);
            $usuario->setCpf_usuario($cpf);
            $usuario->setTipo_usuario($tipoUsuario);
            $usuario->setAtivo_usuario($ativoUsuario);
            $usuario->setSexo_usuario($sexo);
            $result = $usuario->insert();
            if ($result) {
                header("location:frmLogin.php?sucesso=1");
            }
        }
    } elseif (count($error) == 0) {
        if ($sexo == "Masculino") {
            $usuario->setNome_usuario($nome);
            $usuario->setSobrenome_usuario($sobrenome);
            $usuario->setSenha_usuario($senha);
            $usuario->setUrl_foto_usuario("fotos/perfilHdefault.png");
            $usuario->setData_nasc_usuario($dataNascimento);
            $usuario->setAcesso_usuario($acesso_usuario);
            $usuario->setEmail_usuario($email);
            $usuario->setCpf_usuario($cpf);
            $usuario->setTipo_usuario($tipoUsuario);
            $usuario->setAtivo_usuario($ativoUsuario);
            $usuario->setSexo_usuario($sexo);
            $result = $usuario->insert();
            header("location:frmLogin.php?sucesso=1");
        } elseif ($sexo == "Feminino") {
            $usuario->setNome_usuario($nome);
            $usuario->setSobrenome_usuario($sobrenome);
            $usuario->setSenha_usuario($senha);
            $usuario->setUrl_foto_usuario("fotos/perfilMdefault.png");
            $usuario->setData_nasc_usuario($dataNascimento);
            $usuario->setAcesso_usuario($acesso_usuario);
            $usuario->setEmail_usuario($email);
            $usuario->setCpf_usuario($cpf);
            $usuario->setTipo_usuario($tipoUsuario);
            $usuario->setAtivo_usuario($ativoUsuario);
            $usuario->setSexo_usuario($sexo);
            $result = $usuario->insert();
            header("location:frmLogin.php?sucesso=1");
        }
    }
    if (count($error) != 0) {
        foreach ($error as $erro) {
            echo $erro . "<br />";
        }
    }
}


