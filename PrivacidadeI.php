<!DOCTYPE html>
<html>
    <head>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="O Vestibulário é o site de estudos para vestibular que conta com professores de todo o Brasil, prontos para te ajudar a entrar na tão sonhada faculdade!">
        <meta name="keywords" content="vestibular, estudar, universidade, gratis, faculdade, materia, enem, fuvest, simulado, exercicios, curso, aprender, passar, conteudo, aula, disciplina, online">
        <meta name="author" content="Vanguart">
        <link rel="shortcut icon" href="imagem/fav-icon-vestibulario.png">
        <link href="css/css_reset.css" rel="stylesheet"/>
        <link rel="stylesheet" media="screen and (max-width:319px)" href="css/style_319px.css">
        <link rel="stylesheet" media="screen and (min-width:320px) and (max-width:479px)" href="css/style_320px.css">
        <link rel="stylesheet" media="screen and (min-width:480px) and (max-width:599px)" href="css/style_480px.css">
        <link rel="stylesheet" media="screen and (min-width:600px) and (max-width:767px)" href="css/style_600px.css">
        <link rel="stylesheet" media="screen and (min-width:768px) and (max-width:1023px)" href="css/style_768px.css">
        <link rel="stylesheet" media="screen and (min-width:1024px) and (max-width:1199px)" href="css/style_1024px.css">
        <link rel="stylesheet" media="screen and (min-width:1200px)" href="css/style_1200px.css"> 
        <script src="javascript/js.js"></script>
        <script src="javascript/jquery-3.1.0.js" type="text/javascript"></script>
        <script src="javascript/modal.js"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <script src="javascript/jquery.maskedinput.js" type="text/javascript"></script>
    </head>
    <body id="body2">
        <div id="container2">
            <a href="frmLogin.php"><img src="imagem/logoG.png" alt="Logo Vestibulario" id="responsaLogo"></a>
            <section id="espacoPrivI">
                <h2 class="tituloTermosI">Política de Privacidade</h2>    
    
                    <p class="textoTermosI">Nós, do Vestibulário, reconhecemos que a privacidade é importante. Essa política se aplica ao conteúdo e serviços oferecidos pelo Vestibulário. Ressaltamos que em serviços particulares, onde se faz necessário algumas informações mais detalhadas, nós postaremos avisos de privacidade separados para descrever como esses processam informações pessoais. Se você tiver qualquer dúvida acerca dos termos dessa Política de privacidade, por favor, não hesite em nos contatar através do e-mail da empresa vanguart.provedor@gmail.com.</p>
                    <p class="textoTermosI"><h4>1.0	Informação coletadas por nós.</h4>
                    Oferecemos acesso ao nosso conteúdo apenas para usuários cadastrados. Sendo assim coletaremos as seguintes informações:</p>
                    <p class="textoTermosI"><h4>2.0 Informação fornecida</h4>
                    Quando o usuário faz o cadastro em nosso site, nós solicitamos algumas informações pessoais como: nome, endereço de e-mail, senha da conta, CPF, escolaridade e outras informações complementares.</p>
                    <p class="textoTermosI"><h4>3.0 Comunicações do Usuário</h4>
                    Quando o usuário nos enviar um e-mail, será averiguado sua petição a fim de processar as suas solicitações e melhorar os nossos serviços.</p>
                   <p class="textoTermosI"> <h4>4.0 Outros sites</h4>
                    Essa política de privacidade se aplica ao Vestibulário e aos serviços que pertencem e são operados pelo site. Não exercemos controle sobre algumas publicidades exibidas, como por exemplo, link patrocinados fornecidos pela empresa Google Inc. E outros. Esses outros sites podem colocar seus próprios cookies ou outros arquivos no seu computador, coletar dados ou solicitar algum tipo de informação pessoal.
                    O Vestibulário apenas processa sua informação pessoal para os propósitos descritos na política de privacidade aplicável e avisos de privacidade para serviços específicos. Além do citado acima, tais propósitos incluem:
                    Fornecer nossos conteúdos e serviços aos usuários, incluindo a exibição alterada do conteúdo e propaganda;
                    Verificação, pesquisa e análise a fim de manter, proteger e melhorar nossos serviços;
                    Certificar-se do funcionamento técnico da nossa rede;
                    Desenvolver novos serviços.</p>
                    <p class="textoTermosI"><h4>5.0 Opções para informações pessoais</h4>
                    Ao se cadastrar no Vestibulário, pediremos certas informações pessoais, estas que serão utilizadas de forma que discordará de nossa Política de Privacidade. Portanto, solicitaremos que você concorde com tal uso, a fim de evitar complicações.</p>
                    <p class="textoTermosI"><h4>6.0 informações compartilhadas</h4>
                    Só será compartilhada informações pessoais com outras companhias ou indivíduos fora do nosso website nas condições conforme se seguem:
                    Quando tivermos seu consentimento. Requeremos a anuência opcional para compartilhar de qualquer informação pessoal.
                    Forneceremos as informações aos nossos websites associados ou negócios com a finalidade de processar as informações pessoais em nosso nome. Exigimos que os mesmos concordem em processar informações com base em nossas
                    É necessário que o acesso, utilização, preservação, afastamento ou revelação de tais informações seja feito para atender qualquer lei aplicável, regulamento, processo, legal ou solicitação factível governamental, (b) termos de serviço aplicável incluindo investigações de violação em potencial desse documento, (c) detectar, prevenir ou de outra forma endereços fraudulentos, segurança ou questões técnicas ou (d) proteger contra dados iminentes aos direitos, propriedades ou segurança do Vestibulário.</p>
                    <p class="textoTermosI"><h4>7.0 Alterações na política</h4>
                    A Política de Privacidade pode ser mudada a qualquer momento. Não reduziremos os seus direitos sob essa Política sem o consentimento explícito, esperando que essas mudanças sejam secundárias. Sendo assim, postaremos qualquer tipo de alteração nessa página, qualquer alteração a essa Política, se as alterações forem significativas, providenciaremos um aviso mais proeminente (por via de e-mails notificaremos as alterações).
                    Se houver qualquer dúvida em relação a nossa Política sugerimos que entre em contato conosco através do e-mail vanguart.provedor@gmail.com.</p>
            </section>
        </div>

        <footer id="rodape_fixo2">
            <a href="sobreNosI.php"><img src="imagem/logoV.png" id="img_rodape" width="80px" height="70px" alt="#"/></a>
            
            <a href="TermosI.php" class="link_rodape">Termos de uso</a>
            <a href="PrivacidadeI.php" class="link_rodape">Privacidade</a>
            <a href="sobreNosI.php" class="link_rodape">Sobre Nós</a>
        </footer>
    </body> 
</div>

</html>





		