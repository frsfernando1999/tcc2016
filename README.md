# Vestibulário

TCC do curso de Informática pra Internet feito em 2016.

O propósito deste projeto é criar um site de estudos para vestibulares com cadastro para professores e alunos.

### O site conta com:
1. Simulados de vestibulares;
2. Conteúdos de todas as principais matérias que são escritos pelos professores;
3. Exercicios de prática escritos pelos professores que podem ser dissertativos ou questões de multipla escolha;

### Como o site foi feito
- Foi usado MySql, um banco de dados relacional
- HTML, CSS e Javascript para interação e formatação das páginas
- PHP para a comunicação com o banco de dados 


