<!DOCTYPE html>
<html>
    <head>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="O Vestibulário é o site de estudos para vestibular que conta com professores de todo o Brasil, prontos para te ajudar a entrar na tão sonhada faculdade!">
        <meta name="keywords" content="vestibular, estudar, universidade, gratis, faculdade, materia, enem, fuvest, simulado, exercicios, curso, aprender, passar, conteudo, aula, disciplina, online">
        <meta name="author" content="Vanguart">
        <link rel="shortcut icon" href="imagem/fav-icon-vestibulario.png">
        <link href="css/css_reset.css" rel="stylesheet"/>
        <link rel="stylesheet" media="screen and (max-width:319px)" href="css/style_319px.css">
        <link rel="stylesheet" media="screen and (min-width:320px) and (max-width:479px)" href="css/style_320px.css">
        <link rel="stylesheet" media="screen and (min-width:480px) and (max-width:599px)" href="css/style_480px.css">
        <link rel="stylesheet" media="screen and (min-width:600px) and (max-width:767px)" href="css/style_600px.css">
        <link rel="stylesheet" media="screen and (min-width:768px) and (max-width:1023px)" href="css/style_768px.css">
        <link rel="stylesheet" media="screen and (min-width:1024px) and (max-width:1199px)" href="css/style_1024px.css">
        <link rel="stylesheet" media="screen and (min-width:1200px)" href="css/style_1200px.css"> 
        <script src="javascript/js.js"></script>
        <script src="javascript/jquery-3.1.0.js" type="text/javascript"></script>
        <script src="javascript/modal.js"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <script src="javascript/jquery.maskedinput.js" type="text/javascript"></script>
    </head>
    <body id="body2">
        <div id="container2">
            <a href="frmLogin.php"><img src="imagem/logoG.png" alt="Logo Vestibulario" id="responsaLogo"></a> 
            <section id="espacoTermosI">
                <h2 class="tituloTermosI">Termos de uso</h2>    
                
                <p class="textoTermosI"> <h4>1.0 Aceitações dos termos de uso.</h4>
                    O site www.vestibulario.com.br, operado pela empresa VANGUART, fornece ao usuário o acesso e uso dos serviços e conteúdos disponibilizados, que estão sujeitos à aceitação dos termos e condições estabelecidos neste documento.
                    Reservamos o direito de modificações e exclusões, temporariamente ou permanentes, com ou sem aviso prévio. Em caso de dúvidas entre em contato pelo e-mail vanguart.provedor@gmail.com.</p>
                   <p class="textoTermosI"> <h4>2.0 Modificações dos termos de uso.</h4>
                    Reservamos o direito de alterar a qualquer momento esse documento sem avisos prévios. As alterações que porventura sejam realizadas entrarão em vigor após a publicação nesta página. O seu acesso e utilização do conteúdo deste website será considerado um “concordar” dos novos termos de uso. Caso não concorde com alguma cláusula, entre em contato conosco.</p>
                   <p class="textoTermosI"> <h4>3.0 Propriedade intelectual.</h4>
                    O conteúdo geral do site Vestibulário pertence apenas à empresa Vanguart. Não é autorizado o plágio ou qualquer outro tipo de cópia. Caso o usuário não respeite as normas o mesmo receberá um processo, pertencente à lei 9.610 de 1998.
                    <h4>3.1 Licença de uso limitado:</h4>
                    <h4>3.1.1 Tutor:</h4>
                    O usuário/tutor terá permissão de fazer modificações no site se for relacionado a conteúdos, atividades e vídeo aulas, porém as postagens serão avaliadas pelo site e assim liberadas na rede;
                    <h4>3.1.2 Estudante:</h4>
                    O usuário/estudante só poderá ter acesso ao conteúdo fornecido pelo site ou tutores, não tendo então acesso a modificações.</p>
                  <p class="textoTermosI">  <h4>4. Marca:</h4>
                    O nome “Vestibulário” e logo está vinculado a empresa Vanguart, e não está autorizado a ser utilizado por terceiros, a menos que tenha algum vínculo direto com a empresa.</p>
                  <p class="textoTermosI"> <h4> 5. Violação de direitos autorais:</h4>
                    Se você acredita que qualquer conteúdo aqui disponibilizado viole direitos autorais seu e de terceiros, por favor, nos comunique imediatamente pelo endereço de e-mail vanguart.provedor@gmail.com para a averiguação e eventual remoção do conteúdo.</p>
                  <p class="textoTermosI">  <h4>6. Proibições:</h4>
                    Você não pode praticar as seguintes condutas ou utilizar o conteúdo desse site:
                    De forma que viole qualquer lei vigente no Brasil e no país de onde o site é acessado;
                    Enviar e-mails indesejados (“Spam”) em nome de nossa empresa Vanguart ou Vestibulário;
                    Se passar por funcionário da empresa seja qual for o motivo;
                    Fazer coleta de informações por meio de software automatizados;
                    Criação de perfis fakes (falsos);
                    Tentar obter acesso à uma parte restrita do site sem autorização;
                    Realizar ataques de negação de serviço contra qualquer servidor ou rede utilizada por esse site;
                    Não interferir no funcionamento do site.</p>
            </section>
        </div>

        <footer id="rodape_fixo2">
            <a href="sobreNosI.php"><img src="imagem/logoV.png" id="img_rodape" width="80px" height="70px" alt="#"/></a>

            <a href="TermosI.php" class="link_rodape">Termos de uso</a>
            <a href="PrivacidadeI.php" class="link_rodape">Privacidade</a>
            <a href="sobreNosI.php" class="link_rodape">Sobre Nós</a>
        </footer>
    </body> 
</div>

</html>

