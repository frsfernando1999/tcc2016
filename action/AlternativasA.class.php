<?php

require_once 'model/AlternativasM.class.php';

class AlternativasA extends AlternativasM{
    protected $sqlInsert = "insert into alternativas(alter_a, alter_b, alter_c, alter_d, alter_e, cod_resp, cod_questao)
                                                 values('%s', '%s', '%s', '%s', '%s', '%s', '%s')";
    
    protected $sqlSelect = "select * from alternativas where 1=1 %s %s";
    
    protected $sqlUpdate = "update alternativas set alter_a='%s', alter_b='%s', alter_c='%s', alter_d='%s', alter_e='%s', cod_resp='%s', cod_questao='%s'
                                                 where cod_alter='%s'";
    
    protected $sqlDelete = "delete from alternativas where cod_alter='%s'"; 

    public function insert(){
        $sql = sprintf($this->sqlInsert,
        $this->getAlter_a(),
        $this->getAlter_b(),
        $this->getAlter_c(),
        $this->getAlter_d(),
        $this->getAlter_e(),
        $this->getCod_resp(),
        $this->getCod_questao());
        return $this->runQuery($sql);
    }
    
    public function update(){
    $sql=  sprintf($this->sqlUpdate,
        $this->getAlter_a(),
        $this->getAlter_b(),
        $this->getAlter_c(),
        $this->getAlter_d(),
        $this->getAlter_e(),
        $this->getCod_resp(),
        $this->getCod_questao(),
        $this->getCod_alter());
        return $this->runQuery($sql);
}
        public function delete(){
        $sql=  sprintf($this->sqlDelete,
        $this->getCod_alter());
        return $this->runQuery($sql);
}
        public function select($where='', $order='') {
        $sql = sprintf($this->sqlSelect,$where,$order);
        return $this->runSelect($sql); 
        
    }
}
