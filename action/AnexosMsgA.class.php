<?php

require_once 'model/AnexosMsgM.class.php';

class AnexosMsgA extends AnexosMsgM{
    protected $sqlInsert = "insert into anexos_msg(data_msg,cod_msg,url_anexo_msg,titulo_anexo)
                                                 values('%s', '%s', '%s', '%s')";
    
    protected $sqlSelect = "select * from anexos_msg where 1=1 %s %s";
    
    protected $sqlUpdate = "update anexos_msg set cod_msg='%s',  url_anexo_msg='%s', titulo_anexo='%s'
                                                 where cod_anexo_msg='%s'";
    
    protected $sqlDelete = "delete from anexos_msg where cod_anexo_msg='%s'"; 

    public function insert(){
        $sql = sprintf($this->sqlInsert,
        $this->getData_msg(TRUE),
        $this->getCod_msg(),
        $this->getUrl_anexo_msg(),
        $this->getTitulo_anexo());
        return $this->runQuery($sql);
    }
    
    public function update(){
    $sql=  sprintf($this->sqlUpdate,
        $this->getCod_msg(),
        $this->getUrl_anexo_msg(),
        $this->getTitulo_anexo(),
        $this->getCod_anexo_msg());
        return $this->runQuery($sql);
}
        public function delete(){
        $sql=  sprintf($this->sqlDelete,
        $this->getCod_anexo_msg());
        return $this->runQuery($sql);
}
        public function select($where='', $order='') {
        $sql = sprintf($this->sqlSelect,$where,$order);
        return $this->runSelect($sql); 
        
    }
}

