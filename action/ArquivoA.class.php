<?php

require_once 'model/ArquivoM.class.php';

class ArquivoA extends ArquivoM{    
   protected $sqlInsert = "insert into arquivo(url_arq, legenda_arq, tipo_arq, topico_disciplina_cod_topico)
                                                 values('%s', '%s', '%s', '%s')";
    
    protected $sqlSelect = "select * from arquivo where 1=1 %s %s";
    
    protected $sqlUpdate = "update arquivo set url_arq='%s', legenda_arq='%s', tipo_arq='%s',topico_disciplina_cod_topico='%s'
                                                 where cod_arq='%s'";
    
    protected $sqlDelete = "delete from arquivo where cod_arq='%s'"; 

    public function insert(){
        $sql = sprintf($this->sqlInsert,
        $this->getUrl_arq(),
        $this->getLegenda_arq(),
        $this->getTipo_arq(),
        $this->getTopico_disciplina_cod_topico());
        return $this->runQuery($sql);
    }
    
    public function update(){
    $sql=  sprintf($this->sqlUpdate,
        $this->getUrl_arq(),
        $this->getLegenda_arq(),
        $this->getTipo_arq(),
        $this->getTopico_disciplina_cod_topico(),
        $this->getCod_arq());
        return $this->runQuery($sql);
}
        public function delete(){
        $sql=  sprintf($this->sqlDelete,
        $this->getCod_arq());
        return $this->runQuery($sql);
}
        public function select($where='', $order='') {
        $sql = sprintf($this->sqlSelect,$where,$order);
        return $this->runSelect($sql); 
        
    }
}
