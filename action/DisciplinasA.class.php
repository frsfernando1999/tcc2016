<?php

require_once 'model/DisciplinasM.class.php';

class DisciplinasA extends DisciplinasM{
    protected $sqlInsert = "insert into disciplinas(nome_disc, img_disc)
                                                 values('%s','%s')";
    
    protected $sqlSelect = "select * from disciplinas where 1=1 %s %s";
    
    protected $sqlUpdate = "update disciplinas set nome_disc='%s', img_disc='%s' 
                                                 where cod_disc='%s'";
    
    protected $sqlDelete = "delete from disciplinas where cod_disc='%s'"; 

    public function insert(){
        $sql = sprintf($this->sqlInsert,
        $this->getNome_disc(),
        $this->getImg_disc());
        return $this->runQuery($sql);
    }
    
    public function update(){
    $sql=  sprintf($this->sqlUpdate,
        $this->getNome_disc(),
        $this->getImg_disc(),
        $this->getCod_disc());
        return $this->runQuery($sql);
}
        public function delete(){
        $sql=  sprintf($this->sqlDelete,
        $this->getCod_disc());
        return $this->runQuery($sql);
}
        public function select($where='', $order='') {
        $sql = sprintf($this->sqlSelect,$where,$order);
        return $this->runSelect($sql); 
        
    }
}

