<?php

require_once 'model/ImagemM.class.php';

class ImagemA extends ImagemM{
    protected $sqlInsert = "insert into imagem(url_arq, legenda_arq, cod_questao)
                                                 values('%s', '%s', '%s')";
    
    protected $sqlSelect = "select * from imagem where 1=1 %s %s";
    
    protected $sqlUpdate = "update imagem set url_arq='%s', legenda_arq='%s', cod_questao='%s' 
                                                 where cod_img='%s'";
    
    protected $sqlDelete = "delete from imagem where cod_img='%s'"; 

    public function insert(){
        $sql = sprintf($this->sqlInsert,
        $this->getUrl_arq(),
        $this->getLegenda_arq(),
        $this->getCod_questao());
        return $this->runQuery($sql);
    }
    
    public function update(){
    $sql=  sprintf($this->sqlUpdate,
        $this->getUrl_arq(),
        $this->getLegenda_arq(),
        $this->getCod_questao(),
        $this->getCod_img());
        return $this->runQuery($sql);
}
        public function delete(){
        $sql=  sprintf($this->sqlDelete,
        $this->getCod_img());
        return $this->runQuery($sql);
}
        public function select($where='', $order='') {
        $sql = sprintf($this->sqlSelect,$where,$order);
        return $this->runSelect($sql); 
        
    }
}
