<?php

require_once 'model/ListaExerciciosM.class.php';

class ListaExerciciosA extends ListaExerciciosM{
    protected $sqlInsert = "insert into lista_exercicios (titulo_lista, status_exclusao_lista, data_inclusao_lista ,tipo_lista,default_lista, cod_usuario, cod_topico)
                                                 values('%s','%s','%s','%s','%s','%s','%s')";
    
    protected $sqlSelect = "select disciplinas.*,
             lista_exercicios.*
             from disciplinas INNER JOIN lista_exercicios where 1=1 %s %s";
    
    protected $sqlUpdate = "update lista_exercicios set titulo_lista='%s', status_exclusao_lista='%s', data_inclusao_lista='%s',tipo_lista='%s',default_lista='%s', cod_usuario='%s', cod_topico='%s' 
                                                 where cod_lista='%s'";
    
    protected $sqlDelete = "delete from lista_exercicios where cod_lista='%s'"; 
    
    protected $sqlDesativarLista = "update lista_exercicios set status_exclusao_lista='%s' 
                                                 where cod_lista='%s'";
    
    
    
    
 protected $sqlSelectInner = 
            "SELECT 
    usuario.nome_usuario,usuario.sobrenome_usuario,
    lista_exercicios.cod_lista,lista_exercicios.titulo_lista,lista_exercicios.tipo_lista,lista_exercicios.default_lista,lista_exercicios.status_exclusao_lista,lista_exercicios.data_inclusao_lista,
    topico_disciplina.nome_topico,topico_disciplina.cod_topico,
    ramo_disciplina.nome_ramo,ramo_disciplina.cod_ramo,
    disciplinas.nome_disc,disciplinas.cod_disc 
    FROM lista_exercicios INNER JOIN topico_disciplina ON(lista_exercicios.cod_topico = topico_disciplina.cod_topico) INNER JOIN usuario ON(lista_exercicios.cod_usuario=usuario.cod_usuario) INNER JOIN ramo_disciplina ON(topico_disciplina.cod_ramo = ramo_disciplina.cod_ramo) INNER JOIN disciplinas ON(ramo_disciplina.disciplinas_cod_disc=disciplinas.cod_disc) WHERE 1=1 %s %s;";

 protected $sqlSelectQuestoes = 
            "SELECT 
    questoes.cod_questao
    FROM questoes INNER JOIN lista_exercicios ON(lista_exercicios.cod_lista = questoes.lista_exercicios_cod_lista) WHERE 1=1 %s %s;";

    public function insert(){
        $sql = sprintf($this->sqlInsert,
        $this->getTitulo_lista(),
        $this->getStatus_exclusao_lista(),
        $this->getData_inclusao_lista(),
        $this->getTipo_lista(),
        $this->getDefault_lista(),
        $this->getCod_usuario(),
        $this->getCod_topico());
        return $this->runQuery($sql);
    }
    
    public function update(){
    $sql=  sprintf($this->sqlUpdate,
        $this->getTitulo_lista(),
        $this->getStatus_exclusao_lista(),
        $this->getData_inclusao_lista(),
        $this->getTipo_lista(),
        $this->getDefault_lista(),
        $this->getCod_usuario(),
        $this->getCod_topico(),
        $this->getCod_lista());
        return $this->runQuery($sql);
}
        public function delete(){
        $sql=  sprintf($this->sqlDelete,
        $this->getCod_lista());
        return $this->runQuery($sql);
}
        public function desativarLista(){
        $sql=  sprintf($this->sqlDesativarLista,
        $this->getStatus_exclusao_lista(),
        $this->getCod_lista());
        return $this->runQuery($sql);
}

        public function select($where='', $order='') {
        $sql = sprintf($this->sqlSelect,$where,$order);
        return $this->runSelect($sql); 
        
    }
        public function selectQuestoes($where='', $order='') {
        $sql = sprintf($this->sqlSelectQuestoes,$where,$order);
        return $this->runSelect($sql); 
        
    }

    public function selectInner($where='', $order='') {
        $sql = sprintf($this->sqlSelectInner,$where,$order);
        return $this->runSelect($sql); 
        
    }
}
