<?php

require_once 'model/MensagensM.class.php';

class MensagensA extends MensagensM{
    protected $sqlInsert = "insert into mensagens(data_msg, cod_msg, status_exclusao_msg, titulo_msg, texto_msg, cod_usuario_orig, cod_usuario_dest)
                                                 values('%s', '%s', '%s','%s', '%s', '%s', '%s')";
    
    protected $sqlSelect = "select * from mensagens where 1=1 %s %s";
    
    protected $sqlUpdate = "update mensagens set cod_msg='%s', status_exclusao_msg='%s', titulo_msg='%s', texto_msg='%s', cod_usuario_orig='%s', cod_usuario_dest='%s'
                                                 where data_msg='%s'";
    
    protected $sqlDelete = "delete from mensagens where cod_msg='%s'"; 

    public function insert(){
        $sql = sprintf($this->sqlInsert,
        $this->getData_msg(TRUE),
        $this->getCod_msg(),
        $this->getStatus_exclusao_msg(),
        $this->getTitulo_msg(),
        $this->getTexto_msg(),
        $this->getCod_usuario_orig(),
        $this->getCod_usuario_dest());
        return $this->runQuery($sql);
    }
    
    public function update(){
    $sql=  sprintf($this->sqlUpdate,
        $this->getCod_msg(),
        $this->getStatus_exclusao_msg(),
        $this->getTitulo_msg(),
        $this->getTexto_msg(),
        $this->getCod_usuario_orig(),
        $this->getCod_usuario_dest(),
        $this->getData_msg(TRUE));
        return $this->runQuery($sql);
}
        public function delete(){
        $sql=  sprintf($this->sqlDelete,
        $this->getCod_msg());
        return $this->runQuery($sql);
}
        public function select($where='', $order='') {
        $sql = sprintf($this->sqlSelect,$where,$order);
        return $this->runSelect($sql); 
        
    }
}
