<?php

require_once 'model/QuestoesM.class.php';

class QuestoesA extends QuestoesM{
    
    protected $sqlInsert = "insert into questoes (enunciado_ex_obj, obs_questao, lista_exercicios_cod_lista, img_questao)
                                                 values('%s','%s','%s','%s')";
        
    protected $sqlUpdate = "update questoes set enunciado_ex_obj='%s', obs_questao='%s' lista_exercicios_cod_lista='%s'
                                                 where cod_questao='%s'";
    
    protected $sqlDelete = "delete from questoes where cod_questao='%s'"; 
    
    protected $sqlSelectCod = "SELECT max(cod_questao) as id FROM questoes";

    protected $sqlSelect = "select 
            questoes.*,
            alternativas.*,
            lista_exercicios.* 
            from questoes
            INNER JOIN
            alternativas ON(questoes.cod_questao=alternativas.cod_questao)
            INNER JOIN 
            lista_exercicios ON(questoes.lista_exercicios_cod_lista=lista_exercicios.cod_lista)  
            where 1=1 %s %s";
    
    protected $sqlSelectD = "select 
            questoes.*,
            lista_exercicios.* 
            from questoes
            INNER JOIN 
            lista_exercicios ON(questoes.lista_exercicios_cod_lista=lista_exercicios.cod_lista)  
            where 1=1 %s %s";

    public function insert(){
        $sql = sprintf($this->sqlInsert,
        $this->getEnunciado_ex_obj(),
        $this->getObs_questao(),
        $this->getLista_exercicios_cod_lista(),
        $this->getImg_questao());
        return $this->runQuery($sql);
    }
    
    public function update(){
    $sql=  sprintf($this->sqlUpdate,
        $this->getEnunciado_ex_obj(),
        $this->getObs_questao(),
        $this->getLista_exercicios_cod_lista(),
        $this->getCod_questao());
        return $this->runQuery($sql);
}
        public function delete(){
        $sql=  sprintf($this->sqlDelete,
        $this->getCod_questao());
        return $this->runQuery($sql);
}
        public function select($where='', $order='') {
        $sql = sprintf($this->sqlSelect,$where,$order);
        return $this->runSelect($sql); 
        
    }
        public function selectCod($where='', $order='') {
        $sql = sprintf($this->sqlSelectCod,$where,$order);
        return $this->runSelect($sql); 
        
    }
        public function selectD($where='', $order='') {
        $sql = sprintf($this->sqlSelectD,$where,$order);
        return $this->runSelect($sql); 
        
    }
}


