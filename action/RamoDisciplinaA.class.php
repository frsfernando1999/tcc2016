<?php

require_once 'model/RamoDisciplinaM.class.php';

class RamoDisciplinaA extends RamoDisciplinaM{

    protected $sqlInsert = "insert into ramo_disciplina(nome_ramo, disciplinas_cod_disc)
                                                 values('%s', '%s')";
    
    protected $sqlSelect = "select * from ramo_disciplina where 1=1 %s %s";
    
    protected $sqlUpdate = "update ramo_disciplina set nome_ramo='%s', disciplinas_cod_disc='%s'
                                                 where cod_ramo='%s'";
    
    protected $sqlDelete = "delete from ramo_disciplina where cod_ramo='%s'"; 
    
    protected $sqlSelectInner = "select disciplinas.nome_disc,disciplinas.cod_disc from disciplinas where 1=1 %s %s";

    public function insert(){
        $sql = sprintf($this->sqlInsert,
        $this->getNome_ramo(),
        $this->getDisciplinas_cod_disc());
        return $this->runQuery($sql);
    }
    
    public function update(){
    $sql=  sprintf($this->sqlUpdate,
        $this->getNome_ramo(),
        $this->getDisciplinas_cod_disc(),
        $this->getCod_ramo());
        return $this->runQuery($sql);
}
        public function delete(){
        $sql=  sprintf($this->sqlDelete,
        $this->getCod_ramo());
        return $this->runQuery($sql);
}
        public function select($where='', $order='') {
        $sql = sprintf($this->sqlSelect,$where,$order);
        return $this->runSelect($sql); 
        
    }
        public function selectInner($where='', $order=''){
        $sql = sprintf($this->sqlSelectInner,$where,$order);
        return $this->runSelect($sql);       
    } 

        }

