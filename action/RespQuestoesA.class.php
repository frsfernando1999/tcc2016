<?php

require_once 'model/RespQuestoesM.class.php';

class RespQuestoesA extends RespQuestoesM{
      
    protected $sqlInsert = "insert into resp_questoes(cod_usuario, cod_questao, resp_user, data_resp_user)
                                                values('%s','%s','%s','%s')";
    
    protected $sqlSelect = "select resp_questoes.*,
        questoes.*,
        alternativas.*,
        lista_exercicios.*
        from resp_questoes INNER JOIN questoes ON (resp_questoes.cod_questao = questoes.cod_questao)
        INNER JOIN alternativas ON (alternativas.cod_questao=questoes.cod_questao)
        INNER JOIN lista_exercicios ON (questoes.lista_exercicios_cod_lista = lista_exercicios.cod_lista)
        where 1=1 %s %s";
    
    protected $selectExDiss = "select resp_questoes.*,
        questoes.*
        from resp_questoes INNER JOIN questoes ON (resp_questoes.cod_questao = questoes.cod_questao)
        where 1=1 %s %s";
    
    protected $sqlUpdate = "update resp_questoes set resp_user='%s', data_resp_user='%s'
                                                 where cod_questao='%s' and cod_usuario='%s'";
    
    protected $sqlDelete = "delete from resp_questoes where cod_usuario='%s'"; 

    public function insert(){
        $sql = sprintf($this->sqlInsert,
        $this->getCod_usuario(),
        $this->getCod_questao(),
        $this->getResp_user(),
        $this->getData_resp_user());
        return $this->runQuery($sql);
    }
    
    public function update(){
    $sql=  sprintf($this->sqlUpdate,
        $this->getResp_user(),
        $this->getData_resp_user(),
        $this->getCod_questao(),
        $this->getCod_usuario());
        return $this->runQuery($sql);
}
        public function delete(){
        $sql=  sprintf($this->sqlDelete,
        $this->getCod_usuario());
        return $this->runQuery($sql);
}
        public function select($where='', $order='') {
        $sql = sprintf($this->sqlSelect,$where,$order);
        return $this->runSelect($sql); 
        
    }
    
        public function selectExDiss($where='', $order='') {
        $sql = sprintf($this->selectExDiss,$where,$order);
        return $this->runSelect($sql); 
        
    }
}
