<?php

require_once 'model/TopicoDisciplinaM.class.php';

class TopicoDisciplinaA extends TopicoDisciplinaM {

    protected $sqlInsert = "insert into topico_disciplina (nome_topico,texto_topico,cod_ramo,cod_usuario,apresentacao_topico)
                                                 values('%s','%s','%s','%s','%s')";
    protected $sqlSelect = "select * from topico_disciplina where 1=1 %s %s";
    protected $sqlSelectRamo = "select disciplinas.*,
            ramo_disciplina.*
            from ramo_disciplina INNER JOIN disciplinas ON (ramo_disciplina.disciplinas_cod_disc=disciplinas.cod_disc) where 1=1 %s %s";
    
    protected $sqlUpdate = "update topico_disciplina set nome_topico='%s', texto_topico='%s', cod_ramo='%s', cod_usuario='%s', apresentacao_topico='%s'
                                                 where cod_topico='%s'";
    protected $sqlDelete = "delete from topico_disciplina where cod_topico='%s'";
    protected $sqlSelectInner = 
            "SELECT 
    disciplinas.cod_disc,
    ramo_disciplina.disciplinas_cod_disc,ramo_disciplina.cod_ramo,ramo_disciplina.nome_ramo,
    topico_disciplina.cod_topico,topico_disciplina.cod_ramo,topico_disciplina.nome_topico,topico_disciplina.texto_topico,topico_disciplina.apresentacao_topico,
    arquivo.url_arq,arquivo.legenda_arq,arquivo.topico_disciplina_cod_topico
    FROM
    disciplinas
        INNER JOIN
    ramo_disciplina ON(disciplinas.cod_disc = ramo_disciplina.disciplinas_cod_disc)
        INNER JOIN
    topico_disciplina ON(ramo_disciplina.cod_ramo = topico_disciplina.cod_ramo)
        INNER JOIN
    arquivo ON(topico_disciplina.cod_topico=arquivo.topico_disciplina_cod_topico)
    WHERE 
     1=1 %s %s;";

    public function insert() {
        $sql = sprintf($this->sqlInsert, 
               $this->getNome_topico(), 
               $this->getTexto_topico(), 
               $this->getCod_ramo(), 
               $this->getCod_usuario(), 
               $this->getApresentacao_topico());
               return $this->runQuery($sql);
    }

    public function update() {
        $sql = sprintf($this->sqlUpdate, 
               $this->getNome_topico(), 
               $this->getTexto_topico(),
               $this->getCod_ramo(), 
               $this->getCod_usuario(),
               $this->getApresentacao_topico(),
               $this->getCod_topico());
               return $this->runQuery($sql);
    }

    public function delete() {
        $sql = sprintf($this->sqlDelete, 
                $this->getCod_topico());
                return $this->runQuery($sql);
    }

    public function select($where = '', $order = '') {
        $sql = sprintf($this->sqlSelect, $where, $order);
        return $this->runSelect($sql);
    }
    public function selectInner($where = '', $order = '') {
        $sql = sprintf($this->sqlSelectInner, $where, $order);
        return $this->runSelect($sql);
    }
    public function selectRamo($where = '', $order = '') {
        $sql = sprintf($this->sqlSelectRamo, $where, $order);
        return $this->runSelect($sql);
    }

}
