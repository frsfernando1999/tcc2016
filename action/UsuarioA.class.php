<?php

require_once 'model/UsuarioM.class.php';

class UsuarioA extends UsuarioM{
    
    protected $sqlInsert = "insert into usuario (nome_usuario, sobrenome_usuario, senha_usuario, url_foto_usuario, data_nasc_usuario, acesso_usuario, email_usuario, cpf_usuario, tipo_usuario, ativo_usuario, sexo_usuario)
                                                 values('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')";
    
    protected $sqlSelect = "select * from usuario where 1=1 %s %s";
    
    protected $sqlUpdate = "update usuario set nome_usuario='%s',sobrenome_usuario='%s',senha_usuario='%s',url_foto_usuario='%s',acesso_usuario='%s',email_usuario='%s',cpf_usuario='%s',tipo_usuario='%s',ativo_usuario='%s',sexo_usuario='%s'
                                                 where cod_usuario='%s'";
    protected $sqlUpdateSenha = "update usuario set senha_usuario='%s'
                                                 where email_usuario='%s'";
    
    protected $sqlDesativarUsuario = "update usuario set ativo_usuario='%s'
                                                 where cod_usuario='%s'";
    
    protected $sqlUpdateSenhaPerfil = "update usuario set senha_usuario='%s'
                                                 where cod_usuario='%s'";
    
    protected $sqlUpdateFotoPerfil = "update usuario set url_foto_usuario='%s'
                                                 where cod_usuario='%s'";
    
    protected $sqlDelete = "delete from usuario where cod_usuario='%s'"; 
    
    protected $sqlSelectPaginasPermitidas = "select tipo_usuario from usuario where 1=1 %s %s";
    
    protected $sqlUpdatePerfil = "update usuario set nome_usuario='%s', sobrenome_usuario='%s', sexo_usuario='%s', data_nasc_usuario='%s' 
            where cod_usuario='%s'";

    public function insert(){
        $sql = sprintf($this->sqlInsert,
        $this->getNome_usuario(),
        $this->getSobrenome_usuario(),
        $this->getSenha_usuario(),
        $this->getUrl_foto_usuario(),       
        $this->getData_nasc_usuario(),
        $this->getAcesso_usuario(),
        $this->getEmail_usuario(),
        $this->getCpf_usuario(),
        $this->getTipo_usuario(),
        $this->getAtivo_usuario(),
        $this->getSexo_usuario());
        return $this->runQuery($sql);
    }
    
    public function update(){
    $sql=  sprintf($this->sqlUpdate,
        $this->getNome_usuario(),
        $this->getSobrenome_usuario(),
        $this->getSenha_usuario(),
        $this->getUrl_foto_usuario(),       
        $this->getData_nasc_usuario(),
        $this->getAcesso_usuario(),
        $this->getEmail_usuario(),
        $this->getCpf_usuario(),
        $this->getTipo_usuario(),
        $this->getAtivo_usuario(),
        $this->getSexo_usuario(),
        $this->getCod_usuario());
        return $this->runQuery($sql);
}
        public function delete(){
        $sql=  sprintf($this->sqlDelete,
        $this->getCod_usuario());
        return $this->runQuery($sql);
}
        public function select($where='', $order='') {
        $sql = sprintf($this->sqlSelect,$where,$order);
        return $this->runSelect($sql); 
        
    }
        public function updateSenha(){
        $sql=  sprintf($this->sqlUpdateSenha,
        $this->getSenha_usuario(),
        $this->getEmail_usuario());
        return $this->runQuery($sql);
}
        public function desativarUsuario(){
        $sql=  sprintf($this->sqlDesativarUsuario,
        $this->getAtivo_usuario(),
        $this->getCod_usuario());
        return $this->runQuery($sql);
}
        public function updateSenhaPerfil(){
        $sql=  sprintf($this->sqlUpdateSenhaPerfil,
        $this->getSenha_usuario(),
        $this->getCod_usuario());
        return $this->runQuery($sql);
}
        public function updateImgPerfil(){
        $sql=  sprintf($this->sqlUpdateFotoPerfil,
        $this->getUrl_foto_usuario(),       
        $this->getCod_usuario());
        return $this->runQuery($sql);
}
        public function selectPaginasPermitidas(){
        $sql=  sprintf($this->sqlSelectPaginasPermitidas,
        $this->getTipo_usuario());
        return $this->runQuery($sql);
}

        public function updatePerfil(){
        $sql=  sprintf($this->sqlUpdatePerfil,
        $this->getNome_usuario(),
        $this->getSobrenome_usuario(),
        $this->getSexo_usuario(),
        $this->getData_nasc_usuario(),
        $this->getCod_usuario());
        return $this->runQuery($sql);

        }
}


