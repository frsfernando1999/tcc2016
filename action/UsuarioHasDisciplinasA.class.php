<?php

require_once 'model/UsuarioHasDisciplinasM.class.php';

class UsuarioHasDisciplinasA extends UsuarioHasDisciplinasM{
    protected $sqlInsert = "insert into usuario_has_disciplinas(cod_usuario,cod_disc)
                                                    values('%s','%s')";
    
    protected $sqlSelect = "select * from usuario_has_disciplinas where 1=1 %s %s";
    
    // protected $sqlUpdate = "update usuario_has_tipo_usuario set ='%s',descricao_tipo_usuario='%s'
                           // where cod_tipo_usuario='%s'";
    
     protected $sqlDelete = "delete from usuario_has_disciplinas where cod_usuario='%s'"
             . "and cod_disc='%s'"; 
    
     protected $sqlSelectInner = "select usuario.email_usuario, disciplinas.nome_disc, usuario_has_disciplinas.*
             from usuario_has_disciplinas inner join usuario
             on (usuario_has_disciplinas.cod_usuario=usuario.cod_usuario)
             inner join tipo_usuario
             on(usuario_has_disciplinas.cod_disc = disciplinas.cod_disc)
             where 1=1 %s %s";
     
        public function insert(){
        $sql = sprintf($this->sqlInsert,
        $this->getCod_usuario(),
        $this->getCod_disc());
        return $this->runQuery($sql);
    }
    
    
        public function select() {
        $sql = spritf($this->sqlselect,$where,$order);
        return $this->runSelect($sql); 
        
    }   public function selectInner($where='',$order=''){
        $sql = sprintf($this->sqlSelectInner,$where,$order);
        return $this->runSelect($sql);
    }

       /* public function update() {
        $sql = sprintf($this->sqlUpdate,
        $this->getNomeTipoUsuario(),
        $this->getDescricaoTipoUsuario());
        return $this->runQuery($sql);
    }
    */
        public function delete() {
        $sql = sprintf($this->sqlDelete,
        $this->getCod_usuario(),
        $this->getCod_disc());        
        return $this->runQuery($sql);
    }
}
