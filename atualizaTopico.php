   <?php
    session_start();
    require_once 'core/TopicoDisciplina.class.php';
    require_once 'core/Arquivo.class.php';
    $error=array();
    $arquivo=new Arquivo();
    $topico=new TopicoDisciplina();
    
    $codUsuario = $_SESSION['cod_usuario'];
    $nomeTopico = filter_input(INPUT_POST, 'nomeTopico', FILTER_SANITIZE_STRING);
    $topicodisc = filter_input(INPUT_GET, 'topico', FILTER_SANITIZE_STRING);
    $textoApresentacao = filter_input(INPUT_POST, 'textoApres', FILTER_SANITIZE_STRING);
    $conteudoTopico = filter_input(INPUT_POST, 'conteudoTopico', FILTER_SANITIZE_STRING);
    $legendaTopico = filter_input(INPUT_POST, 'LegendaImagemTopico', FILTER_SANITIZE_STRING);
    $codRamo = filter_input(INPUT_POST, 'codRamo', FILTER_SANITIZE_NUMBER_INT);
        
    if($codUsuario==''||$nomeTopico==''||$textoApresentacao==''||$conteudoTopico==''||$legendaTopico==''||$codRamo==''){
     $error[]='Todos os campos s�o obrigatorios';   
                 header('location:index.php?p=cadastrarTopico&erro=2');

    }
    
    if(count($error)==0){
    
    $topico->setNome_topico($nomeTopico);
    $topico->setTexto_topico($conteudoTopico);
    $topico->setCod_ramo($codRamo);
    $topico->setCod_usuario($codUsuario);
    $topico->setApresentacao_topico($textoApresentacao);
    $topico->setCod_topico($topicodisc);
    $result = $topico->update();        
    header('location:index.php?p=home');
        
    }

    
    
if (count($error)!=0){
    foreach ($error as $erro){
        echo $erro . '<br>';
    }
}

    
    
    
    
    
    
    
