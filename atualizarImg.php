<?php
session_start();
require_once 'core/Usuario.class.php';
$usuario=new Usuario();

$fotoAntiga = $_SESSION['url_foto_usuario'];
$cod_usuario = $_SESSION['cod_usuario'];
$foto = $_FILES['foto'];

if (!empty($foto["name"])){

        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?p=perfil&erro=1');
        }

        // Se não houver nenhum erro
        if (count($error) == 0) {

            // Pega extensão da imagem
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);

            // Gera um nome único para a imagem
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];

            // Caminho de onde ficará a imagem
            $caminho_imagem = "fotos/" . $nome_imagem;

            // Faz o upload da imagem para seu respectivo caminho
            move_uploaded_file($foto["tmp_name"], $caminho_imagem);

            // Insere os dados no banco
            
            $usuario->setUrl_foto_usuario("fotos/" . $nome_imagem);
            $usuario->setCod_usuario($cod_usuario);
            $result = $usuario->updateImgPerfil();
            
            if($result){
                
                
                $_SESSION['url_foto_usuario'] = "fotos/" . $nome_imagem;
                header('location:index.php?p=perfil');
            }



}}