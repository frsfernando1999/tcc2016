<?php

session_start();
require_once 'core/Usuario.class.php';
$error = array();
$usuario = new Usuario();

$codUsuario = $_SESSION['cod_usuario'];
$nome = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);
$sobrenome= filter_input(INPUT_POST, 'sobrenome', FILTER_SANITIZE_STRING);
$sexo = filter_input(INPUT_POST, 'sexo', FILTER_SANITIZE_STRING);
$dataNascimento=$_POST['dataNasc'];

if ($nome == '' || $sobrenome == '' || $sexo == '') {
    $error[] = 'Os campos devem estar preenchidos';
    header('location:index.php?p=alterarPerfil&erro=1');
}

if(count($error==0)){
$usuario->setNome_usuario($nome);
$usuario->setSobrenome_usuario($sobrenome);
$usuario->setSexo_usuario($sexo);
$usuario->setData_nasc_usuario($dataNascimento);
$usuario->setCod_usuario($codUsuario);
$result=$usuario->updatePerfil();
$_SESSION['nome_usuario']=$nome;
$_SESSION['sobrenome_usuario']=$sobrenome;
$_SESSION['sexo_usuario']=$sexo;
$_SESSION['data_nasc_usuario']=$dataNascimento;
}
if($result){
    
    header('location:index.php?p=perfil&sucesso=4');
}











