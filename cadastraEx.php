<?php

session_start();
require_once 'core/Questoes.class.php';
require_once 'core/ListaExercicios.class.php';
require_once 'core/Alternativas.class.php';
$caminhoImgQuest=array();
$error = array();
$imgQuest = array();

$lista = new ListaExercicios();
$questao = new Questoes();
$alter = new Alternativas();

$cod_usuario = $_SESSION['cod_usuario'];
$tipo_lista = $_GET['tipoLista'];

$nome_lista = filter_input(INPUT_POST, 'titulo_lista', FILTER_SANITIZE_STRING);
$data_ins_lista = $data_atual;
$status_exc = 1;
$default = 0;
$topico = $_GET['codTopico'];

if ($tipo_lista == 'D') {
    foreach ($_POST['dissQ'] as $dados) {
        $exEnunciado[] = nl2br(filter_var($dados, FILTER_SANITIZE_STRING));
    }
    foreach ($_POST['dissQresp'] as $dados) {
        $exDissResp[] = nl2br(filter_var($dados, FILTER_SANITIZE_STRING));
    }

    if ($_FILES['imgQuest1'] != NULL) {
        $foto = $_FILES['imgQuest1'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[0] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[0]);
    }}
    
    } else {
        $caminhoImgQuest[0] == NULL;
    }

    if ($_FILES['imgQuest2'] != NULL) {
        $foto = $_FILES['imgQuest2'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[1] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[1]);
    }}
    } else {
        $caminhoImgQuest[1] == NULL;
    }

    if ($_FILES['imgQuest3'] != NULL) {
        $foto = $_FILES['imgQuest3'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[2] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[2]);
    }}
    } else {
        $caminhoImgQuest[2] == NULL;
    }

    if ($_FILES['imgQuest4'] != NULL) {
        $foto = $_FILES['imgQuest4'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[3] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[3]);
    }}
    } else {
        $caminhoImgQuest[3] == NULL;
    }

    if ($_FILES['imgQuest5'] != NULL) {
        $foto = $_FILES['imgQuest5'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[4] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[4]);
    }}
    } else {
        $caminhoImgQuest[4] == NULL;
    }

    if ($_FILES['imgQuest6'] != NULL) {
        $foto = $_FILES['imgQuest6'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[5] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[5]);
    }}
    } else {
        $caminhoImgQuest[5] == NULL;
    }

    if ($_FILES['imgQuest7'] != NULL) {
        $foto = $_FILES['imgQuest7'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[6] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[6]);
    }}
    } else {
        $caminhoImgQuest[6] == NULL;
    }

    if ($_FILES['imgQuest8'] != NULL) {
        $foto = $_FILES['imgQuest8'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[7] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[7]);
    }}
    } else {
        $caminhoImgQuest[7] == NULL;
    }

    if ($_FILES['imgQuest9'] != NULL) {
        $foto = $_FILES['imgQuest9'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[8] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[8]);
    }}
    } else {
        $caminhoImgQuest[8] == NULL;
    }

    if ($_FILES['imgQuest10'] != NULL) {
        $foto = $_FILES['imgQuest10'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[9] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[9]);
    }}
        
    } else {
        $caminhoImgQuest[9] == NULL;
    }

    $imgQuest[0] = $caminhoImgQuest[0];
    $imgQuest[1] = $caminhoImgQuest[1];
    $imgQuest[2] = $caminhoImgQuest[2];
    $imgQuest[3] = $caminhoImgQuest[3];
    $imgQuest[4] = $caminhoImgQuest[4];
    $imgQuest[5] = $caminhoImgQuest[5];
    $imgQuest[6] = $caminhoImgQuest[6];
    $imgQuest[7] = $caminhoImgQuest[7];
    $imgQuest[8] = $caminhoImgQuest[8];
    $imgQuest[9] = $caminhoImgQuest[9];
    
    $lista->setTitulo_lista($nome_lista);
    $lista->setStatus_exclusao_lista($status_exc);
    $lista->setData_inclusao_lista($data_ins_lista);
    $lista->setTipo_lista($tipo_lista);
    $lista->setDefault_lista($default);
    $lista->setCod_usuario($cod_usuario);
    $lista->setCod_topico($topico);
    $lista->insert();

    $row = $lista->select("and data_inclusao_lista=\"$data_ins_lista\"");
    $cod_lista = $row[0]['cod_lista'];
    $i = 0;
    while ($i <= 9) {
        $questao->setEnunciado_ex_obj($exEnunciado[$i]);
        $questao->setObs_questao($exDissResp[$i]);
        $questao->setLista_exercicios_cod_lista($cod_lista);
        $questao->setImg_questao($imgQuest[$i]);
        $questao->insert();
        $i++;
    }
    echo "<script>window.location='index.php?p=home&sucesso=6'</script>";
} elseif ($tipo_lista == 'O') {

    foreach ($_POST['objQ'] as $dados) {
        $exEnunciado[] = nl2br(filter_var($dados, FILTER_SANITIZE_STRING));
    }

    $exResp[] = $_POST['alter1'];
    $exResp[] = $_POST['alter2'];
    $exResp[] = $_POST['alter3'];
    $exResp[] = $_POST['alter4'];
    $exResp[] = $_POST['alter5'];
    $exResp[] = $_POST['alter6'];
    $exResp[] = $_POST['alter7'];
    $exResp[] = $_POST['alter8'];
    $exResp[] = $_POST['alter9'];
    $exResp[] = $_POST['alter10'];

    if ($_FILES['imgQuest1'] != NULL) {
        $foto = $_FILES['imgQuest1'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[0] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[0]);
    }}
    
    } else {
        $caminhoImgQuest[0] == NULL;
    }

    if ($_FILES['imgQuest2'] != NULL) {
        $foto = $_FILES['imgQuest2'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[1] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[1]);
    }}
    } else {
        $caminhoImgQuest[1] == NULL;
    }

    if ($_FILES['imgQuest3'] != NULL) {
        $foto = $_FILES['imgQuest3'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[2] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[2]);
    }}
    } else {
        $caminhoImgQuest[2] == NULL;
    }

    if ($_FILES['imgQuest4'] != NULL) {
        $foto = $_FILES['imgQuest4'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[3] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[3]);
    }}
    } else {
        $caminhoImgQuest[3] == NULL;
    }

    if ($_FILES['imgQuest5'] != NULL) {
        $foto = $_FILES['imgQuest5'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[4] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[4]);
    }}
    } else {
        $caminhoImgQuest[4] == NULL;
    }

    if ($_FILES['imgQuest6'] != NULL) {
        $foto = $_FILES['imgQuest6'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[5] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[5]);
    }}
    } else {
        $caminhoImgQuest[5] == NULL;
    }

    if ($_FILES['imgQuest7'] != NULL) {
        $foto = $_FILES['imgQuest7'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[6] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[6]);
    }}
    } else {
        $caminhoImgQuest[6] == NULL;
    }

    if ($_FILES['imgQuest8'] != NULL) {
        $foto = $_FILES['imgQuest8'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[7] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[7]);
    }}
    } else {
        $caminhoImgQuest[7] == NULL;
    }

    if ($_FILES['imgQuest9'] != NULL) {
        $foto = $_FILES['imgQuest9'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[8] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[8]);
    }}
    } else {
        $caminhoImgQuest[8] == NULL;
    }

    if ($_FILES['imgQuest10'] != NULL) {
        $foto = $_FILES['imgQuest10'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[9] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[9]);
    }}
        
    } else {
        $caminhoImgQuest[9] == NULL;
    }

    $imgQuest[0] = $caminhoImgQuest[0];
    $imgQuest[1] = $caminhoImgQuest[1];
    $imgQuest[2] = $caminhoImgQuest[2];
    $imgQuest[3] = $caminhoImgQuest[3];
    $imgQuest[4] = $caminhoImgQuest[4];
    $imgQuest[5] = $caminhoImgQuest[5];
    $imgQuest[6] = $caminhoImgQuest[6];
    $imgQuest[7] = $caminhoImgQuest[7];
    $imgQuest[8] = $caminhoImgQuest[8];
    $imgQuest[9] = $caminhoImgQuest[9];

    foreach ($_POST['textAlterA'] as $dados) {
        $extextA[] = filter_var($dados, FILTER_SANITIZE_STRING);
    }
    foreach ($_POST['textAlterB'] as $dados) {
        $extextB[] = filter_var($dados, FILTER_SANITIZE_STRING);
    }
    foreach ($_POST['textAlterC'] as $dados) {
        $extextC[] = filter_var($dados, FILTER_SANITIZE_STRING);
    }
    foreach ($_POST['textAlterD'] as $dados) {
        $extextD[] = filter_var($dados, FILTER_SANITIZE_STRING);
    }
    foreach ($_POST['textAlterE'] as $dados) {
        $extextE[] = filter_var($dados, FILTER_SANITIZE_STRING);
    }

    $lista->setTitulo_lista($nome_lista);
    $lista->setStatus_exclusao_lista($status_exc);
    $lista->setData_inclusao_lista($data_ins_lista);
    $lista->setTipo_lista($tipo_lista);
    $lista->setDefault_lista($default);
    $lista->setCod_usuario($cod_usuario);
    $lista->setCod_topico($topico);
    $lista->insert();
    $row = $lista->select("and data_inclusao_lista=\"$data_ins_lista\"");
    $cod_lista = $row[0]['cod_lista'];
    //$exEnunciado
    $i = 0;
    while ($i <= 9) {
        $questao->setEnunciado_ex_obj($exEnunciado[$i]);
        $questao->setObs_questao('');
        $questao->setLista_exercicios_cod_lista($cod_lista);
        $questao->setImg_questao($imgQuest[$i]);
        $questao->insert();

        $rowQuestoes = $questao->selectCod();
        $codQuestao = $rowQuestoes[0]['id'];

        $alter->setAlter_a($extextA[$i]);
        $alter->setAlter_b($extextB[$i]);
        $alter->setAlter_c($extextC[$i]);
        $alter->setAlter_d($extextD[$i]);
        $alter->setAlter_e($extextE[$i]);
        $alter->setCod_resp($exResp[$i]);
        $alter->setCod_questao($codQuestao);
        $alter->insert();
        $i++;
    }
    echo "<script>window.location='index.php?p=home&sucesso=6'</script>";
}


