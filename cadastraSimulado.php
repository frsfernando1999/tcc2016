<?php

session_start();
require_once 'core/Questoes.class.php';
require_once 'core/ListaExercicios.class.php';
require_once 'core/Alternativas.class.php';

$error = array();
$caminhoImgQuest = array();
$imgQuest = array();

$lista = new ListaExercicios();
$questao = new Questoes();
$alter = new Alternativas();

$cod_usuario = $_SESSION['cod_usuario'];
$tipo_lista = 'S';
$nome_lista = filter_input(INPUT_POST, 'titulo_lista', FILTER_SANITIZE_STRING);
$data_ins_lista = $data_atual;
$status_exc = 1;
$default = 1;
$topico = 1;

if($tipo_lista == 'S') {

    foreach ($_POST['objQ'] as $dados) {
        $exEnunciado[] = nl2br(filter_var($dados, FILTER_SANITIZE_STRING));
    }

    $exResp[] = $_POST['alter1'];
    $exResp[] = $_POST['alter2'];
    $exResp[] = $_POST['alter3'];
    $exResp[] = $_POST['alter4'];
    $exResp[] = $_POST['alter5'];
    $exResp[] = $_POST['alter6'];
    $exResp[] = $_POST['alter7'];
    $exResp[] = $_POST['alter8'];
    $exResp[] = $_POST['alter9'];
    $exResp[] = $_POST['alter10'];
    $exResp[] = $_POST['alter11'];
    $exResp[] = $_POST['alter12'];
    $exResp[] = $_POST['alter13'];
    $exResp[] = $_POST['alter14'];
    $exResp[] = $_POST['alter15'];
    $exResp[] = $_POST['alter16'];
    $exResp[] = $_POST['alter17'];
    $exResp[] = $_POST['alter18'];
    $exResp[] = $_POST['alter19'];
    $exResp[] = $_POST['alter20'];
    $exResp[] = $_POST['alter21'];
    $exResp[] = $_POST['alter22'];
    $exResp[] = $_POST['alter23'];
    $exResp[] = $_POST['alter24'];
    $exResp[] = $_POST['alter25'];
    $exResp[] = $_POST['alter26'];
    $exResp[] = $_POST['alter27'];
    $exResp[] = $_POST['alter28'];
    $exResp[] = $_POST['alter29'];
    $exResp[] = $_POST['alter30'];
    $exResp[] = $_POST['alter31'];
    $exResp[] = $_POST['alter32'];
    $exResp[] = $_POST['alter33'];
    $exResp[] = $_POST['alter34'];
    $exResp[] = $_POST['alter35'];
    $exResp[] = $_POST['alter36'];
    $exResp[] = $_POST['alter37'];
    $exResp[] = $_POST['alter38'];
    $exResp[] = $_POST['alter39'];
    $exResp[] = $_POST['alter40'];
    $exResp[] = $_POST['alter41'];
    $exResp[] = $_POST['alter42'];
    $exResp[] = $_POST['alter43'];
    $exResp[] = $_POST['alter44'];
    $exResp[] = $_POST['alter45'];
    $exResp[] = $_POST['alter46'];
    $exResp[] = $_POST['alter47'];
    $exResp[] = $_POST['alter48'];
    $exResp[] = $_POST['alter49'];
    $exResp[] = $_POST['alter50'];
    $exResp[] = $_POST['alter51'];
    $exResp[] = $_POST['alter52'];
    $exResp[] = $_POST['alter53'];
    $exResp[] = $_POST['alter54'];
    $exResp[] = $_POST['alter55'];
    $exResp[] = $_POST['alter56'];
    $exResp[] = $_POST['alter57'];
    $exResp[] = $_POST['alter58'];
    $exResp[] = $_POST['alter59'];
    $exResp[] = $_POST['alter60'];
    $exResp[] = $_POST['alter61'];
    $exResp[] = $_POST['alter62'];
    $exResp[] = $_POST['alter63'];
    $exResp[] = $_POST['alter64'];
    $exResp[] = $_POST['alter65'];
    $exResp[] = $_POST['alter66'];
    $exResp[] = $_POST['alter67'];
    $exResp[] = $_POST['alter68'];
    $exResp[] = $_POST['alter69'];
    $exResp[] = $_POST['alter70'];
    $exResp[] = $_POST['alter71'];
    $exResp[] = $_POST['alter72'];
    $exResp[] = $_POST['alter73'];
    $exResp[] = $_POST['alter74'];
    $exResp[] = $_POST['alter75'];
    $exResp[] = $_POST['alter76'];
    $exResp[] = $_POST['alter77'];
    $exResp[] = $_POST['alter78'];
    $exResp[] = $_POST['alter79'];
    $exResp[] = $_POST['alter80'];
    $exResp[] = $_POST['alter81'];
    $exResp[] = $_POST['alter82'];
    $exResp[] = $_POST['alter83'];
    $exResp[] = $_POST['alter84'];
    $exResp[] = $_POST['alter85'];
    $exResp[] = $_POST['alter86'];
    $exResp[] = $_POST['alter87'];
    $exResp[] = $_POST['alter88'];
    $exResp[] = $_POST['alter89'];
    $exResp[] = $_POST['alter90'];
    
    if ($_FILES['imgQuest1'] != NULL) {
        $foto = $_FILES['imgQuest1'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[0] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[0]);
    }}
    
    } else {
        $caminhoImgQuest[0] == NULL;
    }

    if ($_FILES['imgQuest2'] != NULL) {
        $foto = $_FILES['imgQuest2'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[1] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[1]);
    }}
    } else {
        $caminhoImgQuest[1] == NULL;
    }

    if ($_FILES['imgQuest3'] != NULL) {
        $foto = $_FILES['imgQuest3'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[2] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[2]);
    }}
    } else {
        $caminhoImgQuest[2] == NULL;
    }

    if ($_FILES['imgQuest4'] != NULL) {
        $foto = $_FILES['imgQuest4'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[3] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[3]);
    }}
    } else {
        $caminhoImgQuest[3] == NULL;
    }

    if ($_FILES['imgQuest5'] != NULL) {
        $foto = $_FILES['imgQuest5'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[4] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[4]);
    }}
    } else {
        $caminhoImgQuest[4] == NULL;
    }

    if ($_FILES['imgQuest6'] != NULL) {
        $foto = $_FILES['imgQuest6'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[5] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[5]);
    }}
    } else {
        $caminhoImgQuest[5] == NULL;
    }

    if ($_FILES['imgQuest7'] != NULL) {
        $foto = $_FILES['imgQuest7'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[6] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[6]);
    }}
    } else {
        $caminhoImgQuest[6] == NULL;
    }

    if ($_FILES['imgQuest8'] != NULL) {
        $foto = $_FILES['imgQuest8'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[7] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[7]);
    }}
    } else {
        $caminhoImgQuest[7] == NULL;
    }

    if ($_FILES['imgQuest9'] != NULL) {
        $foto = $_FILES['imgQuest9'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[8] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[8]);
    }}
    } else {
        $caminhoImgQuest[8] == NULL;
    }

    if ($_FILES['imgQuest10'] != NULL) {
        $foto = $_FILES['imgQuest10'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[9] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[9]);
    }}
        
    } else {
        $caminhoImgQuest[9] == NULL;
    }
    
    
    if ($_FILES['imgQuest11'] != NULL) {
        $foto = $_FILES['imgQuest11'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[10] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[10]);
    }}
    
    } else {
        $caminhoImgQuest[10] == NULL;
    }

    if ($_FILES['imgQuest12'] != NULL) {
        $foto = $_FILES['imgQuest12'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[11] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[11]);
    }}
    } else {
        $caminhoImgQuest[11] == NULL;
    }

    if ($_FILES['imgQuest13'] != NULL) {
        $foto = $_FILES['imgQuest13'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[12] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[12]);
    }}
    } else {
        $caminhoImgQuest[12] == NULL;
    }

    if ($_FILES['imgQuest14'] != NULL) {
        $foto = $_FILES['imgQuest14'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[13] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[13]);
    }}
    } else {
        $caminhoImgQuest[13] == NULL;
    }

    if ($_FILES['imgQuest15'] != NULL) {
        $foto = $_FILES['imgQuest15'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[14] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[14]);
    }}
    } else {
        $caminhoImgQuest[14] == NULL;
    }

    if ($_FILES['imgQuest16'] != NULL) {
        $foto = $_FILES['imgQuest16'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[15] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[15]);
    }}
    } else {
        $caminhoImgQuest[15] == NULL;
    }

    if ($_FILES['imgQuest17'] != NULL) {
        $foto = $_FILES['imgQuest17'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[16] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[16]);
    }}
    } else {
        $caminhoImgQuest[16] == NULL;
    }

    if ($_FILES['imgQuest18'] != NULL) {
        $foto = $_FILES['imgQuest18'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[17] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[17]);
    }}
    } else {
        $caminhoImgQuest[17] == NULL;
    }

    if ($_FILES['imgQuest19'] != NULL) {
        $foto = $_FILES['imgQuest19'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[18] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[18]);
    }}
    } else {
        $caminhoImgQuest[18] == NULL;
    }

    if ($_FILES['imgQuest20'] != NULL) {
        $foto = $_FILES['imgQuest20'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[19] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[19]);
    }}
        
    } else {
        $caminhoImgQuest[19] == NULL;
    }
    
    
    if ($_FILES['imgQuest21'] != NULL) {
        $foto = $_FILES['imgQuest21'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[20] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[20]);
    }}
    
    } else {
        $caminhoImgQuest[20] == NULL;
    }

    if ($_FILES['imgQuest22'] != NULL) {
        $foto = $_FILES['imgQuest22'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[21] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[21]);
    }}
    } else {
        $caminhoImgQuest[21] == NULL;
    }

    if ($_FILES['imgQuest23'] != NULL) {
        $foto = $_FILES['imgQuest23'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[22] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[22]);
    }}
    } else {
        $caminhoImgQuest[22] == NULL;
    }

    if ($_FILES['imgQuest24'] != NULL) {
        $foto = $_FILES['imgQuest24'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[23] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[23]);
    }}
    } else {
        $caminhoImgQuest[23] == NULL;
    }

    if ($_FILES['imgQuest25'] != NULL) {
        $foto = $_FILES['imgQuest25'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[24] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[24]);
    }}
    } else {
        $caminhoImgQuest[24] == NULL;
    }

    if ($_FILES['imgQuest26'] != NULL) {
        $foto = $_FILES['imgQuest26'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[25] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[25]);
    }}
    } else {
        $caminhoImgQuest[25] == NULL;
    }

    if ($_FILES['imgQuest27'] != NULL) {
        $foto = $_FILES['imgQuest27'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[26] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[26]);
    }}
    } else {
        $caminhoImgQuest[26] == NULL;
    }

    if ($_FILES['imgQuest28'] != NULL) {
        $foto = $_FILES['imgQuest28'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[27] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[27]);
    }}
    } else {
        $caminhoImgQuest[27] == NULL;
    }

    if ($_FILES['imgQuest29'] != NULL) {
        $foto = $_FILES['imgQuest29'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[28] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[28]);
    }}
    } else {
        $caminhoImgQuest[8] == NULL;
    }

    if ($_FILES['imgQuest30'] != NULL) {
        $foto = $_FILES['imgQuest30'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[29] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[29]);
    }}
        
    } else {
        $caminhoImgQuest[29] == NULL;
    }
    
    
    if ($_FILES['imgQuest31'] != NULL) {
        $foto = $_FILES['imgQuest31'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[30] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[30]);
    }}
    
    } else {
        $caminhoImgQuest[30] == NULL;
    }

    if ($_FILES['imgQuest32'] != NULL) {
        $foto = $_FILES['imgQuest32'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[31] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[31]);
    }}
    } else {
        $caminhoImgQuest[31] == NULL;
    }

    if ($_FILES['imgQuest33'] != NULL) {
        $foto = $_FILES['imgQuest33'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[32] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[32]);
    }}
    } else {
        $caminhoImgQuest[32] == NULL;
    }

    if ($_FILES['imgQuest34'] != NULL) {
        $foto = $_FILES['imgQuest34'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[33] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[33]);
    }}
    } else {
        $caminhoImgQuest[33] == NULL;
    }

    if ($_FILES['imgQuest35'] != NULL) {
        $foto = $_FILES['imgQuest35'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[34] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[34]);
    }}
    } else {
        $caminhoImgQuest[34] == NULL;
    }

    if ($_FILES['imgQuest36'] != NULL) {
        $foto = $_FILES['imgQuest36'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[35] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[35]);
    }}
    } else {
        $caminhoImgQuest[35] == NULL;
    }

    if ($_FILES['imgQuest37'] != NULL) {
        $foto = $_FILES['imgQuest37'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[36] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[36]);
    }}
    } else {
        $caminhoImgQuest[36] == NULL;
    }

    if ($_FILES['imgQuest38'] != NULL) {
        $foto = $_FILES['imgQuest38'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[37] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[37]);
    }}
    } else {
        $caminhoImgQuest[37] == NULL;
    }

    if ($_FILES['imgQuest39'] != NULL) {
        $foto = $_FILES['imgQuest39'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[38] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[38]);
    }}
    } else {
        $caminhoImgQuest[38] == NULL;
    }

    if ($_FILES['imgQuest40'] != NULL) {
        $foto = $_FILES['imgQuest40'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[39] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[39]);
    }}
        
    } else {
        $caminhoImgQuest[39] == NULL;
    }
    
    
    if ($_FILES['imgQuest41'] != NULL) {
        $foto = $_FILES['imgQuest41'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[40] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[40]);
    }}
    
    } else {
        $caminhoImgQuest[40] == NULL;
    }

    if ($_FILES['imgQuest42'] != NULL) {
        $foto = $_FILES['imgQuest42'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[41] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[41]);
    }}
    } else {
        $caminhoImgQuest[41] == NULL;
    }

    if ($_FILES['imgQuest43'] != NULL) {
        $foto = $_FILES['imgQuest43'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[42] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[42]);
    }}
    } else {
        $caminhoImgQuest[42] == NULL;
    }

    if ($_FILES['imgQuest44'] != NULL) {
        $foto = $_FILES['imgQuest44'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[43] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[43]);
    }}
    } else {
        $caminhoImgQuest[3] == NULL;
    }

    if ($_FILES['imgQuest45'] != NULL) {
        $foto = $_FILES['imgQuest45'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[44] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[44]);
    }}
    } else {
        $caminhoImgQuest[44] == NULL;
    }

    if ($_FILES['imgQuest46'] != NULL) {
        $foto = $_FILES['imgQuest46'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[45] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[45]);
    }}
    } else {
        $caminhoImgQuest[45] == NULL;
    }

    if ($_FILES['imgQuest47'] != NULL) {
        $foto = $_FILES['imgQuest47'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[46] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[46]);
    }}
    } else {
        $caminhoImgQuest[46] == NULL;
    }

    if ($_FILES['imgQuest48'] != NULL) {
        $foto = $_FILES['imgQuest48'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[47] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[47]);
    }}
    } else {
        $caminhoImgQuest[47] == NULL;
    }

    if ($_FILES['imgQuest49'] != NULL) {
        $foto = $_FILES['imgQuest49'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[48] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[48]);
    }}
    } else {
        $caminhoImgQuest[48] == NULL;
    }

    if ($_FILES['imgQuest50'] != NULL) {
        $foto = $_FILES['imgQuest50'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[49] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[49]);
    }}
        
    } else {
        $caminhoImgQuest[49] == NULL;
    }
    
    
    if ($_FILES['imgQuest51'] != NULL) {
        $foto = $_FILES['imgQuest51'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[50] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[50]);
    }}
    
    } else {
        $caminhoImgQuest[50] == NULL;
    }

    if ($_FILES['imgQuest52'] != NULL) {
        $foto = $_FILES['imgQuest52'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[51] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[51]);
    }}
    } else {
        $caminhoImgQuest[51] == NULL;
    }

    if ($_FILES['imgQuest53'] != NULL) {
        $foto = $_FILES['imgQuest53'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[52] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[52]);
    }}
    } else {
        $caminhoImgQuest[52] == NULL;
    }

    if ($_FILES['imgQuest54'] != NULL) {
        $foto = $_FILES['imgQuest54'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[53] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[53]);
    }}
    } else {
        $caminhoImgQuest[53] == NULL;
    }

    if ($_FILES['imgQuest55'] != NULL) {
        $foto = $_FILES['imgQuest55'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[54] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[54]);
    }}
    } else {
        $caminhoImgQuest[54] == NULL;
    }

    if ($_FILES['imgQuest56'] != NULL) {
        $foto = $_FILES['imgQuest56'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[55] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[55]);
    }}
    } else {
        $caminhoImgQuest[55] == NULL;
    }

    if ($_FILES['imgQuest57'] != NULL) {
        $foto = $_FILES['imgQuest57'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[56] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[56]);
    }}
    } else {
        $caminhoImgQuest[56] == NULL;
    }

    if ($_FILES['imgQuest58'] != NULL) {
        $foto = $_FILES['imgQuest58'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[57] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[57]);
    }}
    } else {
        $caminhoImgQuest[57] == NULL;
    }

    if ($_FILES['imgQuest59'] != NULL) {
        $foto = $_FILES['imgQuest59'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[58] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[58]);
    }}
    } else {
        $caminhoImgQuest[58] == NULL;
    }

    if ($_FILES['imgQuest60'] != NULL) {
        $foto = $_FILES['imgQuest60'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[59] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[59]);
    }}
        
    } else {
        $caminhoImgQuest[59] == NULL;
    }
    
    
    if ($_FILES['imgQuest61'] != NULL) {
        $foto = $_FILES['imgQuest61'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[60] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[60]);
    }}
    
    } else {
        $caminhoImgQuest[60] == NULL;
    }

    if ($_FILES['imgQuest62'] != NULL) {
        $foto = $_FILES['imgQuest62'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[61] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[61]);
    }}
    } else {
        $caminhoImgQuest[61] == NULL;
    }

    if ($_FILES['imgQuest63'] != NULL) {
        $foto = $_FILES['imgQuest63'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[62] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[62]);
    }}
    } else {
        $caminhoImgQuest[62] == NULL;
    }

    if ($_FILES['imgQuest64'] != NULL) {
        $foto = $_FILES['imgQuest64'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[63] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[63]);
    }}
    } else {
        $caminhoImgQuest[63] == NULL;
    }

    if ($_FILES['imgQuest65'] != NULL) {
        $foto = $_FILES['imgQuest65'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[64] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[64]);
    }}
    } else {
        $caminhoImgQuest[64] == NULL;
    }

    if ($_FILES['imgQuest66'] != NULL) {
        $foto = $_FILES['imgQuest66'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[65] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[65]);
    }}
    } else {
        $caminhoImgQuest[65] == NULL;
    }

    if ($_FILES['imgQuest67'] != NULL) {
        $foto = $_FILES['imgQuest67'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[66] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[66]);
    }}
    } else {
        $caminhoImgQuest[66] == NULL;
    }

    if ($_FILES['imgQuest68'] != NULL) {
        $foto = $_FILES['imgQuest68'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[67] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[67]);
    }}
    } else {
        $caminhoImgQuest[67] == NULL;
    }

    if ($_FILES['imgQuest69'] != NULL) {
        $foto = $_FILES['imgQuest69'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[68] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[68]);
    }}
    } else {
        $caminhoImgQuest[68] == NULL;
    }

    if ($_FILES['imgQuest70'] != NULL) {
        $foto = $_FILES['imgQuest70'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[69] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[69]);
    }}
        
    } else {
        $caminhoImgQuest[69] == NULL;
    }
    
    
    if ($_FILES['imgQuest71'] != NULL) {
        $foto = $_FILES['imgQuest71'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[70] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[70]);
    }}
    
    } else {
        $caminhoImgQuest[70] == NULL;
    }

    if ($_FILES['imgQuest72'] != NULL) {
        $foto = $_FILES['imgQuest72'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[71] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[71]);
    }}
    } else {
        $caminhoImgQuest[71] == NULL;
    }

    if ($_FILES['imgQuest73'] != NULL) {
        $foto = $_FILES['imgQuest73'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[72] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[72]);
    }}
    } else {
        $caminhoImgQuest[72] == NULL;
    }

    if ($_FILES['imgQuest74'] != NULL) {
        $foto = $_FILES['imgQuest74'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[73] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[73]);
    }}
    } else {
        $caminhoImgQuest[73] == NULL;
    }

    if ($_FILES['imgQuest75'] != NULL) {
        $foto = $_FILES['imgQuest75'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[74] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[74]);
    }}
    } else {
        $caminhoImgQuest[74] == NULL;
    }

    if ($_FILES['imgQuest76'] != NULL) {
        $foto = $_FILES['imgQuest76'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[75] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[75]);
    }}
    } else {
        $caminhoImgQuest[75] == NULL;
    }

    if ($_FILES['imgQuest77'] != NULL) {
        $foto = $_FILES['imgQuest77'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[76] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[76]);
    }}
    } else {
        $caminhoImgQuest[76] == NULL;
    }

    if ($_FILES['imgQuest78'] != NULL) {
        $foto = $_FILES['imgQuest78'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[77] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[77]);
    }}
    } else {
        $caminhoImgQuest[77] == NULL;
    }

    if ($_FILES['imgQuest79'] != NULL) {
        $foto = $_FILES['imgQuest79'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[78] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[78]);
    }}
    } else {
        $caminhoImgQuest[78] == NULL;
    }

    if ($_FILES['imgQuest80'] != NULL) {
        $foto = $_FILES['imgQuest80'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[79] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[79]);
    }}
        
    } else {
        $caminhoImgQuest[79] == NULL;
    }
    
    
    if ($_FILES['imgQuest81'] != NULL) {
        $foto = $_FILES['imgQuest81'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[80] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[80]);
    }}
    
    } else {
        $caminhoImgQuest[80] == NULL;
    }

    if ($_FILES['imgQuest82'] != NULL) {
        $foto = $_FILES['imgQuest82'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[81] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[81]);
    }}
    } else {
        $caminhoImgQuest[81] == NULL;
    }

    if ($_FILES['imgQuest83'] != NULL) {
        $foto = $_FILES['imgQuest83'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[82] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[82]);
    }}
    } else {
        $caminhoImgQuest[82] == NULL;
    }

    if ($_FILES['imgQuest84'] != NULL) {
        $foto = $_FILES['imgQuest84'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[83] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[83]);
    }}
    } else {
        $caminhoImgQuest[83] == NULL;
    }

    if ($_FILES['imgQuest85'] != NULL) {
        $foto = $_FILES['imgQuest85'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[84] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[84]);
    }}
    } else {
        $caminhoImgQuest[84] == NULL;
    }

    if ($_FILES['imgQuest86'] != NULL) {
        $foto = $_FILES['imgQuest86'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[85] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[85]);
    }}
    } else {
        $caminhoImgQuest[85] == NULL;
    }

    if ($_FILES['imgQuest87'] != NULL) {
        $foto = $_FILES['imgQuest87'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[86] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[86]);
    }}
    } else {
        $caminhoImgQuest[86] == NULL;
    }

    if ($_FILES['imgQuest88'] != NULL) {
        $foto = $_FILES['imgQuest88'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[87] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[87]);
    }}
    } else {
        $caminhoImgQuest[87] == NULL;
    }

    if ($_FILES['imgQuest89'] != NULL) {
        $foto = $_FILES['imgQuest89'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[88] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[88]);
    }}
    } else {
        $caminhoImgQuest[88] == NULL;
    }

    if ($_FILES['imgQuest90'] != NULL) {
        $foto = $_FILES['imgQuest90'];
        if (!empty($foto["name"])){
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp|webp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
            header('location:index.php?erro=2');
        }
        if (count($error) == 0) {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
            $caminhoImgQuest[89] = "fotos/" . $nome_imagem;
            move_uploaded_file($foto["tmp_name"], $caminhoImgQuest[89]);
    }}
        
    } else {
        $caminhoImgQuest[89] == NULL;
    }
    
    $imgQuest[0] = $caminhoImgQuest[0];
    $imgQuest[1] = $caminhoImgQuest[1];
    $imgQuest[2] = $caminhoImgQuest[2];
    $imgQuest[3] = $caminhoImgQuest[3];
    $imgQuest[4] = $caminhoImgQuest[4];
    $imgQuest[5] = $caminhoImgQuest[5];
    $imgQuest[6] = $caminhoImgQuest[6];
    $imgQuest[7] = $caminhoImgQuest[7];
    $imgQuest[8] = $caminhoImgQuest[8];
    $imgQuest[9] = $caminhoImgQuest[9];
    $imgQuest[10] = $caminhoImgQuest[10];
    $imgQuest[11] = $caminhoImgQuest[11];
    $imgQuest[12] = $caminhoImgQuest[12];
    $imgQuest[13] = $caminhoImgQuest[13];
    $imgQuest[14] = $caminhoImgQuest[14];
    $imgQuest[15] = $caminhoImgQuest[15];
    $imgQuest[16] = $caminhoImgQuest[16];
    $imgQuest[17] = $caminhoImgQuest[17];
    $imgQuest[18] = $caminhoImgQuest[18];
    $imgQuest[19] = $caminhoImgQuest[19];
    $imgQuest[20] = $caminhoImgQuest[20];
    $imgQuest[21] = $caminhoImgQuest[21];
    $imgQuest[22] = $caminhoImgQuest[22];
    $imgQuest[23] = $caminhoImgQuest[23];
    $imgQuest[24] = $caminhoImgQuest[24];
    $imgQuest[25] = $caminhoImgQuest[25];
    $imgQuest[26] = $caminhoImgQuest[26];
    $imgQuest[27] = $caminhoImgQuest[27];
    $imgQuest[28] = $caminhoImgQuest[28];
    $imgQuest[29] = $caminhoImgQuest[29];
    $imgQuest[30] = $caminhoImgQuest[30];
    $imgQuest[31] = $caminhoImgQuest[31];
    $imgQuest[32] = $caminhoImgQuest[32];
    $imgQuest[33] = $caminhoImgQuest[33];
    $imgQuest[34] = $caminhoImgQuest[34];
    $imgQuest[35] = $caminhoImgQuest[35];
    $imgQuest[36] = $caminhoImgQuest[36];
    $imgQuest[37] = $caminhoImgQuest[37];
    $imgQuest[38] = $caminhoImgQuest[38];
    $imgQuest[39] = $caminhoImgQuest[39];
    $imgQuest[40] = $caminhoImgQuest[40];
    $imgQuest[41] = $caminhoImgQuest[41];
    $imgQuest[42] = $caminhoImgQuest[42];
    $imgQuest[43] = $caminhoImgQuest[43];
    $imgQuest[44] = $caminhoImgQuest[44];
    $imgQuest[45] = $caminhoImgQuest[45];
    $imgQuest[46] = $caminhoImgQuest[46];
    $imgQuest[47] = $caminhoImgQuest[47];
    $imgQuest[48] = $caminhoImgQuest[48];
    $imgQuest[49] = $caminhoImgQuest[49];
    $imgQuest[50] = $caminhoImgQuest[50];
    $imgQuest[51] = $caminhoImgQuest[51];
    $imgQuest[52] = $caminhoImgQuest[52];
    $imgQuest[53] = $caminhoImgQuest[53];
    $imgQuest[54] = $caminhoImgQuest[54];
    $imgQuest[55] = $caminhoImgQuest[55];
    $imgQuest[56] = $caminhoImgQuest[56];
    $imgQuest[57] = $caminhoImgQuest[57];
    $imgQuest[58] = $caminhoImgQuest[58];
    $imgQuest[59] = $caminhoImgQuest[59];
    $imgQuest[60] = $caminhoImgQuest[60];
    $imgQuest[61] = $caminhoImgQuest[61];
    $imgQuest[62] = $caminhoImgQuest[62];
    $imgQuest[63] = $caminhoImgQuest[63];
    $imgQuest[64] = $caminhoImgQuest[64];
    $imgQuest[65] = $caminhoImgQuest[65];
    $imgQuest[66] = $caminhoImgQuest[66];
    $imgQuest[67] = $caminhoImgQuest[67];
    $imgQuest[68] = $caminhoImgQuest[68];
    $imgQuest[69] = $caminhoImgQuest[69];
    $imgQuest[70] = $caminhoImgQuest[70];
    $imgQuest[71] = $caminhoImgQuest[71];
    $imgQuest[72] = $caminhoImgQuest[72];
    $imgQuest[73] = $caminhoImgQuest[73];
    $imgQuest[74] = $caminhoImgQuest[74];
    $imgQuest[75] = $caminhoImgQuest[75];
    $imgQuest[76] = $caminhoImgQuest[76];
    $imgQuest[77] = $caminhoImgQuest[77];
    $imgQuest[78] = $caminhoImgQuest[78];
    $imgQuest[79] = $caminhoImgQuest[79];
    $imgQuest[80] = $caminhoImgQuest[80];
    $imgQuest[81] = $caminhoImgQuest[81];
    $imgQuest[82] = $caminhoImgQuest[82];
    $imgQuest[83] = $caminhoImgQuest[83];
    $imgQuest[84] = $caminhoImgQuest[84];
    $imgQuest[85] = $caminhoImgQuest[85];
    $imgQuest[86] = $caminhoImgQuest[86];
    $imgQuest[87] = $caminhoImgQuest[87];
    $imgQuest[88] = $caminhoImgQuest[88];
    $imgQuest[89] = $caminhoImgQuest[89];


    foreach ($_POST['textAlterA'] as $dados) {
        $extextA[] = filter_var($dados, FILTER_SANITIZE_STRING);
    }
    foreach ($_POST['textAlterB'] as $dados) {
        $extextB[] = filter_var($dados, FILTER_SANITIZE_STRING);
    }
    foreach ($_POST['textAlterC'] as $dados) {
        $extextC[] = filter_var($dados, FILTER_SANITIZE_STRING);
    }
    foreach ($_POST['textAlterD'] as $dados) {
        $extextD[] = filter_var($dados, FILTER_SANITIZE_STRING);
    }
    foreach ($_POST['textAlterE'] as $dados) {
        $extextE[] = filter_var($dados, FILTER_SANITIZE_STRING);
    }

    $lista->setTitulo_lista($nome_lista);
    $lista->setStatus_exclusao_lista($status_exc);
    $lista->setData_inclusao_lista($data_ins_lista);
    $lista->setTipo_lista($tipo_lista);
    $lista->setDefault_lista($default);
    $lista->setCod_usuario($cod_usuario);
    $lista->setCod_topico($topico);
    $lista->insert();
    $row = $lista->select("and data_inclusao_lista=\"$data_ins_lista\"");
    $cod_lista = $row[0]['cod_lista'];
    //$exEnunciado
    $i = 0;
    while ($i <= 89) {
        $questao->setEnunciado_ex_obj($exEnunciado[$i]);
        $questao->setObs_questao('');
        $questao->setLista_exercicios_cod_lista($cod_lista);
        $questao->setImg_questao($imgQuest[$i]);
        $questao->insert();

        $rowQuestoes = $questao->selectCod();
        $codQuestao = $rowQuestoes[0]['id'];

        $alter->setAlter_a($extextA[$i]);
        $alter->setAlter_b($extextB[$i]);
        $alter->setAlter_c($extextC[$i]);
        $alter->setAlter_d($extextD[$i]);
        $alter->setAlter_e($extextE[$i]);
        $alter->setCod_resp($exResp[$i]);
        $alter->setCod_questao($codQuestao);
        $alter->insert();
        $i++;
    }
    echo "<script>window.location='index.php?p=home&sucesso=6'</script>";
}


