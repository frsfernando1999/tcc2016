<?php
    require_once 'core/RamoDisciplina.class.php';
    $error = array();
    $ramo = new RamoDisciplina();
    $nomeRamo = filter_input(INPUT_POST, 'nomeRamo', FILTER_SANITIZE_STRING);
    $codDisc = filter_input(INPUT_POST, 'codDisc', FILTER_SANITIZE_NUMBER_INT);
    
    if($nomeRamo=='' || $codDisc==''){
        $error[]='Preencha todos os campos';
        header('location:index.php?p=cadastrarRamo&erro=1');
    }
    
    $total=$ramo->select("and nome_ramo='$nomeRamo'",'');
    if(count($total)!=0){
        $error[]='Ramo já registrado';
        header('location:index.php?p=cadastrarRamo&erro=151');
    }
        
    if(count($error)==0){
if ($_POST['cadastrar']) {
    $ramo->setNome_ramo($nomeRamo);
    $ramo->setDisciplinas_cod_disc($codDisc);
    $ramo->insert();
    header('location:index.php?p=areaAdm&sucesso=1');
    }}
    
if (count($error)!=0){
    foreach ($error as $erro){
        echo $erro . '<br>';
    }
}