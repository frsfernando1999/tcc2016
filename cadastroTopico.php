    <?php
    session_start();
    require_once 'core/TopicoDisciplina.class.php';
    require_once 'core/Arquivo.class.php';
    $error=array();
    $arquivo=new Arquivo();
    $topico=new TopicoDisciplina();
    
    $codUsuario = $_SESSION['cod_usuario'];
    $nomeTopico = filter_input(INPUT_POST, 'nomeTopico', FILTER_SANITIZE_STRING);
    $textoApresentacao = filter_input(INPUT_POST, 'textoApres', FILTER_SANITIZE_STRING);
    $conteudoTopico = filter_input(INPUT_POST, 'conteudoTopico', FILTER_SANITIZE_STRING);
    $legendaTopico = filter_input(INPUT_POST, 'LegendaImagemTopico', FILTER_SANITIZE_STRING);
    $codRamo = filter_input(INPUT_POST, 'codRamo', FILTER_SANITIZE_NUMBER_INT);
    if($_FILES['foto']){$foto = $_FILES['foto'];}
//    elseif(empty($_FILES['foto'])){$error[]='Adicione uma imagem';}
    
        $total=$topico->select("and nome_topico='$nomeTopico'");
        if(count($total)!=0){
            $error[]='Tópico já cadastrado';
            header('location:index.php?p=cadastrarTopico&erro=151');
       }
    
    if($codUsuario==''||$nomeTopico==''||$textoApresentacao==''||$conteudoTopico==''||$legendaTopico==''||$codRamo==''){
     $error[]='Todos os campos são obrigatorios';   
                 header('location:index.php?p=cadastrarTopico&erro=2');

    }
    
    if(count($error)==0){
    
    $topico->setNome_topico($nomeTopico);
    $topico->setTexto_topico($conteudoTopico);
    $topico->setCod_ramo($codRamo);
    $topico->setCod_usuario($codUsuario);
    $topico->setApresentacao_topico($textoApresentacao);
    $result = $topico->insert();
    if($result){
        
    
        $row=$topico->select("and nome_topico='$nomeTopico' and cod_usuario='$codUsuario'",'');
        $codTopico=$row[0]['cod_topico'];
        if (!empty($foto["name"])) {

        // Verifica se o arquivo é uma imagem
        if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/", $foto["type"])) {
            $error[] = "Isso não é uma imagem.";
        }

        // Se não houver nenhum erro
        if (count($error) == 0) {

            // Pega extensão da imagem
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);

            // Gera um nome único para a imagem
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];

            // Caminho de onde ficará a imagem
            $caminho_imagem = "img_topico/" . $nome_imagem;

            // Faz o upload da imagem para seu respectivo caminho
            move_uploaded_file($foto["tmp_name"], $caminho_imagem);

            // extensão da imagem
            $extensao = pathinfo($nome_imagem, PATHINFO_EXTENSION);
            
            $arquivo->setUrl_arq("img_topico/" . $nome_imagem);
            $arquivo->setLegenda_arq($legendaTopico);
            $arquivo->setTipo_arq($extensao);
            $arquivo->setTopico_disciplina_cod_topico($codTopico);
            $resultado=$arquivo->insert();
            
            if ($resultado) {
            header('location:index.php?p=areaAdm&sucesso=1');
        }}}
        
        
        
        
        
    }}

    
    
if (count($error)!=0){
    foreach ($error as $erro){
        echo $erro . '<br>';
    }
}

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    