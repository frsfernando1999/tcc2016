<?php

class CfLogin3c {
    // A função explode(), separa a string em partes guardando as partes
    // em uma matriz, a mesma recebe dois parâmetros
    // explode("caracter delimitador", "string")
    public function dateToBr($dataAmericana) {
        $d = explode('-', $dataAmericana);
        $dOk = $d[2].'/'.$d[1].'/'.$d[0];
        return $dOk;
    }

    public function dateToUs($dataBrasil){
        if (stripos($dataBrasil, '/')) {
           $d = explode('/', $dataBrasil); 
        }else{
           $d = explode('-', $dataBrasil); 
            }
        $dOk = $d[2].'-'.$d[1].'-'.$d[0];
        
        return $dOk;
    }
    
    public function dateTimeToBr($dataAmericana_his){
        $d = explode(' ',$dataAmericana_his);
        $dOk = $this->dateToBr($d[0]);
        return $dOk.' '.$d[1];
    }
    
    public function dateTimeToUs($dataBrasil_his){
        $d = explode(' ', $dataBrasil_his);
        $dOk = $this->dateToUs($d[0].' '.$d[1]);
        $dOk = $this->dateToUs($d[0]);
        return $dOk.' '.$d[1];
    }
}
$data_atual = date("Y-m-d H:i:s");
