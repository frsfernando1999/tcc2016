    <?php
require_once 'config/CfLogin3c.class.php';

class DbConnection extends CfLogin3c{
    private $user      = 'u139641168_vang';
    private $pass      = 'rBqNaFs5';
    private $host      = 'mysql.hostinger.com.br';
    private $port      = '3306';
    private $database  = 'u139641168_vest';
    // Método para conexão
    private function connect(){
        $conn = new PDO("mysql:host=$this->host;port=$this->port;dbname=$this->database",  $this->user,  $this->pass);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conn;
        
    }
    // Método para insert, update ou delete
    public function runQuery($sql){
        
        $stm = $this->connect()->prepare($sql);
        return $stm->execute();
        
    }
    // Método para select
    public function runSelect($sql){
        $stm = $this->connect()->prepare($sql);
        $stm->execute();
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }
}
