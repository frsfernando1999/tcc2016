<!DOCTYPE html>
<html>
    <head>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="O Vestibulário é o site de estudos para vestibular que conta com professores de todo o Brasil, prontos para te ajudar a entrar na tão sonhada faculdade!">
        <meta name="keywords" content="vestibular, estudar, universidade, gratis, faculdade, materia, enem, fuvest, simulado, exercicios, curso, aprender, passar, conteudo, aula, disciplina, online">
        <meta name="author" content="Vanguart">
        <link rel="shortcut icon" href="imagem/fav-icon-vestibulario.png">
        <link href="css/css_reset.css" rel="stylesheet"/>
        <link rel="stylesheet" media="screen and (max-width:319px)" href="css/style_319px.css">
        <link rel="stylesheet" media="screen and (min-width:320px) and (max-width:479px)" href="css/style_320px.css">
        <link rel="stylesheet" media="screen and (min-width:480px) and (max-width:599px)" href="css/style_480px.css">
        <link rel="stylesheet" media="screen and (min-width:600px) and (max-width:767px)" href="css/style_600px.css">
        <link rel="stylesheet" media="screen and (min-width:768px) and (max-width:1023px)" href="css/style_768px.css">
        <link rel="stylesheet" media="screen and (min-width:1024px) and (max-width:1199px)" href="css/style_1024px.css">
        <link rel="stylesheet" media="screen and (min-width:1200px)" href="css/style_1200px.css"> 
        <script src="javascript/js.js"></script>
        <script src="javascript/jquery-3.1.0.js" type="text/javascript"></script>
        <script src="javascript/modal.js"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <script src="javascript/jquery.maskedinput.js" type="text/javascript"></script>
    </head>
    <body id="body2">
        <div id="container2">
            <a href="frmLogin.php"> <img src="imagem/logoG.png" alt="Logo Vestibulario" id="responsaLogo"></a>
            <section id="espacoSenhaA">
                <p class="textoSenha">Esqueceu sua senha? Digite seu e-mail e receba uma mensagem para alterar sua senha.</p>    
                <form class="formEsqueceSenha" action="esqueciasenha.php" method="post">
                    <p><label class="labelEsqueceSenha">Email</label></p>
                    <input type="email" name="email" required="" class="emailEsqueceSenha">
                    <p class="botoesSenha">
                        <a href="frmLogin.php" class="voltaLogin">Voltar</a>
                        <input type="submit" class="enviaSenhaNova">
                    </p>
                </form>
            </section>
        </div>

        <footer id="rodape_fixo2">
            <a href="?p=sobreNos"><img src="imagem/logoV.png" id="img_rodape" width="80px" height="70px" alt="#"/></a>

            <a href="TermosI.php" class="link_rodape">Termos de uso</a>
            <a href="PrivacidadeI.php" class="link_rodape">Privacidade</a>
            <a href="sobreNosI.php" class="link_rodape">Sobre Nós</a>
        </footer>
    </body> 
</div>

</html>
