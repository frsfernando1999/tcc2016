<?php
session_start();
if (!empty($_SESSION)) {
    header("location:index.php");
}

if (isset($_GET['erro']) && $_GET['erro'] != "") {
    $erro = $_GET['erro'];
}

if (isset($_GET['sucesso']) && $_GET['sucesso'] != "") {
    $sucesso= $_GET['sucesso'];

}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Vestibulario</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="O Vestibulário é o site de estudos para vestibular que conta com professores de todo o Brasil, prontos para te ajudar a entrar na tão sonhada faculdade!">
        <meta name="keywords" content="vestibular, estudar, universidade, gratis, faculdade, materia, enem, fuvest, simulado, exercicios, curso, aprender, passar, conteudo, aula, disciplina, online">
        <meta name="author" content="Vanguart">
        <link rel="shortcut icon" href="imagem/fav-icon-vestibulario.png">
        <link href="css/css_reset.css" rel="stylesheet"/>
        <link rel="stylesheet" media="screen and (max-width:319px)" href="css/style_319px.css">
        <link rel="stylesheet" media="screen and (min-width:320px) and (max-width:479px)" href="css/style_320px.css">
        <link rel="stylesheet" media="screen and (min-width:480px) and (max-width:599px)" href="css/style_480px.css">
        <link rel="stylesheet" media="screen and (min-width:600px) and (max-width:767px)" href="css/style_600px.css">
        <link rel="stylesheet" media="screen and (min-width:768px) and (max-width:1023px)" href="css/style_768px.css">
        <link rel="stylesheet" media="screen and (min-width:1024px) and (max-width:1199px)" href="css/style_1024px.css">
        <link rel="stylesheet" media="screen and (min-width:1200px)" href="css/style_1200px.css"> 
        <script src="javascript/js.js"></script>
        <script src="javascript/jquery-3.1.0.js" type="text/javascript"></script>
        <script src="javascript/modal.js"></script>
        <script src="https://www.google.com/recaptcha/api.js?onload=myCallBack&render=explicit" async defer></script>
        <script src="javascript/jquery.maskedinput.js" type="text/javascript"></script>
        <script>
            jQuery(function ($) {
                $("#cpf").mask("999.999.999-99");
            });

            function validaSenha(input) {
                if (input.value !== document.getElementById('senha').value) {
                    input.setCustomValidity('Repita a senha corretamente');
                } else {
                    input.setCustomValidity('');
                }
            }
                     
            function validaSenhaP(input) {
                if (input.value !== document.getElementById('senhaP').value) {
                    input.setCustomValidity('Repita a senha corretamente');
                } else {
                    input.setCustomValidity('');
                }
            }

        </script>
    </head>
    <div class="fundoModal"></div>
    <body id="body2">
        <h1 style="display: none">Vestibul�rio</h1>
        <div id="container2">
                 <?php 
                    if($erro!=''||$sucesso!=''){
                    include_once 'modalSucesso.php';
                    }?> 
            <img src="imagem/logoG.png" alt="Logo Vestibulario" id="responsaLogo">
            <div id="textoApresentacao">Bem-vindo ao Vestibulário! Você precisa estar conectado para ter acesso aos conteúdos disponibilizados em nosso site. Registre-se ou faça login.</div>
<section id="formularios">
                
                <button id="botaoAluno" class="botaoAluno" onclick="document.getElementById('frmLoginProfessorSection').style.display='none';document.getElementById('frmLoginAlunoSection').style.display='block'">Sou um aluno</button> <button id="botaoProfessor" class="botaoProfessor"  onclick="document.getElementById('frmLoginAlunoSection').style.display='none';document.getElementById('frmLoginProfessorSection').style.display='block'">Sou um professor</button>
                <section id="frmLoginAlunoSection">
                <article id="loginAluno">
                    <h2 class="titulosLogin">Sou um aluno</h2>

                    <p class="textosLogin">O aluno do Vestibulário tem acesso a conteúdos de diversas matérias, exercícios e
                        provas de vestibulares anteriores.</p>
                    <form class="formsLogin" id="formAluno" method="post" name="frmLoginAluno" action="loginA.php">
                        <label>E-mail</label>
                        <input type="email" name="email" pattern="[A-Za-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required="on" title="Email Inv�lido" placeholder="Digite seu email" class="inputEmail">

                        <label class="frmSenha">Senha</label>
                        <input type="password" class="inputSenhaLogin" pattern=".{6,}" name="senha" required="on" placeholder="Digite sua senha">
                        <div class="botao">
                            <input type="submit"  value="Enviar" class="submitLogin"> 
                        </div>
                    </form>
                    <ul class="loginLinks">
                        <li class="menuLogin"><a href="esqueceuSenhaA.php" class="aLogin">Esqueci a senha</a></li> 
                        <li class="menuLogin"><a href="#" class="aLogin" id="modalA">Vou me registrar</a></li>
                        <!--------------------------------------------------------------JANELA MODAL ALUNO----------------------------------------------->
                        <div class="janelaModalA">
                            <div class="modalTitulo"> </div>

                            <div class="conteudoModalA">
<div class="FecharModal"></div>
                                <h2 id="titulo_login">Novo Aluno(a)</h2>
                                <section>
                                    <form id="cadastroBasicoA" action="CadastroA.php" method="post" enctype="multipart/form-data" name="cadastro" onsubmit="Checa()">


                                        <p class="ImagemPerfilModal">
                                            <input id="selecao-arquivo" class="inputCadastroBasico" type="file" name="foto" onchange="readURL(this);"/>
                                            <img id="aluno" src="imagem/defaultSexo.png" alt="your image" />
                                        <p></p>
                                        <label for="sexoMascA" class="masculino" id="Masculino" onclick="mudaSexo(this);">Masculino</label>
                                        <label for="sexoFemA" class="feminino" id="Feminino" onclick="mudaSexo(this);">Feminino</label>
                                        </p>

                                        <label class="labelCadastroBasico" id="labelImgModal" for="selecao-arquivo">Alterar foto de perfil</label>


                                        <div id="colunaModalA">

                                            <label class="labelCadastroBasico" for="nome">Nome </label>
                                            <input class="inputCadastroBasico" name="nomeA" minlength="3" maxlength="20" type="text" required="on" placeholder="Digite seu nome">

                                            <label class="labelCadastroBasico" for="email" >E-mail </label>
                                            <input class="inputCadastroBasico" name="emailA" maxlength="45" type="email" required="on" placeholder="Digite seu email" pattern="[A-Za-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" title="Email Inv�lido">

                                            <label class="labelCadastroBasico" for="senha">Senha </label>
                                            <input class="inputCadastroBasico" id="senha" maxlength="60" name="senhaA" type="password" required placeholder="Digite sua senha" pattern=".{6,}">
                                            <p></p>
                                            <label class="labelCadastroBasico" for="sobrenome">Sobrenome </label>
                                            <input class="inputCadastroBasico" name="sobrenomeA" maxlength="30" type="text" placeholder="Digite seu sobrenome">

                                            <label class="labelCadastroBasico" id="labelData" for="dataNasc">Data de nascimento</label>
                                            <input class="inputCadastroBasico" id="inputData" name="dataNascA" type="date" required="on" id="input_data" min="1926-01-01" max="2016-01-01">

                                            <label class="labelCadastroBasico" for="confSenha" >Confirmação de senha </label>
                                            <input class="inputCadastroBasico" id="confSenha"  name="confSenha" type="password" required pattern=".{6,}" placeholder="Confirme sua senha" oninput="validaSenha(this)">


                                            <input class="inputCadastroBasico" id="sexoMascA" required="on" name="sexoA" type="radio" src="imagem/perfilH.png" value="Masculino" class="botao_sexo">
                                            <input class="inputCadastroBasico" id="sexoFemA" required="on" name="sexoA" type="radio" src="imagem/perfilM.png" value="Feminino" class="botao_sexo">

                                        </div>

                                        <div id="recaptcha1"></div>


                                        <p>
                                             <input class="concordo" name="termos" required="on" type="checkbox">Eu li e concordo com os <a href="TermosI.php"> termos de uso</a> e <a href="PrivacidadeI.php">privacidade</a>
                                        </p>
                                        <p>
                                            <input type="submit"  value="Cadastrar" name="cadastrar" class="submitCadastro">
                                        </p>
                                    </form>

                            </div>
                        </div>

                    </ul>
                    
                </article>
</section><section id="frmLoginProfessorSection">
                <article id="loginProfessor">
                    <h2 class="titulosLogin">Sou um professor</h2>

                    <p class="textosLogin">O professor do Vestibulário pode
                        criar novas atividades para serem realizadas pelos alunos.</p>


                    <form class="formsLogin" id="formProf" method="post" name="frmLoginAluno" action="loginP.php">
                        <label>E-mail</label>
                        <input type="email" name="email" required="on" pattern="[A-Za-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" placeholder="Digite seu email" class="inputEmail">

                        <label class="frmSenha">Senha</label>
                        <input type="password" class="inputSenhaLogin" pattern=".{6,}" required="on" name="senha" placeholder="Digite sua senha">
                        <div class="botao">
                            <input type="submit"  value="Enviar" class="submitLogin">
                        </div>
                    </form>
                    <ul class="loginLinks">
                        <li class="menuLogin"><a href="esqueceuSenhaP.php" class="aLogin">Esqueci a senha</a></li>
                        <li class="menuLogin"><a href="#" class="aLogin" id="modalP">Vou me registrar</a></li>

                        <!--------------------------------------------------------------JANELA MODAL PROFESSOR----------------------------------------------->
                        <div class="janelaModalP">
                            <div class="modalTitulo"></div>

                            <div class="conteudoModalP">
<div class="FecharModal"></div>
                                <h2 id="titulo_login">Novo Professor(a)</h2>
                                <section>
                                    <form id="cadastroBasico" action="CadastroP.php" method="post" enctype="multipart/form-data" name="cadastro" onsubmit="Checa()">

                                        <p class="ImagemPerfilModal">
                                            <input id="selecao-arquivo" class="inputCadastroBasico" type="file" name="foto" onchange="readURL(this);"/>
                                            <img id="prof" src="imagem/defaultSexo.png" alt="your image" />
                                        <p></p>
                                        <label for="sexoMascP" class="masculino" id="MasculinoP" onclick="mudaSexoP(this);">Masculino</label>
                                        <label for="sexoFemP" class="feminino" id="FemininoP" onclick="mudaSexoP(this);">Feminino</label>
                                        </p>
                                        <label class="labelCadastroBasico" id="labelImgModalP" for="selecao-arquivo">Alterar foto de perfil</label>

                                        <div id="colunaModalP">

                                            <label class="labelCadastroBasico" for="nome">Nome </label>
                                            <input class="inputCadastroBasico" maxlength="12" name="nomeP" required="on" type="text" placeholder="Digite seu nome">

                                            <label class="labelCadastroBasico" for="email" >E-mail </label>
                                            <input class="inputCadastroBasico" name="emailP" type="email" pattern="[A-Za-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required="on" placeholder="Digite seu email">

                                            <label class="labelCadastroBasico" for="senha">Senha </label>
                                            <input class="inputCadastroBasico" id="senhaP" name="senhaP" type="password" required="on" pattern=".{6,}" title="Seis ou mais caracteres no m�nimo" placeholder="Digite sua senha">

                                            <label class="labelCadastroBasico" for="sobrenome">Sobrenome </label>
                                            <input class="inputCadastroBasico" maxlength="12" name="sobrenomeP" type="text" placeholder="Digite seu sobrenome">

                                            <label class="labelCadastroBasico" for="cpf">CPF</label>
                                            <input class="inputCadastroBasico" id="cpf" type="text" name="cpfP" required="on" placeholder="Digite seu CPF" pattern="\d{3}\.\d{3}\.\d{3}-\d{2}">

                                            <label class="labelCadastroBasico" for="confSenha" >Confirmação de senha </label>
                                            <input class="inputCadastroBasico" id="confSenhaP" name="confSenha" type="password" required="on" pattern=".{6,}" title="Seis ou mais caracteres no m�nimo" placeholder="Confirme sua senha" oninput="validaSenhaP(this)">

                                        </div>

                                        <input class="inputCadastroBasico" id="sexoMascP" name="sexoP" type="radio" src="imagem/perfilH.png" value="Masculino">
                                        <input class="inputCadastroBasico" id="sexoFemP" name="sexoP" type="radio" src="imagem/perfilM.png" value="Feminino">

                                        <div id="dataModalP">

                                            <label class="labelCadastroBasico" for="dataNasc" id="labelDataP">Data de nascimento</label>
                                            <input class="inputCadastroBasico" name="dataNascP" required="on" type="date" required="on"  min="1926-01-01" max="2016-01-01" id="inputDataP">

                                        </div>

                                        <div id="recaptcha2"></div>

                                        <p>
                                             <input class="concordo" name="termos" required="on" type="checkbox">Eu li e concordo com os <a href="TermosI.php"> termos de uso</a> e <a href="PrivacidadeI.php">privacidade</a>
                                        </p>
                                        <p>
                                            <input type="submit"  value="Cadastrar" name="cadastrar" class="submitCadastro">
                                        </p>
                                    </form>

                            </div>
                        </div>

                    </ul>  
               
                </article>
    </section>
            </section>        </div>

        <footer id="rodape_fixo2">
            <a href="sobreNosI.php"><img src="imagem/logoV.png" id="img_rodape" width="80px" height="70px" alt="#"/></a>

            <a href="TermosI.php" class="link_rodape">Termos de uso</a>
            <a href="PrivacidadeI.php" class="link_rodape">Privacidade</a>
            <a href="sobreNosI.php" class="link_rodape">Sobre Nós</a>
        </footer>
    </body> 
</div>

</html>


			