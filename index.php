<?php
session_start();
if (empty($_SESSION)) {
    header("location: frmLogin.php?erro=11");
}

if ($_SESSION['ativo_usuario'] == FALSE) {
    session_destroy();
    header('location:frmLogin.php?erro=10');
}
if (isset($_GET['erro']) && $_GET['erro'] != "") {
    $erro = $_GET['erro'];
}

if (isset($_GET['sucesso']) && $_GET['sucesso'] != "") {
    $sucesso= $_GET['sucesso'];

}

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Vestibulario</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="O Vestibulário é o site de estudos para vestibular que conta com professores de todo o Brasil, prontos para te ajudar a entrar na tão sonhada faculdade!">
        <meta name="keywords" content="vestibular, estudar, universidade, gratis, faculdade, materia, enem, fuvest, simulado, exercicios, curso, aprender, passar, conteudo, aula, disciplina, online">
        <meta name="author" content="Vanguart">
        <link rel="shortcut icon" href="imagem/fav-icon-vestibulario.png">
        <link href="css/css_reset.css" rel="stylesheet"/>
        <link href="css/styleCont.css" rel="stylesheet"/>
        <link rel="stylesheet" media="screen and (max-width:319px)" href="css/style_319px.css">
        <link rel="stylesheet" media="screen and (min-width:320px) and (max-width:479px)" href="css/style_320px.css">
        <link rel="stylesheet" media="screen and (min-width:480px) and (max-width:599px)" href="css/style_480px.css">
        <link rel="stylesheet" media="screen and (min-width:600px) and (max-width:767px)" href="css/style_600px.css">
        <link rel="stylesheet" media="screen and (min-width:768px) and (max-width:1023px)" href="css/style_768px.css">
        <link rel="stylesheet" media="screen and (min-width:1024px) and (max-width:1199px)" href="css/style_1024px.css">
        <link rel="stylesheet" media="screen and (min-width:1200px)" href="css/style_1200px.css">     
        <script src="javascript/cheet.js/namuol-cheet.js-7facc4f/cheet.js" type="text/javascript"></script>
        <script src="javascript/jquery-3.1.0.js" type="text/javascript"></script>
        <script src="javascript/modal.js"></script>
        <script src="javascript/js.js"></script>
        <script src="javascript/jsCont.js"></script>
        <script>
function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}    
        </script>
    </head>

    <body id="body" onload="draw()">
        <div id="container">
                 <?php 
                    if($erro!=''||$sucesso!=''){
                    include_once 'modalSucesso.php';
                    }?> 
            <header id="header_fixo">
                <nav id="menu_logo">
                 
                    <a href="?p=home"><img src="imagem/logo.png" alt="" id="logo_fixo"/></a>
                    <div id="menu_fixo">
                       
                        <a href="?p=perfil" id="perfilResponsivo"><img src="<?php echo $_SESSION["url_foto_usuario"]; ?>" id="img_perfil" width="50px" height="50px" alt="#"/></a>
                        <a href="?p=home" class="link_header"><img src="imagem/home.png" class="img_header" alt=""/></a>                       
                        <a href="?p=exercicios" class="link_header"><img src="imagem/exercicio.png" class="img_header"  alt=""/></a>
                        <a href="?p=vestibulares" class="link_header"><img src="imagem/vestibular.png" class="img_header"  alt=""/></a>                        
                    </div>
                    <div id="someMenuResp">
                    <li class="icon">
                        <a href="javascript:void(0);" onclick="myFunction()"> 
                            <img class="botaoMenuResp" src="imagem/mobile_menu.png" width="30px" height="30px" style="float: left;"/> 
                        </a>
                        
                    </li>
                    <ul class="topnav" id="myTopnav">
                        <li><a href="?p=home">Home</a></li>
                        <li><a href="?p=exercicios">Exercícios</a></li>
                        <li><a href="?p=simulado">Simulados</a></li>
                        <li><a href="?p=vestibulares">Vestibulares</a></li>
                            <?php
                            if ($_SESSION['tipo_usuario'] == "3") {
                                echo '<li><a href="?p=areaAdm">Área Administrativa</a></li>';
                            }
                            ?>
                        <li><a href="logout.php">Sair</a></li>

                    </ul>
                    </div>
                </nav>
            </header>
            <div id="content">
                <nav id="lateral_fixo">
                    <a href="?p=perfil"><img src="<?php echo $_SESSION["url_foto_usuario"]; ?>" id="img_perfil" width="125px" height="125px" alt="#"/></a>
                    <div id="menu_lateral">
                        <ul id="lista_lateral">
                            <li class="lista_lateral" id="lateral_nome"><a href="?p=perfil" class="lateral_estilo" id="nome_user"><?php echo $_SESSION['nome_usuario'] . " " . $_SESSION['sobrenome_usuario'] ?></a></li>
                            <li class="lista_lateral"><a href="?p=simulado" class="lateral_estilo">Simulados</a></li>
                            <?php
                            if ($_SESSION['tipo_usuario'] == "3") {
                                echo '<li class="lista_lateral"><a href="?p=areaAdm" class="lateral_estilo">Área Administrativa</a></li>';
                            }
                            ?>
                            <li class="lista_lateral"><a href="logout.php" class="lateral_estilo">Sair</a></li>

                        </ul>

      <?php  if ($_SESSION['tipo_usuario'] == "1" ||$_SESSION['tipo_usuario'] == "2"){
        echo "<canvas id=\"contador\" width=\"200\" height=\"300\">
        </canvas>
<h2 id=\"tituloEnem\">ENEM</h2>
        <h3 id=\"textoContador\">
             <script language=\"javaScript\">
                var data1 = new Date(); // data atual mes/dia/ano
                var data2 = new Date(\"11/05/2016\"); //data alvo mes/dia/ano
                var timeDiff = Math.abs(data2.getTime() - data1.getTime());
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

                
            </script>       
       </h3>";
}?>


                    </div>
                </nav>	
                <?php
                if (isset($_GET['p'])) {
                    $p = $_GET['p'];
                    include_once 'view/' . $p . '.php';
                } else {
                    include_once 'view/home.php';
                }
                ?>  
            </div>

            <footer id="rodape_fixo">
                <a href="?p=sobreNos"><img src="imagem/logoV.png" id="img_rodape" width="80px" height="70px" alt="#"/></a>

                <a href="?p=termos" class="link_rodape">Termos de uso</a>
                <a href="?p=privacidade" class="link_rodape">Privacidade</a>
                <a href="?p=sobreNos" class="link_rodape">Sobre Nós</a>

            </footer>
    </body>   
</html>				