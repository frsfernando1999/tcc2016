function draw() {
    var data1 = new Date(); // data atual mes/dia/ano
    var data2 = new Date("11/05/2016"); //data alvo mes/dia/ano
    var timeDiff = Math.abs(data2.getTime() - data1.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    var diffDaysPadrao = (diffDays * 2) / 365;
//    alert(diffDaysPadrao); // quantos px esse dia vale no circle
    var canvas = document.getElementById('contador');
    if (canvas.getContext) {
        var context = canvas.getContext('2d');

        //circulo
        context.lineWidth = 10;
        context.strokeStyle = "white";
        context.fillStyle = "white";
        context.beginPath();
        var x = 90;   // = 128/2 - centraliza o circulo
        var y = 180;
        var radius = 64;  //raio do circulo = diametro/2
        var anticlockwise = true;
        var startAngle = 4.78;     //inicia o arco na posi��o 0 graus (direita)
        var endAngle = Math.PI * diffDaysPadrao; //termina o arco na posi��o 360 graus (volta completa)
        context.arc(x, y, radius, startAngle, endAngle, anticlockwise);
        context.stroke(); //desenha a borda
        context.font="20px Verdana";
        context.fillText(diffDays,55,190);
        context.fillText("  dias",75,190);
    }
}





    

