<?php

session_start();
require_once './core/Usuario.class.php';
$usuario = new Usuario();

$error = array();
$email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
$email = filter_var($email, FILTER_SANITIZE_EMAIL);
$senha = filter_input(INPUT_POST, 'senha', FILTER_SANITIZE_STRING);

$row = $usuario->select("and email_usuario='$email' and senha_usuario='$senha'");
$total = count($row);

if ($senha == '' || $email == '') {
    $error[0] = "Todos os campos devem estar preenchidos";
    header('location:frmLogin.php?erro=1');
}

if($row[0]['acesso_usuario']!='P'){
    $error[6]='Algo deu errado!';
    header('location:frmLogin.php?erro=1');
}



if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $error[1] = "E-mail Inválido";
    header('location:frmLogin.php?erro=1');
}

if ($total == 0) {
    $error[2] = "Email ou senha errados.";
    header('location:frmLogin.php?erro=1');
}

if (count($error) == 0) {
    if (isset($email) && isset($senha)) {

        $_SESSION["cod_usuario"] = $row[0]['cod_usuario'];
        $_SESSION["nome_usuario"] = $row[0]['nome_usuario'];
        $_SESSION["sobrenome_usuario"] = $row[0]['sobrenome_usuario'];
        $_SESSION["senha_usuario"] = $row[0]['senha_usuario'];
        $_SESSION["url_foto_usuario"] = $row[0]['url_foto_usuario'];
        $_SESSION["data_nasc_usuario"] = $row[0]['data_nasc_usuario'];
        $_SESSION["acesso_usuario"] = $row[0]['acesso_usuario'];
        $_SESSION["email_usuario"] = $row[0]['email_usuario'];
        $_SESSION["cpf_usuario"] = $row[0]['cpf_usuario'];
        $_SESSION["tipo_usuario"] = $row[0]['tipo_usuario'];
        $_SESSION["ativo_usuario"] = $row[0]['ativo_usuario'];
        $_SESSION["sexo_usuario"] = $row[0]['sexo_usuario'];
        header("location: index.php");
    }
}if (count($error) != 0) {
    foreach ($error as $erro) {
        echo $erro . "<br />";
    }
}
