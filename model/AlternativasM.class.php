<?php
require_once 'db/DbConnection.class.php';

class AlternativasM extends DbConnection{
    
    private $cod_alter;
    private $alter_a;
    private $alter_b;
    private $alter_c;
    private $alter_d;
    private $alter_e;
    private $cod_resp;
    private $titulo_alter;
    private $cod_questao;

    function getCod_alter() {
        return $this->cod_alter;
    }

    function getAlter_a() {
        return $this->alter_a;
    }

    function getAlter_b() {
        return $this->alter_b;
    }

    function getAlter_c() {
        return $this->alter_c;
    }

    function getAlter_d() {
        return $this->alter_d;
    }

    function getAlter_e() {
        return $this->alter_e;
    }

    function getCod_resp() {
        return $this->cod_resp;
    }

    function getTitulo_alter() {
        return $this->titulo_alter;
    }

    function getCod_questao() {
        return $this->cod_questao;
    }

    function setCod_alter($cod_alter) {
        $this->cod_alter = $cod_alter;
    }

    function setAlter_a($alter_a) {
        $this->alter_a = $alter_a;
    }

    function setAlter_b($alter_b) {
        $this->alter_b = $alter_b;
    }

    function setAlter_c($alter_c) {
        $this->alter_c = $alter_c;
    }

    function setAlter_d($alter_d) {
        $this->alter_d = $alter_d;
    }

    function setAlter_e($alter_e) {
        $this->alter_e = $alter_e;
    }

    function setCod_resp($cod_resp) {
        $this->cod_resp = $cod_resp;
    }

    function setTitulo_alter($titulo_alter) {
        $this->titulo_alter = $titulo_alter;
    }

    function setCod_questao($cod_questao) {
        $this->cod_questao = $cod_questao;
    }


    
}
