<?php

require_once 'db/DbConnection.class.php';

class AnexosMsgM extends DbConnection{
    
    private $cod_anexo_msg;
    private $data_msg;
    private $cod_msg;
    private $url_anexo_msg;
    private $titulo_anexo;
    
    function getCod_anexo_msg() {
        return $this->cod_anexo_msg;
    }

    function getData_msg($us = FALSE) {
       if($us == TRUE){
          return $this->data_msg;    
       }  else {
           return $this->dateTimeToBr($this->data_msg);
       } 
    }

    function getCod_msg() {
        return $this->cod_msg;
    }

    function getUrl_anexo_msg() {
        return $this->url_anexo_msg;
    }

    function getTitulo_anexo() {
        return $this->titulo_anexo;
    }

    function setCod_anexo_msg($cod_anexo_msg) {
        $this->cod_anexo_msg = $cod_anexo_msg;
    }

    function setData_msg($data_msg) {
        $this->data_msg = $this->dateTimeToUs($data_msg);
    }

    function setCod_msg($cod_msg) {
        $this->cod_msg = $cod_msg;
    }

    function setUrl_anexo_msg($url_anexo_msg) {
        $this->url_anexo_msg = $url_anexo_msg;
    }

    function setTitulo_anexo($titulo_anexo) {
        $this->titulo_anexo = $titulo_anexo;
    }


}