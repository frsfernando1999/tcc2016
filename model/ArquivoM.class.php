<?php


require_once 'db/DbConnection.class.php';

class ArquivoM extends DbConnection{
    
    private $cod_arq;
    private $url_arq;
    private $legenda_arq;
    private $tipo_arq;
    private $topico_disciplina_cod_topico;
    
    function getCod_arq() {
        return $this->cod_arq;
    }

    function getUrl_arq() {
        return $this->url_arq;
    }

    function getLegenda_arq() {
        return $this->legenda_arq;
    }

    function getTipo_arq() {
        return $this->tipo_arq;
    }

    function getTopico_disciplina_cod_topico() {
        return $this->topico_disciplina_cod_topico;
    }

    function setCod_arq($cod_arq) {
        $this->cod_arq = $cod_arq;
    }

    function setUrl_arq($url_arq) {
        $this->url_arq = $url_arq;
    }

    function setLegenda_arq($legenda_arq) {
        $this->legenda_arq = $legenda_arq;
    }

    function setTipo_arq($tipo_arq) {
        $this->tipo_arq = $tipo_arq;
    }

    function setTopico_disciplina_cod_topico($topico_disciplina_cod_topico) {
        $this->topico_disciplina_cod_topico = $topico_disciplina_cod_topico;
    }


}
