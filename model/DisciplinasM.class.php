<?php

require_once 'db/DbConnection.class.php';

class DisciplinasM extends DbConnection{
    
    private $cod_disc;
    private $nome_disc;
    private $img_disc;
    
    function getImg_disc() {
        return $this->img_disc;
    }

    function setImg_disc($img_disc) {
        $this->img_disc = $img_disc;
    }

                
    function getCod_disc() {
        return $this->cod_disc;
    }

    function getNome_disc() {
        return $this->nome_disc;
    }



    function setCod_disc($cod_disc) {
        $this->cod_disc = $cod_disc;
    }

    function setNome_disc($nome_disc) {
        $this->nome_disc = $nome_disc;
    }



}

