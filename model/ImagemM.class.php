<?php

require_once 'db/DbConnection.class.php';

class ImagemM extends DbConnection{
    private $cod_img;
    private $url_arq;
    private $legenda_arq;
    private $cod_questao;
    
    function getCod_img() {
        return $this->cod_img;
    }

    function getUrl_arq() {
        return $this->url_arq;
    }

    function getLegenda_arq() {
        return $this->legenda_arq;
    }

    function getCod_questao() {
        return $this->cod_questao;
    }

    function setCod_img($cod_img) {
        $this->cod_img = $cod_img;
    }

    function setUrl_arq($url_arq) {
        $this->url_arq = $url_arq;
    }

    function setLegenda_arq($legenda_arq) {
        $this->legenda_arq = $legenda_arq;
    }

    function setCod_questao($cod_questao) {
        $this->cod_questao = $cod_questao;
    }


    
    
}
