<?php

require_once 'db/DbConnection.class.php';

class ListaExerciciosM extends DbConnection{
    
    private $cod_lista;
    private $titulo_lista;
    private $status_exclusao_lista;
    private $data_inclusao_lista;
    private $tipo_lista;
    private $default_lista;
    private $cod_usuario;
    private $cod_topico;
    
    function getDefault_lista() {
        return $this->default_lista;
    }

    function setDefault_lista($default_lista) {
        $this->default_lista = $default_lista;
    }

        
    function getTipo_lista() {
        return $this->tipo_lista;
    }

    function setTipo_lista($tipo_lista) {
        $this->tipo_lista = $tipo_lista;
    }

    function getCod_lista() {
        return $this->cod_lista;
    }

    function getTitulo_lista() {
        return $this->titulo_lista;
    }

    function getStatus_exclusao_lista() {
        return $this->status_exclusao_lista;
    }

    function getData_inclusao_lista($us = FALSE) {
       if($us == TRUE){
          return $this->data_inclusao_lista;    
       }  else {
           return $this->dateTimeToBr($this->data_inclusao_lista);
    }}

    function getCod_usuario() {
        return $this->cod_usuario;
    }

    function getCod_topico() {
        return $this->cod_topico;
    }

    function setCod_lista($cod_lista) {
        $this->cod_lista = $cod_lista;
    }

    function setTitulo_lista($titulo_lista) {
        $this->titulo_lista = $titulo_lista;
    }

    function setStatus_exclusao_lista($status_exclusao_lista) {
        $this->status_exclusao_lista = $status_exclusao_lista;
    }

    function setData_inclusao_lista($data_inclusao_lista) {
        $this->data_inclusao_lista = $this->dateTimeToUs($data_inclusao_lista);
    }

    function setCod_usuario($cod_usuario) {
        $this->cod_usuario = $cod_usuario;
    }

    function setCod_topico($cod_topico) {
        $this->cod_topico = $cod_topico;
    }


}