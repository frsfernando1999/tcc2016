<?php

require_once 'db/DbConnection.class.php';

class MensagensM extends DbConnection{
    
    private $cod_msg;
    private $data_msg;
    private $status_exclusao_msg;
    private $titulo_msg;
    private $texto_msg;
    private $cod_usuario_orig;
    private $cod_usuario_dest;
    
    function getCod_msg() {
        return $this->cod_msg;
    }

    function getData_msg($us = FALSE) {
       if($us == TRUE){
          return $this->data_msg;    
       }  else {
           return $this->dateTimeToBr($this->data_msg);
       }    
    }

    function getStatus_exclusao_msg() {
        return $this->status_exclusao_msg;
    }

    function getTitulo_msg() {
        return $this->titulo_msg;
    }

    function getTexto_msg() {
        return $this->texto_msg;
    }

    function getCod_usuario_orig() {
        return $this->cod_usuario_orig;
    }

    function getCod_usuario_dest() {
        return $this->cod_usuario_dest;
    }

    function setCod_msg($cod_msg) {
        $this->cod_msg = $cod_msg;
    }

    function setData_msg($data_msg) {
        $this->data_msg = $this->dateTimeToUs($data_msg);
    }

    function setStatus_exclusao_msg($status_exclusao_msg) {
        $this->status_exclusao_msg = $status_exclusao_msg;
    }

    function setTitulo_msg($titulo_msg) {
        $this->titulo_msg = $titulo_msg;
    }

    function setTexto_msg($texto_msg) {
        $this->texto_msg = $texto_msg;
    }

    function setCod_usuario_orig($cod_usuario_orig) {
        $this->cod_usuario_orig = $cod_usuario_orig;
    }

    function setCod_usuario_dest($cod_usuario_dest) {
        $this->cod_usuario_dest = $cod_usuario_dest;
    }


    
    
}
