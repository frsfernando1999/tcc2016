<?php

require_once 'db/DbConnection.class.php';

class QuestoesM extends DbConnection{
    
    private $cod_questao;
    private $enunciado_ex_obj;
    private $obs_questao;
    private $lista_exercicios_cod_lista;
    private $img_questao;
    
    function getImg_questao() {
        return $this->img_questao;
    }

    function setImg_questao($img_questao) {
        $this->img_questao = $img_questao;
    }

                
    function getCod_questao() {
        return $this->cod_questao;
    }

    function getEnunciado_ex_obj() {
        return $this->enunciado_ex_obj;
    }

    function getObs_questao() {
        return $this->obs_questao;
    }

    function getLista_exercicios_cod_lista() {
        return $this->lista_exercicios_cod_lista;
    }

    function setCod_questao($cod_questao) {
        $this->cod_questao = $cod_questao;
    }

    function setEnunciado_ex_obj($enunciado_ex_obj) {
        $this->enunciado_ex_obj = $enunciado_ex_obj;
    }


    function setObs_questao($obs_questao) {
        $this->obs_questao = $obs_questao;
    }

    function setLista_exercicios_cod_lista($lista_exercicios_cod_lista) {
        $this->lista_exercicios_cod_lista = $lista_exercicios_cod_lista;
    }


}
