<?php

require_once 'db/DbConnection.class.php';

class RamoDisciplinaM extends DbConnection{
    
    private $cod_ramo;
    private $nome_ramo;
    private $disciplinas_cod_disc;
    
    
    function getCod_ramo() {
        return $this->cod_ramo;
    }

    function getNome_ramo() {
        return $this->nome_ramo;
    }

    function getDisciplinas_cod_disc() {
        return $this->disciplinas_cod_disc;
    }

    function setCod_ramo($cod_ramo) {
        $this->cod_ramo = $cod_ramo;
    }

    function setNome_ramo($nome_ramo) {
        $this->nome_ramo = $nome_ramo;
    }

    function setDisciplinas_cod_disc($disciplinas_cod_disc) {
        $this->disciplinas_cod_disc = $disciplinas_cod_disc;
    }


}

