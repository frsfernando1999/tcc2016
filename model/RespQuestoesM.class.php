<?php

require_once 'db/DbConnection.class.php';

class RespQuestoesM extends DbConnection{
    
    private $cod_usuario;
    private $cod_questao;
    private $resp_user;
    private $data_resp_user;
    
    function getCod_usuario() {
        return $this->cod_usuario;
    }

    function getCod_questao() {
        return $this->cod_questao;
    }

    function getResp_user() {
        return $this->resp_user;
    }

    function getData_resp_user($us = FALSE) {
       if($us == TRUE){
          return $this->data_resp_user;    
       }  else {
           return $this->dateTimeToBr($this->data_resp_user);
    }}

    function setCod_usuario($cod_usuario) {
        $this->cod_usuario = $cod_usuario;
    }

    function setCod_questao($cod_questao) {
        $this->cod_questao = $cod_questao;
    }

    function setResp_user($resp_user) {
        $this->resp_user = $resp_user;
    }

    function setData_resp_user($data_resp_user) {
        $this->data_resp_user = $this->dateTimeToUs($data_resp_user);
    }




    
}
