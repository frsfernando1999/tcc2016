<?php

require_once 'db/DbConnection.class.php';

class TopicoDisciplinaM extends DbConnection{
    
    private $cod_topico;
    private $nome_topico;
    private $texto_topico;
    private $cod_ramo;
    private $cod_usuario; //ex: usuario_cod_usuario
    private $apresentacao_topico;
    
    function getApresentacao_topico() {
        return $this->apresentacao_topico;
    }

    function setApresentacao_topico($apresentacao_topico) {
        $this->apresentacao_topico = $apresentacao_topico;
    }
            
    function getCod_topico() {
        return $this->cod_topico;
    }

    function getNome_topico() {
        return $this->nome_topico;
    }

    function getTexto_topico() {
        return $this->texto_topico;
    }

    function getCod_ramo() {
        return $this->cod_ramo;
    }

    function getCod_usuario() {
        return $this->cod_usuario;
    }

    function setCod_topico($cod_topico) {
        $this->cod_topico = $cod_topico;
    }

    function setNome_topico($nome_topico) {
        $this->nome_topico = $nome_topico;
    }

    function setTexto_topico($texto_topico) {
        $this->texto_topico = $texto_topico;
    }

    function setCod_ramo($cod_ramo) {
        $this->cod_ramo = $cod_ramo;
    }

    function setCod_usuario($cod_usuario) {
        $this->cod_usuario = $cod_usuario;
    }


}
