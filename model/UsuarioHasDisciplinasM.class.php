<?php

require_once 'db/DbConnection.class.php';

class UsuarioHasDisciplinasM extends DbConnection{
    
    private $cod_usuario;
    private $cod_disc;
    
    function getCod_usuario() {
        return $this->cod_usuario;
    }

    function getCod_disc() {
        return $this->cod_disc;
    }

    function setCod_usuario($cod_usuario) {
        $this->cod_usuario = $cod_usuario;
    }

    function setCod_disc($cod_disc) {
        $this->cod_disc = $cod_disc;
    }


}
