<?php

require_once 'db/DbConnection.class.php';

 class UsuarioM extends DbConnection{

    private $cod_usuario;
    private $nome_usuario;
    private $sobrenome_usuario;
    private $senha_usuario;
    private $url_foto_usuario;
    private $data_nasc_usuario;
    private $acesso_usuario;
    private $email_usuario;
    private $cpf_usuario;
    private $tipo_usuario;
    private $ativo_usuario;
    private $sexo_usuario;
    
    function getSexo_usuario() {
        return $this->sexo_usuario;
    }

    function setSexo_usuario($sexo_usuario) {
        $this->sexo_usuario = $sexo_usuario;
    }

        
    function getAtivo_usuario() {
        return $this->ativo_usuario;
    }

    function setAtivo_usuario($ativo_usuario) {
        $this->ativo_usuario = $ativo_usuario;
    }
    function getTipo_usuario() {
    return $this->tipo_usuario;
    }

    function setTipo_usuario($tipo_usuario) {
        $this->tipo_usuario = $tipo_usuario;
    }

        function getCod_usuario() {
        return $this->cod_usuario;
    }

    function getNome_usuario() {
        return $this->nome_usuario;
    }

    function getSobrenome_usuario() {
        return $this->sobrenome_usuario;
    }

    function getSenha_usuario() {
        return $this->senha_usuario;
    }

    function getUrl_foto_usuario() {
        return $this->url_foto_usuario;
    }

    function getData_nasc_usuario($us = FALSE) {
       if($us == TRUE){
          return $this->data_nasc_usuario;    
       }  else {
           return $this->dateToBr($this->data_nasc_usuario);
       }
   }    

    function getAcesso_usuario() {
        return $this->acesso_usuario;
    }

    function getEmail_usuario() {
        return $this->email_usuario;
    }

    function getCpf_usuario() {
        return $this->cpf_usuario;
    }

    function setCod_usuario($cod_usuario) {
        $this->cod_usuario = $cod_usuario;
    }

    function setNome_usuario($nome_usuario) {
        $this->nome_usuario = $nome_usuario;
    }

    function setSobrenome_usuario($sobrenome_usuario) {
        $this->sobrenome_usuario = $sobrenome_usuario;
    }

    function setSenha_usuario($senha_usuario) {
        $this->senha_usuario = $senha_usuario;
    }

    function setUrl_foto_usuario($url_foto_usuario) {
        $this->url_foto_usuario = $url_foto_usuario;
    }

    function setData_nasc_usuario($data_nasc_usuario, $us = FALSE) {
        if($us == TRUE){
        $this->data_nasc_usuario = $data_nasc_usuario;}
        else{
    $this->data_nasc_usuario = $this->dateToUs($data_nasc_usuario);
        }
    }

    function setAcesso_usuario($acesso_usuario) {
        $this->acesso_usuario = $acesso_usuario;
    }

    function setEmail_usuario($email_usuario) {
        $this->email_usuario = $email_usuario;
    }

    function setCpf_usuario($cpf_usuario) {
        $this->cpf_usuario = $cpf_usuario;
    }


 }

    
