<?php

session_start();
require_once 'core/Questoes.class.php';
require_once 'core/ListaExercicios.class.php';
require_once 'core/Alternativas.class.php';
require_once 'core/RespQuestoes.class.php';

$lista = new ListaExercicios();
$questao = new Questoes();
$alter = new Alternativas();
$resps= new RespQuestoes();

$cod_usuario = $_SESSION['cod_usuario'];
$tipo_lista=$_GET['tipo'];
$data_resp = $data_atual;
$codLista = $_GET['lista'];
if($tipo_lista == 'O'){
    $total=$lista->selectQuestoes("and lista_exercicios.cod_lista='$codLista'");
    
    foreach ($total as $codigo){
        $codEx[] = $codigo['cod_questao'];
    }
    
    $exResp[] = $_POST['Q0resp'];
    $exResp[] = $_POST['Q1resp'];
    $exResp[] = $_POST['Q2resp'];
    $exResp[] = $_POST['Q3resp'];
    $exResp[] = $_POST['Q4resp'];
    $exResp[] = $_POST['Q5resp'];
    $exResp[] = $_POST['Q6resp'];
    $exResp[] = $_POST['Q7resp'];
    $exResp[] = $_POST['Q8resp'];
    $exResp[] = $_POST['Q9resp'];
    
    $i=0;
    $ja = $resps->select("and resp_questoes.cod_usuario='$cod_usuario' and resp_questoes.cod_questao='$codEx[0]'");
    if(empty($ja)){
    while ($i <= 9){
        $resps->setCod_usuario($_SESSION['cod_usuario']);
        $resps->setCod_questao($codEx[$i]);
        $resps->setResp_user($exResp[$i]);
        $resps->setData_resp_user($data_resp);
        $resps->insert();
        $i++;
        
    }
    header("location:index.php?p=resultadosExercicios&lista=$codLista");
    }elseif(!empty ($ja)){
            while ($i <= 9){
        $resps->setResp_user($exResp[$i]);
        $resps->setData_resp_user($data_resp);
        $resps->setCod_questao($codEx[$i]);
        $resps->setCod_usuario($_SESSION['cod_usuario']);
        $resps->update();
        $i++;

    }
    header("location:index.php?p=resultadosExercicios&lista=$codLista");
            }
            
            
}elseif($tipo_lista=='D'){
    $total=$lista->selectQuestoes("and lista_exercicios.cod_lista='$codLista'");
        foreach ($total as $codigo){
        $codEx[] = $codigo['cod_questao'];
    }

        foreach ($_POST['respExDiss'] as $dados) {
        $respExDiss[] = filter_var($dados, FILTER_SANITIZE_STRING);
    }

    $i=0;
    $ja = $resps->selectExDiss("and resp_questoes.cod_usuario='$cod_usuario' and resp_questoes.cod_questao='$codEx[0]'");
    if(empty($ja)){
    while ($i <= 9){
        $resps->setCod_usuario($_SESSION['cod_usuario']);
        $resps->setCod_questao($codEx[$i]);
        $resps->setResp_user($respExDiss[$i]);
        $resps->setData_resp_user($data_resp);
        $resps->insert();
        $i++;
        
    }
    header("location:index.php?p=resultadosExerciciosDiss&lista=$codLista");
    }elseif(!empty($ja)){
        
        while ($i <= 9){
        $resps->setResp_user($respExDiss[$i]);
        $resps->setData_resp_user($data_resp);
        $resps->setCod_questao($codEx[$i]);
        $resps->setCod_usuario($_SESSION['cod_usuario']);
        $resps->update();
        $i++;
    }
    header("location:index.php?p=resultadosExerciciosDiss&lista=$codLista");
            }
}