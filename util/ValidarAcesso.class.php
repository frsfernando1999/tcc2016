<?php

class ValidarAcesso{

    public static function verificarPermissao($arrayPermitidos) {
        if (!empty($_SESSION)) {
            if(!in_array($_SESSION['tipo_usuario'], $arrayPermitidos)){
                header('location: http://localhost/vestibulario_v1/index.php');
            }
        } else {
            header('location: http://localhost/vestibulario_v1/index.php');
        }
    }
}
