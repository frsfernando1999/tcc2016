<?php
$caminho = $_SERVER['PHP_SELF'];
if (stripos($caminho, 'view')) {
    header("location: ../index.php?p=perfil");
}
//Página com restrição
require_once 'util/ValidarAcesso.class.php';
ValidarAcesso::verificarPermissao(array(0 => 1, 1 => 2, 2 => 3));

require_once "core/Usuario.class.php";

$usuario = new Usuario;
$row = $usuario->select("and cod_usuario='" . $_SESSION['cod_usuario'] . "'", "");
?>

<main class="conteudo" id="conteudo_perfil_perfil">
    <?php
    ?>
    
    <h1 id="novaSenha">Alterar Perfil</h1>
    <form id="cadastroBasicoP" action="atualizarPerfil.php" method="post" enctype="multipart/form-data" name="atPerfil" onsubmit="Checa()">
        <p>
            <label class="labelCadastroBasicoP" for="nome">Nome </label>
            <input class="inputCadastroBasicoP" name="nome"  maxlength="12" minlength="3" type="text" <?php echo "value=\"" .  $row[0]['nome_usuario'] . "\""?> placeholder="Digite seu nome">
        </p>
        <p>
            <label class="labelCadastroBasicoP" for="sobrenome">Sobrenome </label>
            <input class="inputCadastroBasicoP" name="sobrenome" maxlength="12" type="text" <?php echo "value=\"" .  $row[0]['sobrenome_usuario'] . "\""?>  placeholder="Digite seu sobrenome">
        </p>
        <p>
            <label class="labelCadastroBasicoP" for="dataNasc">Data de nascimento </label>
            <input class="inputCadastroBasicoP" name="dataNasc" maxlength="12" type="date" min="1926-01-01" max="2016-01-01" <?php echo "value=\"" .  $row[0]['data_nasc_usuario'] . "\""?>>
        </p>
        <p>
            <label class="labelCadastroBasicoP" for="sexo">Sexo</label>
            <input class="inputCadastroBasicoP" name="sexo" type="radio" value="Masculino" <?php if($row[0]['sexo_usuario']=="Masculino"){echo 'checked=""';}?>  class="botao_sexo">Masculino
            <input class="inputCadastroBasicoP" name="sexo" type="radio" value="Feminino" <?php if($row[0]['sexo_usuario']=="Feminino"){echo 'checked=""';}?>  class="botao_sexo">Feminino
        </p>
        <p>
            <input type="submit" value="Atualizar" name="Atualizar" class="submitCadastroP">
        </p>
    </form>
</main>

