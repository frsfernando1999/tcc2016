<?php
$caminho = $_SERVER['PHP_SELF'];
if (stripos($caminho, 'view')) {
    header("location: ../index.php?p=alterarSenha");
}
//Página com restrição
require_once 'util/ValidarAcesso.class.php';
ValidarAcesso::verificarPermissao(array(0 => 1, 1 => 2, 2 => 3));

require_once "core/Usuario.class.php";

$usuario = new Usuario;
$row = $usuario->select("and cod_usuario='" . $_SESSION['cod_usuario'] . "'", "");
?>

<main class="conteudo" id="conteudo_perfil_senha">
    <?php
    ?>
    
    <h1 id="novaSenha">Nova Senha</h1>
        <form id="alterarSenha" action="atualizarSenha.php" method="post" enctype="multipart/form-data" name="atSenha">

            <p>
                <label class="labelCadastroBasicoS" for="senhaAntiga">Senha Atual </label>
                <input class="inputCadastroBasicoS" name="senhaAntiga" maxlength="60" type="password" required="on" placeholder="Digite sua senha atual">
            </p>
            <p>
                <label class="labelCadastroBasicoS" for="senhaNova">Senha Nova </label>
                <input class="inputCadastroBasicoS" id="senhaPerfil" name="senhaNova" maxlength="60" type="password" required="on" placeholder="Digite sua nova senha">
            </p>    
            <p>
                <label class="labelCadastroBasicoS" for="confSenha">Confirme sua nova senha </label>
                <input class="inputCadastroBasicoS" id="confSenhaPerfil" name="confSenha" maxlength="60" oninput="validaSenhaPerfil(this)" required="" type="password" placeholder="Confirme sua senha">
            </p>    
                <input type="submit" value="Atualizar" name="atualizarSenha" id="enviarSenhaNova" class="submitCadastroS">

        </form>
   
</main>

