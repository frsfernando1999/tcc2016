<?php
$caminho = $_SERVER['PHP_SELF'];
if (stripos($caminho, 'view')) {
    header("location: ../index.php?p=areaAdm");
}
//Página com restrição
require_once 'util/ValidarAcesso.class.php';
ValidarAcesso::verificarPermissao(array(0 => 3));
?>
<main class="conteudo" id="conteudo_adm">
    <h1 id="tituloAdm">Área Administrativa</h1>
    <ul id="lista_menu_adm">
        <li class="areaAdmItemMenu"><a class="linkMenuAdm" href="?p=cadastrarDisciplina">Cadastrar Disciplina</a></li>
        <li class="areaAdmItemMenu"><a class="linkMenuAdm" href="?p=cadastrarRamo">Cadastrar Ramo</a></li>
        <li class="areaAdmItemMenu"><a class="linkMenuAdm" href="?p=cadastrarTopico">Cadastrar Tópico</a></li>
        <li class="areaAdmItemMenu"><a class="linkMenuAdm" href="?p=cadastrarUsuarioAdm">Cadastrar Usuario Administrador</a></li>
        <li class="areaAdmItemMenu"><a class="linkMenuAdm" href="?p=usuariosCadastrados">Usuarios Cadastrados</a></li>
        <li class="areaAdmItemMenu"><a class="linkMenuAdm" href="?p=criarExercicioObjAdmin">Criar exercicio</a></li>
        <li class="areaAdmItemMenu"><a class="linkMenuAdm" href="?p=criarSimulado">Criar Simulado</a></li>
        <li class="areaAdmItemMenu"><a class="linkMenuAdm" href="?p=listasCadastradas">Exercicios Cadastrados</a></li>
    </ul>

</main>