<?php 
$caminho = $_SERVER['PHP_SELF'];
if (stripos($caminho, 'view')) {
    header("location: ../index.php?p=cadastrarDisciplina");
}
require_once 'util/ValidarAcesso.class.php';
ValidarAcesso::verificarPermissao(array(0=>3))

?>
<main class="conteudo" id="conteudoCadDisc">
<h1 class="tituloCad">Nova Disciplina</h1>
<form class="cadastroAdm" action="CadastrarDisc.php" method="post" enctype="multipart/form-data" name="cadastro">
  
        <label class="labelCadastroAdm" for="nomeDisc">Nome Disciplina: </label>
        <input class="inputCadastroAdm" required="on" maxlength="30" name="nomeDisc" type="text" placeholder="Digite o nome da Disciplina">
   
        <label class="labelCadastroAdm" id="tituloImgDisc" for="img">Imagem de Perfil</label>
        <input class="inputCadastroAdmImg" required="on" type="file" name="foto"/>
   
        <input type="submit"  value="Cadastrar" name="cadastrar" class="submitCadastroAdm">

</form>
</main>
