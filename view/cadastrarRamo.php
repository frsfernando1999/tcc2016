<?php
$caminho = $_SERVER['PHP_SELF'];
if (stripos($caminho, 'view')) {
    header("location: ../index.php?p=cadastrarRamo");
}
require_once 'util/ValidarAcesso.class.php';
ValidarAcesso::verificarPermissao(array(0=>3));

require_once 'core/RamoDisciplina.class.php';

$ramodisciplina = new ramoDisciplina();
$row=$ramodisciplina->selectInner('','');
$caminho = $_SERVER['PHP_SELF'];

?>


<main class="conteudo" id="conteudoCadDisc">
    <h1 class="tituloCad">Novo ramo</h1>
    <form class="cadastroAdm" action="cadastroRamo.php" method="post" enctype="multipart/form-data" name="cadastro">

        <p>
            <label class="labelCadastroAdm" for="nomeRamo">Nome Ramo: </label>
            <input class="inputCadastroAdm" required="on" maxlength="50" name="nomeRamo" type="text" placeholder="Digite o nome do ramo">

            <label class="labelCadastroAdm" for="codDisc">Disciplina: </label>
            <select required="on" name="codDisc">
                <?php
                foreach ($row as $nome_disc){
                echo "<option value=\"" . $nome_disc['cod_disc'] . "\">" . $nome_disc['nome_disc'] . "</option>";
                    }
                ?>
            </select>
        </p>
        <p>
            <input type="submit"  value="Cadastrar" name="cadastrar" class="submitCadastroAdm">
        </p>
    </form>
</main>
