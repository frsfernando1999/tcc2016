<?php
$caminho = $_SERVER['PHP_SELF'];
if (stripos($caminho, 'view')) {
    header("location: ../index.php?p=cadastrarTopico");
}
require_once 'util/ValidarAcesso.class.php';
ValidarAcesso::verificarPermissao(array(0 => 3));
require_once 'core/RamoDisciplina.class.php';
$ramos = new RamoDisciplina();
$nomeRamo = $ramos->select('', '')
?>


<main class="conteudo" id="conteudoTopico">
    <h1 class="tituloCad">Novo tópico</h1>
    <form class="cadastroAdmT" action="cadastroTopico.php" method="post" enctype="multipart/form-data" name="cadastro">


        <label class="labelCadastroAdmT" for="nomeTopico">Nome Tópico: </label>
        <input class="inputCadastroAdmTopicoB" maxlength="50" required="on" name="nomeTopico" type="text" placeholder="Digite o nome do tópico">


        <select required="on" id="ramoTopicoSelect" name="codRamo">
            <?php
            foreach ($nomeRamo as $nome_ramo) {
                echo "<option class=\"selecionaRamo\" value=\"" . $nome_ramo['cod_ramo'] . "\">" . $nome_ramo['nome_ramo'] . "</option>";
            }
            ?>
        </select>

        <p></p>

        <label class="labelCadastroAdmT" for="textoApres">Texto de Apresentação: </label>
        <textarea class="inputCadastroAdmT" maxlength="255" required="on" name="textoApres" type="text" placeholder="Digite o texto de apresentação aqui"></textarea>
        
        <p></p>

        <label class="labelCadastroAdmT" for="conteudoTopico">Conteúdo do Tópico: </label>
        <textarea class="inputCadastroAdmTopico" maxlength="65535" required="on"  name="conteudoTopico" type="text" placeholder="Digite o conteudo"></textarea>
        
        <p></p>
        
        <label class="labelCadastroAdmT" for="imagemTopico">Imagem: </label>
        <input class="inputCadastroAdmT" required="on" name="foto" type="file">
        
        <p></p>
        
        <label class="labelCadastroAdmT" for="LegendaImagemTopico">Legenda para imagem: </label>
        <input class="inputCadastroAdmTopicoB" required="on" maxlength="75" name="LegendaImagemTopico" id="inputLegendaImagemTopico" type="text" placeholder="Digite uma legenda para o topico">
        
        <p></p>
        
        
        <input type="submit" id="enviaNovoTopico" value="Cadastrar" name="cadastrar" class="submitCadastroAdm">
        
    </form>
</main>
