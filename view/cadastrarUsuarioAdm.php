<?php
$caminho = $_SERVER['PHP_SELF'];
if (stripos($caminho, 'view')) {
    header("location: ../index.php?p=cadastrarUsuarioAdm");
}
require_once 'util/ValidarAcesso.class.php';
ValidarAcesso::verificarPermissao(array(0 => 3));
?>

<main class="conteudo" id="conteudoCadDisc">
    <h1 class="tituloCad">Novo Administrador(a)</h1>
    <form class="cadastroAdm" id="cadastroUserAdm" action="cadastroAdm.php" method="post" enctype="multipart/form-data" name="cadastro">

        <p></p>

        <label class="labelCadastroAdm" for="nome">Nome </label>
        <input class="inputCadastroAdm" required="on" maxlength="20" name="nomeM" type="text" placeholder="Digite seu nome">

        <p></p>

        <label class="labelCadastroAdm" for="sobrenome">Sobrenome </label>
        <input class="inputCadastroAdm" maxlength="30" name="sobrenomeM" type="text" placeholder="Digite seu sobrenome">

        <p></p>

        <label class="labelCadastroAdm" for="email" >E-mail </label>
        <input class="inputCadastroAdm" name="emailM" maxlength="45" type="email" placeholder="Digite seu email">

        <p></p>

        <label class="labelCadastroAdm" for="senha">Senha </label>
        <input class="inputCadastroAdm" maxlength="60" name="senhaM" type="password"  placeholder="Digite sua senha">

        <p></p>

        <label class="labelCadastroAdm" for="sexo">Sexo</label>
        <input class="inputCadastroAdm" name="sexoM" type="radio" value="Masculino" class="botao_sexo">Masculino
        <input class="inputCadastroAdm" name="sexoM" type="radio" value="Feminino" class="botao_sexo">Feminino
        <p></p>

        <label class="labelCadastroAdm" for="dataNasc" id="label_data">Data de nascimento</label>
        <input class="inputCadastroAdm" name="dataNascM" min="1926-01-01" max="2016-01-01" type="date" id="input_data">

        <p></p>

        <label class="labelCadastroAdm"  for="img">Imagem de Perfil</label>
        <input class="inputCadastroAdm" id="colocaImgNovoAdm" type="file" name="foto"/>

        <p></p>

        <label class="labelCadastroAdm" for="sexo"></label>
        <input type="submit" id="enviaNovoAdm" value="Cadastrar" name="cadastrar" class="submitCadastroAdm">

    </form>
</main>
