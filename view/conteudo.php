<?php
$caminho = $_SERVER['PHP_SELF'];
if (stripos($caminho, 'view')) {
    header("location: ../index.php?p=topicosDisciplina");
}
require_once 'util/ValidarAcesso.class.php';
ValidarAcesso::verificarPermissao(array(0 => 1, 1 => 2, 2 => 3));

$topico = $_GET['topico'];
require_once 'core/TopicoDisciplina.class.php';

$conteudo = new TopicoDisciplina();
$row = $conteudo->selectInner("and cod_topico=$topico");
?>
<main class="conteudo" id="conteudo_disciplina">
    <div id="imagemCont">
        <?php
        foreach ($row as $texto) {
            echo "<h1 id=\"tituloCont\">" . $texto['nome_topico'] . "</h1>";
            echo "<img id=\"imgCont\" src=\"" . $texto['url_arq'] . "\">";
        }
        ?>
    </div>
    <div id="colunasCont">
        <?php
        echo "<p id=\"textoCont\">" . nl2br($texto['texto_topico']) . "</p>";
        ?>

    </div>
    <div id="posicaoBotoesCont">
        <a class="voltarBotao" onClick="history.go(-1)"> Voltar </a>
        <?php if ($_SESSION['tipo_usuario'] == 2 || $_SESSION['tipo_usuario'] == 3) {
            echo "<a class=\"voltarBotao\" href=\"index.php?p=criarExercicioObj&codTopico=$topico\">Deseja criar um exercicios sobre este tópico?</a>";
        } ?>
        <?php if ($_SESSION['tipo_usuario'] == 3) {
            echo "<a class=\"voltarBotao\" href=\"index.php?p=atualizarTopico&codTopico=$topico\">Atualizar Tópico</a>";
        } ?>

    </div>
</main>
