<?php
$caminho = $_SERVER['PHP_SELF'];
if (stripos($caminho, 'view')) {
    header("location: ../index.php?p=criarExercicioDiss");
}
require_once 'util/ValidarAcesso.class.php';
ValidarAcesso::verificarPermissao(array(0 => 2, 1 => 3));
$topico = $_GET['codTopico'];
?>
<main class="conteudo" id="conteudo_cria_exercicio">
    <h1 id="titulo_cadastrar_exercicio">Nova lista de exercicios</h1>

    <?php echo "<form id=\"formDiss\" action=\"cadastraEx.php?tipoLista=D&codTopico=$topico\" method=\"post\" enctype=\"multipart/form-data\" name=\"cadastroEx\">"; ?>
    <?php echo "<a href=\"?p=criarExercicioObj&codTopico=$topico\" class=\"botaoEx\">Exercícios Objetivos</a>" ?>
    <?php echo "<a href=\"?p=criarExercicioDiss&codTopico=$topico\" class=\"botaoEx\">Exercícios Dissertativo</a>" ?>
    <label class="nomeListaEx">Nome da Lista: </label><input type="text" class="nomeListaExInput" required="on" name="titulo_lista" maxlength="45">
    <section id="exsDissertativo">

        <div>
            <label class="enunciadoEx">Enunciado do exercício 1: </label>
            <textarea class="descricaoEx" type="text" name="dissQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx1" onclick="this.style.border='2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx1" style="display:none" class="fotoEx" type="file" name="imgQuest1">
        </div>

        <div>
            <label class="enunciadoEx">Enunciado do exercício 2: </label>
            <textarea type="text" name="dissQ[]" class="descricaoEx" maxlength="65535" required="on"></textarea>
            <label for="fotoEx2" onclick="this.style.border='2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx2" style="display:none" class="fotoEx" type="file" name="imgQuest2">
        </div>

        <div>
            <label class="enunciadoEx">Enunciado do exercício 3:</label>
            <textarea type="text" name="dissQ[]" maxlength="65535" class="descricaoEx" required="on"></textarea>
            <label for="fotoEx3" onclick="this.style.border='2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx3" style="display:none" class="fotoEx" type="file" name="imgQuest3">
        </div>

        <div>
            <label class="enunciadoEx">Enunciado do exercício 4: </label>
            <textarea type="text" name="dissQ[]" maxlength="65535" required="on" class="descricaoEx"></textarea>
            <label for="fotoEx4" onclick="this.style.border='2px solid green'" class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx4" style="display:none" class="fotoEx" type="file" name="imgQuest4">
        </div>

        <div>
            <label class="enunciadoEx">Enunciado do exercício 5: </label>
            <textarea type="text" name="dissQ[]" maxlength="65535" required="on" class="descricaoEx"></textarea>
            <label for="fotoEx5" onclick="this.style.border='2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx5" style="display:none" class="fotoEx" type="file" name="imgQuest5">
        </div>

        <div>
            <label class="enunciadoEx">Enunciado do exercício 6: </label>
            <textarea type="text" name="dissQ[]" maxlength="65535" required="on" class="descricaoEx"></textarea>
            <label for="fotoEx6" onclick="this.style.border='2px solid green'" class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx6" style="display:none" class="fotoEx" type="file" name="imgQuest6">
        </div>

        <div>
            <label class="enunciadoEx">Enunciado do exercício 7: </label>
            <textarea type="text" name="dissQ[]" class="descricaoEx" maxlength="65535" required="on"></textarea>
            <label for="fotoEx7" onclick="this.style.border='2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx7" style="display:none" class="fotoEx" type="file" name="imgQuest7">
        </div>

        <div>
            <label class="enunciadoEx">Enunciado do exercício 8: </label>
            <textarea type="text" name="dissQ[]" maxlength="65535" class="descricaoEx" required="on"></textarea>
            <label for="fotoEx8" onclick="this.style.border='2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx8" style="display:none" class="fotoEx" type="file" name="imgQuest8">
        </div>

        <div>
            <label class="enunciadoEx">Enunciado do exercício 9: </label>
            <textarea type="text" name="dissQ[]" class="descricaoEx" maxlength="65535" required="on"></textarea>
            <label for="fotoEx9" onclick="this.style.border='2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx9" style="display:none" class="fotoEx" type="file" name="imgQuest9">
        </div>

        <div>
            <label class="enunciadoEx">Enunciado do exercício 10: </label>
            <textarea type="text" name="dissQ[]" maxlength="65535" class="descricaoEx" required="on"></textarea>
            <label for="fotoEx10" onclick="this.style.border='1px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx10" style="display:none" class="fotoEx" type="file" name="imgQuest10">
            
        </div>

        
        <label class="enunciadoEx">Resposta do exercicio 1: </label>
        <textarea class="descricaoEx" type="text" name="dissQresp[]" maxlength="65535" required="on"></textarea>

        <label class="enunciadoEx">Resposta do exercicio 2: </label>
        <textarea class="descricaoEx" type="text" name="dissQresp[]" maxlength="65535" required="on"></textarea>

        <label class="enunciadoEx">Resposta do exercicio 3: </label>
        <textarea class="descricaoEx" type="text" name="dissQresp[]" maxlength="65535" required="on"></textarea>

        <label class="enunciadoEx">Resposta do exercicio 4: </label>
        <textarea class="descricaoEx" type="text" name="dissQresp[]" maxlength="65535" required="on"></textarea>

        <label class="enunciadoEx">Resposta do exercicio 5: </label>
        <textarea class="descricaoEx" type="text" name="dissQresp[]" maxlength="65535" required="on"></textarea>

        <label class="enunciadoEx">Resposta do exercicio 6: </label>
        <textarea class="descricaoEx" type="text" name="dissQresp[]" maxlength="65535" required="on"></textarea>

        <label class="enunciadoEx">Resposta do exercicio 7: </label>
        <textarea class="descricaoEx" type="text" name="dissQresp[]" maxlength="65535" required="on"></textarea>

        <label class="enunciadoEx">Resposta do exercicio 8: </label>
        <textarea class="descricaoEx" type="text" name="dissQresp[]" maxlength="65535" required="on"></textarea>

        <label class="enunciadoEx">Resposta do exercicio 9: </label>
        <textarea class="descricaoEx" type="text" name="dissQresp[]" maxlength="65535" required="on"></textarea>

        <label class="enunciadoEx">Resposta do exercicio 10: </label>
        <textarea class="descricaoEx" type="text" name="dissQresp[]" maxlength="65535" required="on"></textarea>


    </section>

    <input id="enviarEx" type="submit" value="Enviar">


    </form>
</main>
