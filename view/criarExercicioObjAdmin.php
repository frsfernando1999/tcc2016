<?php
$caminho = $_SERVER['PHP_SELF'];
if (stripos($caminho, 'view')) {
    header("location: ../index.php?p=criarExercicioObjAdmin");
}
require_once 'util/ValidarAcesso.class.php';
ValidarAcesso::verificarPermissao(array(0 => 3));

require_once 'core/TopicoDisciplina.class.php';
$topicos = new TopicoDisciplina();
$row = $topicos->select();
?>
<main class="conteudo" id="conteudo_cria_exercicio">
    <h1 id="titulo_cadastrar_exercicio">Nova lista de exercicios</h1>
    <form id="formObj" action="cadastraExAdmin.php?tipoLista=O" method="post" enctype="multipart/form-data" name="cadastroEx">
        <label class="nomeListaEx">Nome da Lista: </label><input type="text" class="nomeListaExInput" required="on" name="titulo_lista" maxlength="45">
        <label>Tópico da Disciplina</label>
        <select name="codTopico" class="selectTopicoListaExercicios"> 
            <?php
            foreach ($row as $nome_topico) {
                echo "<option value=\"" . $nome_topico['cod_topico'] . "\">" . $nome_topico['nome_topico'] . "</option>";
            }
            ?>

        </select>
        <section id="exsObjetivo">

            <label class="enunciadoEx">Enunciado do exercicio 1: </label>
            <textarea type="text" class="descricaoEx" maxlength="65535" name="objQ[]" required="on"></textarea>
            <label for="fotoEx1" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx1" style="display:none" class="fotoEx" type="file" name="imgQuest1">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A)</label> <input required="on" type="radio" class="selecaoEx" name="alter1" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input required="on" type="radio" class="selecaoEx" name="alter1" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input required="on" type="radio" class="selecaoEx" name="alter1" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input required="on" type="radio" class="selecaoEx" name="alter1" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input required="on" type="radio" class="selecaoEx" name="alter1" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>


            <label class="enunciadoEx">Enunciado do exercicio 2: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx2" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx2" style="display:none" class="fotoEx" type="file" name="imgQuest2">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input required="on" type="radio" class="selecaoEx" name="alter2" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input required="on" type="radio" class="selecaoEx" name="alter2" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input required="on" type="radio" class="selecaoEx" name="alter2" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input required="on" type="radio" class="selecaoEx" name="alter2" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"> <label>E) </label><input required="on" type="radio" class="selecaoEx" name="alter2" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercicio 3: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx3" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx3" style="display:none" class="fotoEx" type="file" name="imgQuest3">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input required="on" type="radio" class="selecaoEx" name="alter3" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input required="on" type="radio" class="selecaoEx" name="alter3" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input required="on" type="radio" class="selecaoEx" name="alter3" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input required="on" type="radio" class="selecaoEx" name="alter3" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input required="on" type="radio" class="selecaoEx" name="alter3" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercicio 4: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx4" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx4" style="display:none" class="fotoEx" type="file" name="imgQuest4">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input required="on" type="radio" class="selecaoEx" name="alter4" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input required="on" type="radio" class="selecaoEx" name="alter4" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input required="on" type="radio" class="selecaoEx" name="alter4" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input required="on" type="radio" class="selecaoEx" name="alter4" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input required="on" type="radio" class="selecaoEx" name="alter4" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercicio 5: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx5" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx5" style="display:none" class="fotoEx" type="file" name="imgQuest5">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input required="on" type="radio" class="selecaoEx" name="alter5" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input required="on" type="radio" class="selecaoEx" name="alter5" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input required="on" type="radio" class="selecaoEx" name="alter5" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input required="on" type="radio" class="selecaoEx" name="alter5" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input required="on" type="radio" class="selecaoEx" name="alter5" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercicio 6: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx6" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx6" style="display:none" class="fotoEx" type="file" name="imgQuest6">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input required="on" type="radio" class="selecaoEx" name="alter6" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input required="on" type="radio" class="selecaoEx" name="alter6" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input required="on" type="radio" class="selecaoEx" name="alter6" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input required="on" type="radio" class="selecaoEx" name="alter6" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input required="on" type="radio" class="selecaoEx" name="alter6" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercicio 7: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx7" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx7" style="display:none" class="fotoEx" type="file" name="imgQuest7">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input required="on" type="radio" class="selecaoEx" name="alter7" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input required="on" type="radio" class="selecaoEx" name="alter7" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input required="on" type="radio" class="selecaoEx" name="alter7" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input required="on" type="radio" class="selecaoEx" name="alter7" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input required="on" type="radio" class="selecaoEx" name="alter7" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercicio 8: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx8" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx8" style="display:none" class="fotoEx" type="file" name="imgQuest8">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input required="on" type="radio" class="selecaoEx" name="alter8" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input required="on" type="radio" class="selecaoEx" name="alter8" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input required="on" type="radio" class="selecaoEx" name="alter8" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input required="on" type="radio" class="selecaoEx" name="alter8" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input required="on" type="radio" class="selecaoEx" name="alter8" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercicio 9: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx9" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx9" style="display:none" class="fotoEx" type="file" name="imgQuest9">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input required="on" type="radio" class="selecaoEx" name="alter9" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input required="on" type="radio" class="selecaoEx" name="alter9" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input required="on" type="radio" class="selecaoEx" name="alter9" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input required="on" type="radio" class="selecaoEx" name="alter9" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input required="on" type="radio" class="selecaoEx" name="alter9" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercicio 10: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx10" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx10" style="display:none" class="fotoEx" type="file" name="imgQuest10">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input required="on" type="radio" class="selecaoEx" name="alter10" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input required="on" type="radio" class="selecaoEx" name="alter10" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input required="on" type="radio" class="selecaoEx" name="alter10" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input required="on" type="radio" class="selecaoEx" name="alter10" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input required="on" type="radio" class="selecaoEx" name="alter10" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>


        </section>

        <input id="enviarEx" type="submit" value="Enviar">


    </form>
</main>
