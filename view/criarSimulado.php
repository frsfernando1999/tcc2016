<?php
$caminho = $_SERVER['PHP_SELF'];
if (stripos($caminho, 'view')) {
    header("location: ../index.php?p=criarSimulado");
}
require_once 'util/ValidarAcesso.class.php';
ValidarAcesso::verificarPermissao(array(0 => 3));

require_once 'core/TopicoDisciplina.class.php';
$topicos = new TopicoDisciplina();
$row = $topicos->select();
?>
<main class="conteudo" id="conteudo_cria_exercicio">
    <h1 id="titulo_cadastrar_exercicio">Nova lista de exercícios</h1>
    <form id="formObj" action="cadastraSimulado.php" method="post" enctype="multipart/form-data" name="cadastroEx">
        <label class="nomeListaEx">Nome do Simulado: </label><input type="text" class="nomeListaExInput" required="on" name="titulo_lista" maxlength="45">

        <section id="exsObjetivo">

            <label class="enunciadoEx">Enunciado do exercício 1: </label>

            <textarea  type="text" class="descricaoEx" maxlength="65535" name="objQ[]" required="on"></textarea>
            <label for="fotoEx1" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx1" style="display:none" class="fotoEx" type="file" name="imgQuest1">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A)</label> <input type="radio" class="selecaoEx" name="alter1" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter1" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter1" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter1" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter1" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercício 2: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx2" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx2" style="display:none" class="fotoEx" type="file" name="imgQuest2">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter2" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter2" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter2" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter2" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"> <label>E) </label><input type="radio" class="selecaoEx" name="alter2" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 3: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx3" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx3" style="display:none" class="fotoEx" type="file" name="imgQuest3">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter3" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter3" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter3" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter3" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter3" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 4: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx4" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx4" style="display:none" class="fotoEx" type="file" name="imgQuest4">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter4" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter4" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter4" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter4" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter4" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 5: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx5" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx5" style="display:none" class="fotoEx" type="file" name="imgQuest5">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter5" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter5" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter5" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter5" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter5" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 6: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx6" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx6" style="display:none" class="fotoEx" type="file" name="imgQuest6">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter6" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter6" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter6" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter6" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter6" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 7: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx7" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx7" style="display:none" class="fotoEx" type="file" name="imgQuest7">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter7" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter7" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter7" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter7" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter7" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 8: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx8" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx8" style="display:none" class="fotoEx" type="file" name="imgQuest8">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter8" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter8" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter8" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter8" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter8" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 9: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx9" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx9" style="display:none" class="fotoEx" type="file" name="imgQuest9">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter9" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter9" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter9" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter9" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter9" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 10: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx10" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx10" style="display:none" class="fotoEx" type="file" name="imgQuest10">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter10" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter10" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter10" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter10" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter10" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercício 11: </label>
            <textarea type="text" class="descricaoEx" maxlength="65535" name="objQ[]" required="on"></textarea>
            <label for="fotoEx11" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx11" style="display:none" class="fotoEx" type="file" name="imgQuest11">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A)</label> <input type="radio" class="selecaoEx" name="alter11" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter11" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter11" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter11" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter11" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercício 12: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx12" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx12" style="display:none" class="fotoEx" type="file" name="imgQuest12">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter12" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter12" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter12" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter12" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"> <label>E) </label><input type="radio" class="selecaoEx" name="alter12" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercício 13: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx13" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx13" style="display:none" class="fotoEx" type="file" name="imgQuest13">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter13" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter13" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter13" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter13" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter13" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 14: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx14" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx14" style="display:none" class="fotoEx" type="file" name="imgQuest14">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter14" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter14" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter14" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter14" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter14" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 15: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx15" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx15" style="display:none" class="fotoEx" type="file" name="imgQuest15">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter15" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter15" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter15" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter15" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter15" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 16: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx16" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx16" style="display:none" class="fotoEx" type="file" name="imgQuest16">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter16" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter16" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter16" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter16" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter16" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 17: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx17" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx17" style="display:none" class="fotoEx" type="file" name="imgQuest17">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter17" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter17" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter17" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter17" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter17" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 18: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx18" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx18" style="display:none" class="fotoEx" type="file" name="imgQuest18">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter18" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter18" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter18" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter18" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter18" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 19: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx19" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx19" style="display:none" class="fotoEx" type="file" name="imgQuest19">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter19" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter19" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter19" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter19" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter19" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 20: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx20" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx20" style="display:none" class="fotoEx" type="file" name="imgQuest20">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter20" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter20" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter20" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter20" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter20" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercício 21: </label>
            <textarea type="text" class="descricaoEx" maxlength="65535" name="objQ[]" required="on"></textarea>
            <label for="fotoEx21" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx21" style="display:none" class="fotoEx" type="file" name="imgQuest21">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A)</label> <input type="radio" class="selecaoEx" name="alter21" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter21" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter21" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter21" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter21" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercício 22: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx22" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx22" style="display:none" class="fotoEx" type="file" name="imgQuest22">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter22" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter22" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter22" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter22" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"> <label>E) </label><input type="radio" class="selecaoEx" name="alter22" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercício 23: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx23" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx23" style="display:none" class="fotoEx" type="file" name="imgQuest23">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter23" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter23" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter23" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter23" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter23" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 24: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx24" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx24" style="display:none" class="fotoEx" type="file" name="imgQuest24">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter24" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter24" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter24" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter24" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter24" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 25: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx25" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx25" style="display:none" class="fotoEx" type="file" name="imgQuest25">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter25" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter25" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter25" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter25" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter25" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 26: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx26" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx26" style="display:none" class="fotoEx" type="file" name="imgQuest26">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter26" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter26" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter26" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter26" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter26" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 27: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx27" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx27" style="display:none" class="fotoEx" type="file" name="imgQuest27">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter27" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter27" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter27" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter27" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter27" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 28: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx28" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx28" style="display:none" class="fotoEx" type="file" name="imgQuest28">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter28" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter28" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter28" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter28" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter28" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 29: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx29" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx29" style="display:none" class="fotoEx" type="file" name="imgQuest29">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter29" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter29" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter29" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter29" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter29" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 30: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx30" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx30" style="display:none" class="fotoEx" type="file" name="imgQuest30">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter30" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter30" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter30" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter30" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter30" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercício 31: </label>
            <textarea type="text" class="descricaoEx" maxlength="65535" name="objQ[]" required="on"></textarea>

            <label for="fotoEx31" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx31" style="display:none" class="fotoEx" type="file" name="imgQuest31">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A)</label> <input type="radio" class="selecaoEx" name="alter31" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter31" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter31" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter31" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter31" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercício 32: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx32" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx32" style="display:none" class="fotoEx" type="file" name="imgQuest32">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter32" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter32" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter32" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter32" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"> <label>E) </label><input type="radio" class="selecaoEx" name="alter32" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercício 33: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx33" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx33" style="display:none" class="fotoEx" type="file" name="imgQuest33">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter33" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter33" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter33" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter33" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter33" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 34: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx34" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx34" style="display:none" class="fotoEx" type="file" name="imgQuest34">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter34" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter34" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter34" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter34" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter34" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 35: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx35" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx35" style="display:none" class="fotoEx" type="file" name="imgQuest35">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter35" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter35" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter35" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter35" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter35" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercicio 36: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx36" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx36" style="display:none" class="fotoEx" type="file" name="imgQuest36">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter36" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter36" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter36" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter36" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter36" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 37: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx37" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx37" style="display:none" class="fotoEx" type="file" name="imgQuest37">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter37" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter37" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter37" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter37" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter37" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 38: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx38" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx38" style="display:none" class="fotoEx" type="file" name="imgQuest38">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter38" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter38" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter38" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter38" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter38" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 39: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx39" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx39" style="display:none" class="fotoEx" type="file" name="imgQuest39">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter39" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter39" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter39" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter39" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter39" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 40: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx40" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx40" style="display:none" class="fotoEx" type="file" name="imgQuest40">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter40" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter40" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter40" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter40" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter40" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercício 41: </label>
            <textarea type="text" class="descricaoEx" maxlength="65535" name="objQ[]" required="on"></textarea>

            <label for="fotoEx41" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx41" style="display:none" class="fotoEx" type="file" name="imgQuest41">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A)</label> <input type="radio" class="selecaoEx" name="alter41" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter41" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter41" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter41" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter41" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercício 42: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx42" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx42" style="display:none" class="fotoEx" type="file" name="imgQuest42">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter42" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter42" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter42" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter42" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"> <label>E) </label><input type="radio" class="selecaoEx" name="alter42" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercício 43: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx43" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx43" style="display:none" class="fotoEx" type="file" name="imgQuest43">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter43" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter43" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter43" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter43" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter43" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 44: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx44" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx44" style="display:none" class="fotoEx" type="file" name="imgQuest44">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter44" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter44" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter44" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter44" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter44" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 45: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx45" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx45" style="display:none" class="fotoEx" type="file" name="imgQuest45">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter45" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter45" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter45" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter45" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter45" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 46: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx46" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx46" style="display:none" class="fotoEx" type="file" name="imgQuest46">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter46" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter46" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter46" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter46" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter46" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 47: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx47" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx47" style="display:none" class="fotoEx" type="file" name="imgQuest47">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter47" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter47" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter47" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter47" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter47" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 48: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx48" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx48" style="display:none" class="fotoEx" type="file" name="imgQuest48">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter48" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter48" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter48" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter48" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter48" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 49: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx49" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx49" style="display:none" class="fotoEx" type="file" name="imgQuest49">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter49" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter49" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter49" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter49" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter49" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 50: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx50" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx50" style="display:none" class="fotoEx" type="file" name="imgQuest50">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter50" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter50" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter50" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter50" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter50" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercício 51: </label>
            <textarea type="text" class="descricaoEx" maxlength="65535" name="objQ[]" required="on"></textarea>

            <label for="fotoEx51" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx51" style="display:none" class="fotoEx" type="file" name="imgQuest51">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A)</label> <input type="radio" class="selecaoEx" name="alter51" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter51" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter51" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter51" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter51" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercício 52: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx52" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx52" style="display:none" class="fotoEx" type="file" name="imgQuest52">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter52" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter52" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter52" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter52" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter52" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercício 53: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx53" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx53" style="display:none" class="fotoEx" type="file" name="imgQuest53">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter53" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter53" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter53" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter53" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter53" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 54: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx54" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx54" style="display:none" class="fotoEx" type="file" name="imgQuest54">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter54" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter54" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter54" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter54" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter54" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 55: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx55" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx55" style="display:none" class="fotoEx" type="file" name="imgQuest55">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter55" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter55" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter55" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter55" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter55" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 56: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx56" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx56" style="display:none" class="fotoEx" type="file" name="imgQuest56">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter56" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter56" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter56" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter56" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter56" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 57: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx57" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx57" style="display:none" class="fotoEx" type="file" name="imgQuest57">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter57" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter57" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter57" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter57" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter57" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 58: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx58" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx58" style="display:none" class="fotoEx" type="file" name="imgQuest58">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter58" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter58" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter58" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter58" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter58" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 59: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx59" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx59" style="display:none" class="fotoEx" type="file" name="imgQuest59">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter59" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter59" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter59" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter59" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter59" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 60: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx60" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx60" style="display:none" class="fotoEx" type="file" name="imgQuest60">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter60" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter60" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter60" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter60" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter60" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercício 61: </label>
            <textarea type="text" class="descricaoEx" maxlength="65535" name="objQ[]" required="on"></textarea>
            <label for="fotoEx61" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx61" style="display:none" class="fotoEx" type="file" name="imgQuest61">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A)</label> <input type="radio" class="selecaoEx" name="alter61" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter61" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter61" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter61" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter61" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercício 62: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx62" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx62" style="display:none" class="fotoEx" type="file" name="imgQuest62">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter62" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter62" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter62" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter62" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"> <label>E) </label><input type="radio" class="selecaoEx" name="alter62" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercício 63: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx63" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx63" style="display:none" class="fotoEx" type="file" name="imgQuest63">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter63" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter63" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter63" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter63" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter63" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 64: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx64" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx64" style="display:none" class="fotoEx" type="file" name="imgQuest64">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter64" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter64" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter64" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter64" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter64" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 65: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx65" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx65" style="display:none" class="fotoEx" type="file" name="imgQuest65">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter65" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter65" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter65" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter65" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter65" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 66: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx66" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx66" style="display:none" class="fotoEx" type="file" name="imgQuest66">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter66" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter66" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter66" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter66" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter66" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 67: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx67" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx67" style="display:none" class="fotoEx" type="file" name="imgQuest67">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter67" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter67" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter67" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter67" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter67" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 68: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx68" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx68" style="display:none" class="fotoEx" type="file" name="imgQuest68">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter68" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter68" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter68" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter68" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter68" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 69: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx69" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx69" style="display:none" class="fotoEx" type="file" name="imgQuest69">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter69" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter69" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter69" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter69" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter69" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 70: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx70" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx70" style="display:none" class="fotoEx" type="file" name="imgQuest70">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter70" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter70" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter70" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter70" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter70" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercício 71: </label>
            <textarea type="text" class="descricaoEx" maxlength="65535" name="objQ[]" required="on"></textarea>
            <label for="fotoEx71" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx71" style="display:none" class="fotoEx" type="file" name="imgQuest71">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A)</label> <input type="radio" class="selecaoEx" name="alter71" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter71" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter71" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter71" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter71" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercício 72: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx72" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx72" style="display:none" class="fotoEx" type="file" name="imgQuest72">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter72" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter72" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter72" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter72" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"> <label>E) </label><input type="radio" class="selecaoEx" name="alter72" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 73: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx73" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx73" style="display:none" class="fotoEx" type="file" name="imgQuest73">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter73" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter73" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter73" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter73" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter73" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 74: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx74" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx74" style="display:none" class="fotoEx" type="file" name="imgQuest74">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter74" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter74" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter74" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter74" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter74" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 75: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx75" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx75" style="display:none" class="fotoEx" type="file" name="imgQuest75">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter75" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter75" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter75" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter75" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter75" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 76: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx76" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx76" style="display:none" class="fotoEx" type="file" name="imgQuest76">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter76" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter76" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter76" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter76" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter76" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 77: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx77" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx77" style="display:none" class="fotoEx" type="file" name="imgQuest77">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter77" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter77" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter77" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter77" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter77" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 78: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx78" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx78" style="display:none" class="fotoEx" type="file" name="imgQuest78">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter78" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter78" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter78" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter78" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter78" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 79: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx79" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx79" style="display:none" class="fotoEx" type="file" name="imgQuest79">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter79" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter79" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter79" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter79" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter79" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 80: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx80" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx80" style="display:none" class="fotoEx" type="file" name="imgQuest80">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter80" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter80" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter80" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter80" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter80" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercício 81: </label>
            <textarea type="text" class="descricaoEx" maxlength="65535" name="objQ[]" required="on"></textarea>
            <label for="fotoEx81" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx81" style="display:none" class="fotoEx" type="file" name="imgQuest81">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A)</label> <input type="radio" class="selecaoEx" name="alter81" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter81" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter81" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter81" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter81" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercício 82: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx82" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx82" style="display:none" class="fotoEx" type="file" name="imgQuest82">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter82" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter82" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter82" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter82" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"> <label>E) </label><input type="radio" class="selecaoEx" name="alter82" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>

            <label class="enunciadoEx">Enunciado do exercício 83: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx83" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx83" style="display:none" class="fotoEx" type="file" name="imgQuest83">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter83" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter83" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter83" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter83" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter83" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 84: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx84" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx84" style="display:none" class="fotoEx" type="file" name="imgQuest84">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter84" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter84" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter84" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter84" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter84" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 85: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx85" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx85" style="display:none" class="fotoEx" type="file" name="imgQuest85">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter85" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter85" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter85" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter85" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter85" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 86: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx86" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx86" style="display:none" class="fotoEx" type="file" name="imgQuest86">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter86" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter86" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter86" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter86" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter86" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 87: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx87" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx87" style="display:none" class="fotoEx" type="file" name="imgQuest87">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter87" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter87" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter87" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter87" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter87" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 88: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>
            <label for="fotoEx88" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx88" style="display:none" class="fotoEx" type="file" name="imgQuest88">

            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter88" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter88" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter88" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter88" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter88" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 89: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx89" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx89" style="display:none" class="fotoEx" type="file" name="imgQuest89">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter89" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter89" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter89" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter89" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter89" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
            <label class="enunciadoEx">Enunciado do exercício 90: </label>
            <textarea type="text" class="descricaoEx" name="objQ[]" maxlength="65535" required="on"></textarea>

            <label for="fotoEx90" onclick="this.style.border = '2px solid green'"  class="SelecionarImgEx">Selecionar imagem (opcional)</label>
            <input id="fotoEx90" style="display:none" class="fotoEx" type="file" name="imgQuest90">
            <div class="posicaoAlternativas">
                <div class="espacoEx"><label>A) </label><input type="radio" class="selecaoEx" name="alter90" value="A"><input type="text" class="respostaEx" name="textAlterA[]" required="on"></div>
                <div class="espacoEx"><label>B) </label><input type="radio" class="selecaoEx" name="alter90" value="B"><input type="text" class="respostaEx" name="textAlterB[]" required="on"></div>
                <div class="espacoEx"><label>C) </label><input type="radio" class="selecaoEx" name="alter90" value="C"><input type="text" class="respostaEx" name="textAlterC[]" required="on"></div>
                <div class="espacoEx"><label>D) </label><input type="radio" class="selecaoEx" name="alter90" value="D"><input type="text" class="respostaEx" name="textAlterD[]" required="on"></div>
                <div class="espacoEx"><label>E) </label><input type="radio" class="selecaoEx" name="alter90" value="E"><input type="text" class="respostaEx" name="textAlterE[]" required="on"></div>
            </div>
          


        </section>

        <input id="enviarEx" type="submit" value="Enviar">


    </form>
</main>
