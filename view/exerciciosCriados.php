<?php
$caminho = $_SERVER['PHP_SELF'];
if (stripos($caminho, 'view')) {
    header("location: ../index.php?p=exercicios");
}
require_once 'util/ValidarAcesso.class.php';
ValidarAcesso::verificarPermissao(array(0 => 1, 1 => 2, 2 => 3));

require_once 'core/ListaExercicios.class.php';
$lista = new ListaExercicios();
$discs = $lista->select("group by nome_disc");
?>

<main class="conteudo" id="conteudo_listas_exercicios">
    <!---->    
    <form class="formBusca" method="get" action="">
        <label id="tituloBusca">Buscar</label>
        <input type="hidden" id="consulta" name="p" value="exerciciosCriados" style="display: none;" maxlength="0" />
        <input type="text" id="consulta" placeholder="Pesquisa..." value=" " name="consulta" maxlength="255"/>
        <select class="selecionaPesquisa" name="disc">
            <option value="">Selecionar uma Disciplina: </option>
            <?php
            foreach ($discs as $disc) {
                echo "<option value=\"" . $disc['cod_disc'] . "\">" . $disc['nome_disc'] . "</option>";
            }
            ?>
        </select>
        <input type="submit" id="buscar" value="Buscar" />
         <label id="buscaEnvia" for="buscar">Pesquisar</label>
    </form>

    <div class="tituloTopicoEx">
        <h1 id="tituloExerciciosX">Exercícios</h1>
    </div>
        <a id="CriaExProf" href="?p=exercicios">Voltar para os exercicios padrões</a>

    <div id="colunaTopicoExP">

        <?php
        $url = $_SERVER['REQUEST_URI'];
        
        if (!stripos($url, 'consulta')) {
            $row = $lista->selectInner("and default_lista='0' and status_exclusao_lista<>'0' and tipo_lista <> 'S'", "order by tipo_lista desc");
            foreach ($row as $lista) {
                echo "<section class=\"" . $lista['nome_disc'] . "\" id=\"espacoExTopicosP\"> <a id=\"topicoExP\"href=\"index.php?p=exerciciosLista&tipo=" . $lista['tipo_lista'] . "&lista=" . $lista['cod_lista'] . "\">
            <p class=\"linhaTopicoEx\">" . $lista['nome_disc'];
                if ($lista['tipo_lista'] == 'D') {
                    echo " (Exercício Dissertativo)";
                } elseif ($lista['tipo_lista'] == 'O') {
                    echo " (Exercício Objetivo)";
                }
                "</p>";
                echo "<p class=\"linhaTopicoEx\">" . $lista['titulo_lista'] . "
                    <p class=\"linhaTopicoEx\">" . $lista['nome_topico'] . "
                    <p class=\"linhaTopicoEx\">" . "Feito por: " . $lista['nome_usuario'] . " " . $lista['sobrenome_usuario'] . "
                    </section></a>";
            }
        } elseif (stripos($url, 'consulta')) {
            if ($_GET['consulta'] == '') {
                echo "<script>window.location='index.php?p=exerciciosCriados'</script>";
            } elseif ($_GET['disc'] != '') {
                $disc = filter_input(INPUT_GET, 'disc', FILTER_SANITIZE_STRING);
                $busca = filter_input(INPUT_GET, 'consulta', FILTER_SANITIZE_STRING);
                $total = $lista->selectInner("and titulo_lista like '%$busca%' and status_exclusao_lista='1' and default_lista='0' and tipo_lista<>'S' and disciplinas.cod_disc='$disc'", "order by titulo_lista desc");
                foreach ($total as $lista){
                echo "<section class=\"" . $lista['nome_disc'] . "\" id=\"espacoExTopicosP\"> <a id=\"topicoExP\"href=\"index.php?p=exerciciosLista&tipo=" . $lista['tipo_lista'] . "&lista=" . $lista['cod_lista'] . "\">
                <p class=\"linhaTopicoEx\">" . $lista['nome_disc'];
                if ($lista['tipo_lista'] == 'D') {
                    echo " (Exercício Dissertativo)";
                } elseif ($lista['tipo_lista'] == 'O') {
                    echo " (Exercício Objetivo)";
                }
                "</p>";
                echo "<p class=\"linhaTopicoEx\">" . $lista['titulo_lista'] . "
                    <p class=\"linhaTopicoEx\">" . $lista['nome_topico'] . "
                    <p class=\"linhaTopicoEx\">" . "Feito por: " . $lista['nome_usuario'] . " " . $lista['sobrenome_usuario'] . "
                    </section></a>";
            }            

            } else {
                $busca = filter_input(INPUT_GET, 'consulta', FILTER_SANITIZE_STRING);
                $total = $lista->selectInner("and titulo_lista like '%$busca%' and status_exclusao_lista='1' and default_lista='0' and tipo_lista<>'S'", "order by titulo_lista desc");
                foreach ($total as $lista){
                echo "<section class=\"" . $lista['nome_disc'] . "\" id=\"espacoExTopicosP\"> <a id=\"topicoExP\"href=\"index.php?p=exerciciosLista&tipo=" . $lista['tipo_lista'] . "&lista=" . $lista['cod_lista'] . "\">
                <p class=\"linhaTopicoEx\">" . $lista['nome_disc'];
                if ($lista['tipo_lista'] == 'D') {
                    echo " (Exercício Dissertativo)";
                } elseif ($lista['tipo_lista'] == 'O') {
                    echo " (Exercício Objetivo)";
                }
                "</p>";
                echo "<p class=\"linhaTopicoEx\">" . $lista['titulo_lista'] . "
                    <p class=\"linhaTopicoEx\">" . $lista['nome_topico'] . "
                    <p class=\"linhaTopicoEx\">" . "Feito por: " . $lista['nome_usuario'] . " " . $lista['sobrenome_usuario'] . "
                    </section></a>";
            }
                
            }                    if(empty($total)){echo "Não houve nenhum resultado para o termo " . $busca;}
            
        }
        ?>
    </div>
    <input class="voltarBotao" id="voltarTopicoExP" type="button" value="Voltar" onClick="history.go(-1)"> 

</main>