<?php
$caminho = $_SERVER['PHP_SELF'];
if (stripos($caminho, 'view')) {
    header("location: ../index.php?p=exerciciosLista");
}
require_once 'util/ValidarAcesso.class.php';
ValidarAcesso::verificarPermissao(array(0 => 1, 1 => 2, 2 => 3));

$codigo = $_GET['lista'];
$tipo = $_GET['tipo'];
require_once 'core/Questoes.class.php';
$questoes = new Questoes();

if ($tipo == "O") {
    $row = $questoes->select("and lista_exercicios_cod_lista='$codigo'");
}
if ($tipo == "D") {
    $row = $questoes->selectD("and lista_exercicios_cod_lista='$codigo'");
}
$i=1;
?>
<main class="conteudo" id="conteudo_questoes">
             <?php   echo "<div class=\"tituloExSeleciona\"><h1>" . $row[0]['titulo_lista'] . "</h1></div>"; ?>

    <div class="colunaExSeleciona">
       <?php echo "<form id=\"questoes\" action=\"respQuestoes.php?lista=$codigo&tipo=$tipo\" method=\"post\" enctype=\"multipart/form-data\" name=\"questoes\">"; ?>
        <?php
       if ($tipo == "O") {
            foreach ($row as $key => $questao) {
                echo "<div class=\"enunciadoEx\"><p class=\"enunciadoFacilita\">"  . $i++ . ") " . nl2br($questao['enunciado_ex_obj']) . "</p></div>";
                echo "<div class=\"imgEnunciadoEx\">";  
                
                if($questao['img_questao']!=''){
                    echo "<img class=\"imgExercicio\" src=\"" . $questao['img_questao'] . "\" alt=\"\"\>";
                }
                
                echo "</div>
                <div class=\"questaoExSeleciona\">
        <div class=\"questaoExLinha\"> <label class=\"letraEx\">A) </label>  <input class=\"bolinhaEx\" type=\"radio\" required=\"on\" value=\"A\" name=\"Q" . $key . "resp\"><label>" . $questao['alter_a'] . "</label> </div> <br>    
        <div class=\"questaoExLinha\"> <label class=\"letraEx\">B) </label>  <input class=\"bolinhaEx\" type=\"radio\" required=\"on\" value=\"B\" name=\"Q" . $key . "resp\"><label>" . $questao['alter_b'] . "</label> </div> <br>    
        <div class=\"questaoExLinha\"> <label class=\"letraEx\">C) </label><input class=\"bolinhaEx\" type=\"radio\" required=\"on\" value=\"C\" name=\"Q" . $key . "resp\"><label>" . $questao['alter_c'] . "</label> </div> <br>    
        <div class=\"questaoExLinha\"> <label class=\"letraEx\">D) </label><input class=\"bolinhaEx\" type=\"radio\" required=\"on\" value=\"D\" name=\"Q" . $key . "resp\"><label>" . $questao['alter_d'] . "</label> </div> <br>    
        <div class=\"questaoExLinha\"> <label class=\"letraEx\">E) </label><input class=\"bolinhaEx\" type=\"radio\" required=\"on\" value=\"E\" name=\"Q" . $key . "resp\"><label>" . $questao['alter_e'] . "</label> </div> <br>       
            </div>
        ";
            }
        } elseif ($tipo == "D") {
            foreach ($row as $questao) {
                echo "<div class=\"enunciadoEx\">" . $i++ . ") " . nl2br($questao['enunciado_ex_obj']) . "</div><br/>";
                                echo "<div class=\"imgEnunciadoEx\">";  
                
                if($questao['img_questao']!=''){
                    echo "<img class=\"imgExercicio\" src=\"" . $questao['img_questao'] . "\" alt=\"\"\>";
                }
                
                echo "</div>

            <textarea class=\"areaResposta\" name=\"respExDiss[]\"></textarea>
        ";
            }
        } else {
            echo 'Essa lista não existe';
        }
        ?>
    </div>
    
    <div class="posicaoEnviarEx">
    <input class="enviarEx" type="submit" value="Enviar">
    </div>
    </form>
</main>