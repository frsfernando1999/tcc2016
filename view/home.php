<?php $caminho = $_SERVER['PHP_SELF'];
if (stripos($caminho, 'view')) {
    header("location: ../index.php?p=home");
}
require_once 'util/ValidarAcesso.class.php';
ValidarAcesso::verificarPermissao(array(0=>1,1=>2,2=>3));

require_once 'core/Disciplinas.class.php';
$disc = new Disciplinas();

$row = $disc->select('','order by cod_disc');
?>
<main class="conteudo" id="conteudo_home">
    <div id="titulo_home">    
    <h1 id="tituloHome">É só escolher a disciplina e bora estudar!</h1>
    </div>             
    <?php 
    
    foreach ($row as $disc){
    echo  "<article class=\"disciplinas\">
            
            <a href=\"?p=topicosDisciplina&disc=" . $disc['cod_disc'] . "\">
            <img src=\"" . $disc['img_disc'] . "\" alt=\"\" width=\"300px\" height=\"300px\" class=\"disciplinasIMG\"/>
            </a>            
            <h2 class=\"homeH2\">" . $disc['nome_disc'] . "</h2>
           
            
</article></a>";
    }?>
    
</main>