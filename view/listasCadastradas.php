<?php
$caminho = $_SERVER['PHP_SELF'];
if (stripos($caminho, 'view')) {
    header("location: ../index.php?p=listasCadastradas");
}
require_once 'util/ValidarAcesso.class.php';
ValidarAcesso::verificarPermissao(array(0 => 3));

require_once 'core/ListaExercicios.class.php';
$lista = new ListaExercicios();
$row=$lista->selectInner('order by cod_lista');
?>

<main class="conteudo" id="conteudo_usuarios_cadastrados">
    <table border="1px solid black;">
        <tr>
            <th>Código</th>
            <th>Titulo da Lista</th>
            <th>Tipo da lista</th>
            <th>Default</th>
            <th>Criador</th>
            <th>Tópico</th>
            <th colspan="2">Ações</th>
        </tr>
        <?php
        foreach ($row as $lista){
            echo "<tr class=\"userAdm\">
            <td class=\"userAdm\">" . $lista['cod_lista'] . "</td>
            <td class=\"userAdm\">" . $lista['titulo_lista'] . "</td>
            <td class=\"userAdm\">" . $lista['tipo_lista'] . "</td>
            <td class=\"userAdm\">" . $lista['default_lista'] . "</td>
            <td class=\"userAdm\">" . $lista['nome_usuario'] . " " . $lista['sobrenome_usuario'] . "</td>
            <td class=\"userAdm\">" . $lista['nome_topico'] . "</td>";
            if ($lista['status_exclusao_lista']==1) {
                echo "<td class=\"userAdm\"><a href=\"index.php?p=deletarLista&codLista=" . $lista['cod_lista'] . "\">Desativar</a></td>
        </tr>";
            }elseif($lista['status_exclusao_lista']!=1) {
                        echo "<td class=\"userAdm\"><a href=\"index.php?p=deletarLista&codLista=" . $lista['cod_lista'] . "\">Reativar</a></td>
        </tr>";
    }

        }?>
    </table>
</main>

