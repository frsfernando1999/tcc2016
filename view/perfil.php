<?php
$caminho = $_SERVER['PHP_SELF'];
if (stripos($caminho, 'view')) {
    header("location: ../index.php");
}

//Página com restrição
require_once 'util/ValidarAcesso.class.php';
ValidarAcesso::verificarPermissao(array(0 => 1, 1 => 2, 2 => 3));

require_once "core/Usuario.class.php";

$usuario = new Usuario;
$row = $usuario->select("and cod_usuario='" . $_SESSION['cod_usuario'] . "'", "");
?>

<main class="conteudo" id="conteudoPerfil">
    <?php
    echo "<img src=\"" . $row[0]['url_foto_usuario'] . "\" class=\"imagemPerfil\" alt=\"" . $row[0]['nome_usuario'] . "\">" . '<br>';
    ?>

    <div id="informacoes">
        <?php
        echo "<h2 class=\"idPerfilT\">Nome</h2> <h2 class=\"idPerfil\">" . $row[0]['nome_usuario'] . "&nbsp" . $row[0]['sobrenome_usuario'] . "</h2>";
        echo "<h2 class=\"idPerfilT\">E-mail</h2> <h2 class=\"idPerfil\">" . $row[0]['email_usuario'] . '</h2>';
        echo "<h2 class=\"idPerfilT\">Data de Nascimento</h2> <h2 class=\"idPerfil\">";         
                $d = explode('-', $row[0]['data_nasc_usuario']);
        $dOk = $d[2].'/'.$d[1].'/'.$d[0];
        echo $dOk;
        echo "</h2>";
        echo "<h2 class=\"idPerfilT\">Sexo</h2> <h2 class=\"idPerfil\">" . $row[0]['sexo_usuario'] . '</h2>';
        ?>
    </div>
    <div id="alteraP">
        <a href="index.php?p=alterarPerfil" class="alterarPerfil"><img src="imagem/edit.png" class="editarPerfil" alt=""/></a> 
        <h5 id="editarTextoP">Editar Perfil</h5>
    </div>
    <div id="alteraS">
        <a href="index.php?p=alterarSenha" class="alterarPerfil"><img src="imagem/senha.png" class="editarPerfil" alt=""/></a> 
        <h5 id="editarTextoS">Alterar Senha</h5>
    </div>
    <div id="alteraF">
        <label for="trocaFotoPerfil"><img src="imagem/user.png" class="editarPerfil" alt=""/></label>
        <h5 id="editarTextoF">Alterar Foto</h5>
    </div>
    <form id="attFoto" action="atualizarImg.php" method="post" enctype="multipart/form-data" name="attFoto" onsubmit="Checa()">
        
        <input type="file" id="trocaFotoPerfil" name="foto" onchange="readURL(this);" required="on">
        <input type="submit" class="alterarFotoPerfil" id="confirmaPerfilImg" value="Confirmar alteração">
    </form>
</main>
