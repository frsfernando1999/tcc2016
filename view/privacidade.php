<?php
$caminho = $_SERVER['PHP_SELF'];
if (stripos($caminho, 'view')) {
    header("location: ../index.php?p=privacidade");
}

//Página com restrição
require_once 'util/ValidarAcesso.class.php';
ValidarAcesso::verificarPermissao(array(0 => 1, 1 => 2, 2 => 3));

?>

<main class="conteudo" id="privacidade">
    
    <div id="privacidadeTexto">
            <h1 class="hTermos" id="tituloPriv">Política de privacidade</h1>

            <p class="paragrafoTermos">Nós, do Vestibulário, reconhecemos que a privacidade é importante. Essa política se aplica ao conteúdo e serviços oferecidos pelo Vestibulário. 
Ressaltamos que em serviços particulares, onde se faz necessário algumas informações mais detalhadas, nós postaremos avisos de privacidade separados para descrever como esses processam informações pessoais.
Se você tiver qualquer dúvida acerca dos termos dessa Política de privacidade, por favor, não hesite em nos contatar através do e-mail da empresa vanguart.provedor@gmail.com.
</p>
            
            <h2 class="hTermos">1.0	 Informação coletadas por nós.</h2>
        <p class="paragrafoTermos">Oferecemos acesso ao nosso conteúdo apenas para usuários cadastrados. Sendo assim coletaremos as seguintes informações:</p>
        <h2 class="hTermos">2.0 Informação fornecida </h2>
        <p class="paragrafoTermos">Quando o usuário faz o cadastro em nosso site, nós solicitamos algumas informações pessoais como: nome, endereço de e-mail, senha da conta, CPF, escolaridade e outras informações complementares.</p>
        <h2 class="hTermos">3.0 Comunicações do Usuário</h2>
        <p class="paragrafoTermos">Quando o usuário nos enviar um e-mail, será averiguado sua petição a fim de processar as suas solicitações e melhorar os nossos serviços.</p>
        <h2 class="hTermos">4.0 Outros sites</h2>
        <p class="paragrafoTermos">Essa política de privacidade se aplica ao Vestibulário e aos serviços que pertencem e são operados pelo site. Não exercemos controle sobre algumas publicidades exibidas, como por exemplo, link patrocinados fornecidos pela empresa Google Inc. E outros. Esses outros sites podem colocar seus próprios cookies ou outros arquivos no seu computador, coletar dados ou solicitar algum tipo de informação pessoal.</p>
        <p class="paragrafoTermos">O Vestibulário apenas processa sua informação pessoal para os propósitos descritos na política de privacidade aplicável e avisos de privacidade para serviços específicos. Além do citado acima, tais propósitos incluem:</p>
        
        <ul>
            <li class="listaTermos">	Fornecer nossos conteúdos e serviços aos usuários, incluindo a exibição alterada do conteúdo e propaganda;</li>
            <li class="listaTermos">	Verificação, pesquisa e análise a fim de manter, proteger e melhorar nossos serviços;</li>
            <li class="listaTermos">	Certificar-se do funcionamento técnico da nossa rede;</li>
            <li class="listaTermos">	Desenvolver novos serviços.</li>
        </ul>    
        
        <h2 class="hTermos">5.0 Opções para informações pessoais</h2>
        <p class="paragrafoTermos">Ao se cadastrar no Vestibulário, pediremos certas informações pessoais, estas que serão utilizadas de forma que discordará de nossa Política de Privacidade. Portanto, solicitaremos que você concorde com tal uso, a fim de evitar complicações.</p>
        <h2 class="hTermos">6.0 informações compartilhadas </h2>
        <p class="paragrafoTermos">Só será compartilhada informações pessoais com outras companhias ou indivíduos fora do nosso website nas condições conforme se seguem:</p>
        
        <ul>
            <li class="listaTermos">	Quando tivermos seu consentimento.  Requeremos a anuência opcional para compartilhar de qualquer informação pessoal.</li>
            <li class="listaTermos">	Forneceremos as informações aos nossos websites associados ou negócios com a finalidade de processar as informações pessoais em nosso nome. Exigimos que os mesmos concordem em processar informações com base em nossas </li>
            <li class="listaTermos">	É necessário que o acesso, utilização, preservação, afastamento ou revelação de tais informações seja feito para atender qualquer lei aplicável, regulamento, processo, legal ou solicitação factível governamental, (b) termos de serviço aplicável incluindo investigações de violação em potencial desse documento, (c) detectar, prevenir ou de outra forma endereços fraudulentos, segurança ou questões técnicas ou (d) proteger contra dados iminentes aos direitos, propriedades ou segurança do Vestibulário.</li>
        </ul>  
        
        <h2 class="hTermos">7.0 Alterações na política</h2>
        <p class="paragrafoTermos">A Política de Privacidade pode ser mudada a qualquer momento. Não reduziremos os seus direitos sob essa Política sem o consentimento explícito, esperando que essas mudanças sejam secundárias. Sendo assim, postaremos qualquer tipo de alteração nessa página, qualquer alteração a essa Política, se as alterações forem significativas, providenciaremos um aviso mais proeminente (por via de e-mails notificaremos as alterações). </p>
        <p class="paragrafoTermos">Se houver qualquer dúvida em relação a nossa Política sugerimos que entre em contato conosco através do e-mail vanguart.provedor@gmail.com.</p>
        </div>
</main>