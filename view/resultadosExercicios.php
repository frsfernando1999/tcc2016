<?php
$caminho = $_SERVER['PHP_SELF'];
if (stripos($caminho, 'view')) {
    header("location: ../index.php?p=exercicios");
}
//Página com restrição
require_once 'util/ValidarAcesso.class.php';
ValidarAcesso::verificarPermissao(array(0 => 1, 1 => 2, 2 => 3));

require_once 'core/RespQuestoes.class.php';
require_once 'core/ListaExercicios.class.php';
$cod_usuario = $_SESSION['cod_usuario'];
$codLista = $_GET['lista'];
$respostas = new RespQuestoes();
$total = $respostas->select("and questoes.lista_exercicios_cod_lista='$codLista' and resp_questoes.cod_usuario='$cod_usuario'");
$i = 1;
$u = 0;
        foreach ($total as $key => $resposta) {
            if ($resposta['resp_user'] == $resposta['cod_resp']) {
                $u++;
        }}
?>
<main class="conteudo" id="conteudo_perfil">
    <table class="tabelaResp" border="1px">
        <tr> <?php echo "<th id=\"mostraAcertos\" colspan=\"3\">Você acertou $u questões.</th>"; ?></tr>       
        <tr>

            <th class="linhaResp">Exercicio</th>
            <th class="linhaResp">Sua resposta</th>
            <th class="linhaResp">Resposta certa</th>
        </tr>
        <?php
        //var_dump($total);
        foreach ($total as $key => $resposta) {
            if ($resposta['resp_user'] == $resposta['cod_resp']) {
                echo " <tr class=\"linhaResp\">
            <td>";
                echo $i++;
                echo "</td>
            <td class=\"respCerta\">" . $resposta['resp_user'] . "</td>
            <td class=\"respCerta\">" . $resposta['cod_resp'] . "</td>
                </tr>
                
        ";
            } elseif ($resposta['resp_user'] != $resposta['cod_resp']) {
                echo " <tr class=\"linhaResp\">
            <td>";
                echo $i++;
                echo "</td>
            <td class=\"respErrada\">" . $resposta['resp_user'] . "</td>
            <td class=\"respErrada\">" . $resposta['cod_resp'] . "</td>
                </tr>
        ";
            }
        }
        ?>
        <tr>
            <td colspan="3" class="linhaResp"><a id="linkVoltaResposta" href="" onClick="history.go(-1)">Voltar a lista</a></td>
        </tr>
    </table>
</main>

