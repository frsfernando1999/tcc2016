<?php
$caminho = $_SERVER['PHP_SELF'];
if (stripos($caminho, 'view')) {
    header("location: ../index.php?p=exercicios");
}
//Página com restrição
require_once 'util/ValidarAcesso.class.php';
ValidarAcesso::verificarPermissao(array(0 => 1, 1 => 2, 2 => 3));

require_once 'core/RespQuestoes.class.php';
require_once 'core/ListaExercicios.class.php';
$cod_usuario = $_SESSION['cod_usuario'];
$codLista = $_GET['lista'];
$respostas = new RespQuestoes();
$total = $respostas->selectExDiss("and questoes.lista_exercicios_cod_lista='$codLista' and resp_questoes.cod_usuario='$cod_usuario'");
$i = 1;
?>

<main class="conteudo" id="respostaDiss">
    <?php
    foreach ($total as $key => $resposta) {
        echo '<div class="respostaExEspaco">';
        echo "<section class=\"enunciadoExercicioResult\">" . "<h2 class=\"tituloRespostaDiss\">Questão " . $i++ . "</h2>" . $resposta['enunciado_ex_obj'] . "</section>";
        echo "<section class=\"respUsuario\">" . "<h2 class=\"tituloRespostaDiss\">Resposta</h2>";
        echo $resposta['resp_user'];
        echo "</section>";
        echo "<section class=\"respCerta\">" . "<h2 class=\"tituloRespostaDiss\">Correção</h2>";
        echo $resposta['obs_questao'];
        echo "</section>";
        echo '</div>';
    }
    ?>

    <div class="linhaRespDiss" ><a id="linkVoltaRespostaDiss" class="linhaRespDiss" href="" onClick="history.go(-1)">Voltar a lista</a></div>
</main>

