<?php
$caminho = $_SERVER['PHP_SELF'];
if (stripos($caminho, 'view')) {
    header("location: ../index.php?p=simulado");
}
require_once 'util/ValidarAcesso.class.php';
ValidarAcesso::verificarPermissao(array(0 => 1, 1 => 2, 2 => 3));
require_once 'core/ListaExercicios.class.php';
$lista = new ListaExercicios();
$row = $lista->selectInner("and tipo_lista = 'S' and status_exclusao_lista='1'");
?>

<main class="conteudo" id="conteudo_listas_exercicios">

    <div class="tituloTopicoEx" id="tituloTopicoSimulado">
        <h1>Simulados</h1>
    </div>

    <div id="colunaTopicoSimulado">
        <?php
        foreach ($row as $lista) {
            echo "<section id=\"espacoSimuladoTopicos\"><a id=\"topicoSimulado\"  href=\"index.php?p=simuladoLista&tipo=" . $lista['tipo_lista'] . "&lista=" . $lista['cod_lista'] . "\">
            <p class=\"linhaTopicoEx\">" . $lista['titulo_lista'] . "
                </section></a>";
        }
        ?>
    </div>
</main>
