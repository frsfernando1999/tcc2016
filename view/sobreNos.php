<?php
$caminho = $_SERVER['PHP_SELF'];
if (stripos($caminho, 'view')) {
    header("location: ../index.php?p=sobreNos");
}
require_once 'util/ValidarAcesso.class.php';
ValidarAcesso::verificarPermissao(array(0 => 1, 1 => 2, 2 => 3));
?>

<main class="conteudo" id="sobreNos">
    <h1 id="tituloSobreNos">Equipe Vanguart</h1>
    
    <div id="posicaoImgGrupo">
    <img src="imagem/fefe.jpg" class="img_grupo" alt=""/>
    <h3 class="nomeGrupo">Fernando Sousa</h3>
    <h3 class="funcaoGrupo">Back-end</h3>
    </div>
    <div id="posicaoImgGrupo">
    <img src="imagem/inara.jpg" class="img_grupo" alt=""/>
    <h3 class="nomeGrupo">Inara Fernandes</h3>
    <h3 class="funcaoGrupo">Monografia</h3>
    </div>
    <div id="posicaoImgGrupo">
    <img src="imagem/jack.jpg" class="img_grupo" alt=""/>
    <h3 class="nomeGrupo">Jaqueline Estevo</h3>
    <h3 class="funcaoGrupo">Monografia</h3>
    </div>
    <div id="posicaoImgGrupo">
    <img src="imagem/ju.jpg" class="img_grupo" alt=""/>
    <h3 class="nomeGrupo">Juliana Damasceno</h3>
    <h3 class="funcaoGrupo">Front-end</h3>
    </div>
    <div id="posicaoImgGrupo">
    <img src="imagem/mh.jpg" class="img_grupo" alt=""/>
    <h3 class="nomeGrupo">Mateus Henrique</h3>
    <h3 class="funcaoGrupo">Front-end</h3>
    </div>
    <div id="posicaoImgGrupo">
    <img src="imagem/grell.jpg" class="img_grupo" alt=""/>
    <h3 class="nomeGrupo">Matheus M. Grell</h3>
    <h3 class="funcaoGrupo">Back-end</h3>
    </div>
    <div id="posicaoImgGrupo">
    <img src="imagem/nick.jpg" class="img_grupo" alt=""/>
    <h3 class="nomeGrupo">Nicoli Yope</h3>
    <h3 class="funcaoGrupo">Monografia</h3>
    </div>
    <div id="posicaoImgGrupo">
    <img src="imagem/rafa.jpg" class="img_grupo" alt=""/>
    <h3 class="nomeGrupo">Rafael Perin</h3>
    <h3 class="funcaoGrupo">Front-end</h3>
    </div>
    
    <div id="divTextoSobreNos">
    <p class="textoSobreNos">
        Este site foi criado como Trabalho de Conclusão de Curso no curso técnico de Informática para Internet
        do colégio ITB Maria Sylvia Chaluppe Mello, pela equipe Vanguart. Para tal, a equipe realizou um processo
        de desenvolvimento que começou no engajar da equipe pelo campo das ideias, pelos rascunhos do projeto, até
        a devida codificação do website e programação. Fizemos uso de programas e recursos utilizados por diversos
        desenvolvedores web e profissionais da área. O site foi finalizado no período de um ano, contando com
        auxílio de professores e técnicos de web design. Além do site em si, disponível online, também foi desenvolvido
        a monografia, que, seguindo as normas da abnt, apresenta conceitos importantes e múltiplas informações 
        necessárias para o maior compreendimento da equipe e de todo processo de realização do projeto. 
    </p>
    <p class="textoSobreNos" id="paragrafo2SobreNos">
        O objetivo do site Vestibulário é a criação de uma plataforma de estudos que possibilite a interação entre
        o estudante e o professor, que estará lá para tirar suas dúvidas e ajudá-lo em seu treino para o vestibular.
        O site provém uma vasta gama de conteúdo e informação, tudo para guiar o vestibulando em sua jornada para
        entrar na sonhada universidade. Apesar de um trabalho de conclusão de curso, o Vestibulário visa ser uma
        das mais úteis plataformas de uso jovem e a integração com o professor, que constitue uma das características
        únicas do site.
    </p>
    </div>
</main>