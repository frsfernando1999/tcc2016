<?php
$caminho = $_SERVER['PHP_SELF'];
if (stripos($caminho, 'view')) {
    header("location: ../index.php?p=termos");
}

//Página com restrição
require_once 'util/ValidarAcesso.class.php';
ValidarAcesso::verificarPermissao(array(0 => 1, 1 => 2, 2 => 3));

?>

<main class="conteudo" id="termos">
    <div id="termosDeUso">
        <h1 class="hTermos" id="tituloTermos">Termos de uso</h1>
    <h2 class="hTermos">1.0 Aceitações dos termos de uso.</h2>
    <p class="paragrafoTermos">O site www.vestibulario.com.br, operado pela empresa VANGUART, fornece ao usuário o acesso e uso 
        dos serviços e conteúdos disponibilizados, que estão sujeitos à aceitação dos termos e condições estabelecidos neste documento.</p>
    <p class="paragrafoTermos">Reservamos o direito de modificações e exclusões, temporariamente ou permanentes, com ou sem aviso prévio. 
        Em caso de dúvidas entre em contato pelo e-mail vanguart.provedor@gmail.com.</p>
    <h2 class="hTermos">2.0 Modificações dos termos de uso.</h2>
    <p class="paragrafoTermos">Reservamos o direito de alterar a qualquer momento esse documento sem avisos prévios. 
        As alterações que porventura sejam realizadas entrarão em vigor após a publicação nesta página. 
        O seu acesso e utilização do conteúdo deste website será considerado um “concordar” dos novos termos de uso. 
        Caso não concorde com alguma cláusula, entre em contato conosco.</p>
    <h2 class="hTermos">3.0 Propriedade intelectual.</h2>
    <p class="paragrafoTermos">O conteúdo geral do site Vestibulário pertence apenas à empresa Vanguart.
        Não é autorizado o plágio ou qualquer outro tipo de cópia. Caso o usuário não respeite as normas o mesmo receberá um processo, 
        pertencente à lei 9.610 de 1998.   </p>
    <h3 class="hTermos">3.1 Licença de uso limitado:</h3>
    <h3 class="hTermos">3.1.1 Tutor: </h3>
    <p class="paragrafoTermos">O usuário/tutor terá permissão de fazer modificações 
        no site se for relacionado a conteúdos, atividades e vídeo aulas, 
        porém as postagens serão avaliadas pelo site e assim liberadas na rede;</p>
    <h3 class="hTermos">3.1.2 Estudante:</h3>
    <p class="paragrafoTermos">O usuário/estudante só poderá ter acesso ao conteúdo fornecido pelo site ou tutores, não tendo então acesso a modificações.</p>
    <h2 class="hTermos">4. Marca:</h2>
    <p class="paragrafoTermos">O nome “Vestibulário” e logo está vinculado a empresa Vanguart, 
        e não está autorizado a ser utilizado por terceiros, a menos que tenha 
        algum vínculo direto com a empresa. </p>
    <h2 class="hTermos">5. Violação de direitos autorais:</h2>
    <p class="paragrafoTermos">Se você acredita que qualquer conteúdo aqui disponibilizado 
        viole direitos autorais seu e de terceiros, por favor, nos comunique 
        imediatamente pelo endereço de e-mail vanguart.provedor@gmail.com para a averiguação
        e eventual remoção do conteúdo.</p>
    <h2 class="hTermos">6. Proibições:</h2>
    <p class="paragrafoTermos">Você não pode praticar as seguintes condutas ou utilizar o conteúdo desse site:</p>
    <ul>
        <li class="listaTermos">	De forma que viole qualquer lei vigente no Brasil e no país de onde o site é acessado;</li>
        <li class="listaTermos">	Enviar e-mails indesejados (“Spam”) em nome de nossa empresa Vanguart ou Vestibulário;</li>
        <li class="listaTermos">	Se passar por funcionário da empresa seja qual for o motivo;</li>
        <li class="listaTermos">	Fazer coleta de informações por meio de software automatizados;</li>
        <li class="listaTermos">	Criação de perfis fakes (falsos);</li>
        <li class="listaTermos">	Tentar obter acesso à uma parte restrita do site sem autorização;</li>
        <li class="listaTermos">	Realizar ataques de negação de serviço contra qualquer servidor ou rede utilizada por esse site;</li>
        <li class="listaTermos">	Não interferir no funcionamento do site.</li>
    </ul>
    </div>
</main>

