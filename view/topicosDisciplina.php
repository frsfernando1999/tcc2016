<?php
$caminho = $_SERVER['PHP_SELF'];
if (stripos($caminho, 'view')) {
    header("location: ../index.php?p=topicosDisciplina");
}
//Página com restrição
require_once 'util/ValidarAcesso.class.php';
ValidarAcesso::verificarPermissao(array(0 => 1, 1 => 2, 2 => 3));

require_once "core/TopicoDisciplina.class.php";

$topicodisciplina = new TopicoDisciplina;
$disc = isset($_GET['disc'])?$_GET['disc']: ""  ;
$row = $topicodisciplina->selectInner("and disciplinas.cod_disc=$disc", " order by ramo_disciplina.cod_ramo desc");
$ramos = $topicodisciplina->selectRamo("and disciplinas.cod_disc='$disc'", "group by nome_ramo");
?>

<main class="conteudo" id="conteudo_topico">
    <form class="formBusca" method="get" action="">
        <label id="tituloBusca">Buscar</label>
        <input type="text" id="consulta" placeholder="Pesquisa..." value=" " name="consulta" maxlength="255"/>
        <input type="hidden" id="consulta" name="p" maxlength="0" value="topicosDisciplina"/>
            <?php echo "<input type=\"hidden\" id=\"consulta\" name=\"disc\" maxlength=\"0\" value=\"$disc\"/>"; ?>
        <select class="selecionaPesquisa" name="ramo">
            <option value="">Selecionar um ramo</option>
            <?php
            foreach ($ramos as $ramo) {
                echo "<option class=\"optionSelect\" value=\"" .  $ramo['cod_ramo'] . "\">" .  $ramo['nome_ramo'] . "</option>";
            }
            ?>
        </select>
        <input type="submit" id="buscar" value="Buscar" />
         <label id="buscaEnvia" for="buscar">Pesquisar</label>
    </form>

    <?php
    $url = $_SERVER['REQUEST_URI'];

    if (!stripos($url, 'consulta')) {

        foreach ($row as $topico) {
            echo "<a href=\"?p=conteudo&topico=" . $topico['cod_topico'] . "\"><article class=\"topico\">
          <img src=\" " . $topico['url_arq'] . "\" class=\"imagemTopico\" alt=\" " . $topico['legenda_arq'] . "\">
          <h3 class=\"tituloTopico\">" . $topico['nome_topico'] . " </h3>
          <p class=\"textoTopico\">" . $topico['apresentacao_topico'] . "</p>
          </article></a>";
        }
    } elseif (stripos($url, 'consulta')) {
        if ($_GET['consulta'] == '') {
            echo "<script>window.location='index.php?p=topicosDisciplina&disc=$disc'</script>";
        } elseif ($_GET['ramo'] == '') {
            $discipl = filter_input(INPUT_GET, 'disc', FILTER_SANITIZE_STRING);
            $busca = filter_input(INPUT_GET, 'consulta', FILTER_SANITIZE_STRING);
            $total = $topicodisciplina->selectInner(" and  (topico_disciplina.texto_topico like '%$busca%' or nome_topico like '%$busca%') and ramo_disciplina.disciplinas_cod_disc='$discipl' and disciplinas.cod_disc='$discipl'", "order by nome_topico desc");
            foreach ($total as $topico) {
                echo "<a href=\"?p=conteudo&topico=" . $topico['cod_topico'] . "\"><article class=\"topico\">
          <img src=\" " . $topico['url_arq'] . "\" class=\"imagemTopico\" alt=\" " . $topico['legenda_arq'] . "\">
          <h3 class=\"tituloTopico\">" . $topico['nome_topico'] . " </h3>
          <p class=\"textoTopico\">" . $topico['apresentacao_topico'] . "</p>
          </article></a>";
            }
        } else {
            $busca = filter_input(INPUT_GET, 'consulta', FILTER_SANITIZE_STRING);
            $discipli = filter_input(INPUT_GET, 'disc', FILTER_SANITIZE_STRING);
            $ramo = filter_input(INPUT_GET, 'ramo', FILTER_SANITIZE_STRING);            
            $total = $topicodisciplina->selectInner(" and ( topico_disciplina.texto_topico like '%$busca%' or nome_topico like '%$busca%') and ramo_disciplina.disciplinas_cod_disc='$discipli' and ramo_disciplina.cod_ramo='$ramo'", "order by nome_topico desc");
            foreach ($total as $topico) {
                echo "<a href=\"?p=conteudo&topico=" . $topico['cod_topico'] . "\"><article class=\"topico\">
          <img src=\" " . $topico['url_arq'] . "\" class=\"imagemTopico\" alt=\" " . $topico['legenda_arq'] . "\">
          <h3 class=\"tituloTopico\">" . $topico['nome_topico'] . " </h3>
          <p class=\"textoTopico\">" . $topico['apresentacao_topico'] . "</p>
          </article></a>";
            }
        } if (empty($total)) {
            echo "Não houve nenhum resultado para o termo " . $busca;
        }
    }
    ?>

</main>
	