<?php
$caminho = $_SERVER['PHP_SELF'];
if (stripos($caminho, 'view')) {
    header("location: ../index.php?p=usuariosCadastrados");
}
require_once 'util/ValidarAcesso.class.php';
ValidarAcesso::verificarPermissao(array(0 => 3));

require_once 'core/Usuario.class.php';
$usuario = new Usuario();
$row=$usuario->select('order by cod_usuario');
?>

<main class="conteudo" id="conteudo_usuarios_cadastrados">
    <table border="1px solid black;">
        <tr>
            <th class="someTabelaUsuario">Código</th>
            <th>Nome</th>
            <th>Email</th>
            <th>Senha</th>
            <th class="someTabelaUsuario">Sexo</th>
            <th class="someTabelaUsuario">Foto</th>
            <th class="someTabelaUsuario">Data de Nascimento</th>
            <th>Acesso</th>
            <th>CPF</th>
            <th class="someTabelaUsuario">Tipo de Usuõrio</th>
            <th>Ativo</th>
            <th>Ações</th>
        </tr>
        <?php
        foreach ($row as $user){
            echo "<tr class=\"userAdm\">
            <td class=\"someTabelaUsuario\" class=\"userAdm\">" . $user['cod_usuario'] . "</td>
            <td class=\"userAdm\">" . $user['nome_usuario'] . " " .  $user['sobrenome_usuario'] .  "</td>
            <td class=\"userAdm\">" . $user['email_usuario'] . "</td>
            <td class=\"userAdm\">" . $user['senha_usuario'] . "</td>
            <td class=\"someTabelaUsuario\" class=\"userAdm\">" . $user['sexo_usuario'] . "</td>
            <td class=\"someTabelaUsuario\" class=\"userAdm\"><img class=\"imgTabelaUser\"src=\"" . $user['url_foto_usuario'] . "\"></td>
            <td class=\"someTabelaUsuario\" class=\"userAdm\">" . $user['data_nasc_usuario'] . "</td>
            <td class=\"userAdm\">" . $user['acesso_usuario'] . "</td>
            <td class=\"userAdm\">" . $user['cpf_usuario'] . "</td>
            <td class=\"someTabelaUsuario\" class=\"userAdm\">" . $user['tipo_usuario'] . "</td>
            <td class=\"userAdm\">" . $user['ativo_usuario'] . "</td>";
            if ($user['ativo_usuario']==1) {
                echo "<td class=\"userAdm\"><a href=\"index.php?p=deletarUsuario&codUsuario=" . $user['cod_usuario'] . "\">Desativar</a></td>
        </tr>";
            }elseif($user['ativo_usuario']!=1) {
                echo "<td class=\"userAdm\"><a href=\"index.php?p=deletarUsuario&codUsuario=" . $user['cod_usuario'] . "\">Reativar</a></td>
        </tr>";
    }
        }?>
    </table>
</main>

