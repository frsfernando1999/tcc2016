<?php
$caminho = $_SERVER['PHP_SELF'];
if (stripos($caminho, 'view')) {
    header("location: ../index.php?p=vestibulares");
}
require_once 'util/ValidarAcesso.class.php';
ValidarAcesso::verificarPermissao(array(0 => 1, 1 => 2, 2 => 3))
?>
<main class="conteudo" id="conteudo_vestibular">
    <article id="enem">
        <article class="parteTitulo">
            <h2 class="tituloVestibular">ENEM</h2>
        </article>

        <h3 id="abrirMaterial1" onclick="toggle_visibility('parteLinks');">Material de Estudos</h3>
        <article id="parteLinks" class="menu1">
   

<h2>Português</h2>
<p>Gramática</p>
<p>Literatura</p>
<p>Interpretação de Texto</p>
<p>Redação</p>
<br>

<h2>História</h2>
<p>História Geral</p>
<p>História do Brasil</p>
<br>

<h2>Geografia</h2>
<p>Cartografia</p>
<p>Astronomia</p>
<p>Geografia Física</p>
<p>Geologia e Relevo</p>
<p>Clima</p>
<p>Hidrografia</p>
<p>Vegetação</p>
<p>Extração e Fontes de Energia</p>
<p>Agropecuária</p>
<p>População</p>
<p>Urbanização</p>
<p>Sistemas Econômicos</p>
<p>A Indústria</p>
<p>O Mundo Pós-Guerra</p>
<p>Estado-Nação</p>
<p>Globalização e Desenvolvimento</p>
<p>Comunicações e Transportes</p>
<p>Meio Ambiente</p>
<p>África</p>
<p>Ásia</p>
<p>Oriente Médio</p>
<p>Geografia Física do Brasil</p>
<p>As Regiões do Brasil</p>
<p>População do Brasil</p>
<p>Energia e Minerais</p>
<p>Industrialização Brasileira</p>
<p>Urbanização Brasileira</p>
<p>Agropecuária do Brasil</p>
<p>Economia Brasileira</p>
<p>O Transporte no Brasil</p>
<p>Questões Ambientais</p>
<br>

<h2>Biologia</h2>
<p>Citologia</p>
<p>Anatomia e Fisiologia</p>
<p>Reprodução e Desenvolvimento</p>
<p>Sistema Imunitário</p>
<p>Nutrição e Digestão</p>
<p>A Respiração</p>
<p>A Circulação</p>
<p>Proteção, suporte e movimento</p>
<p>Seres Vivos</p>
<p>Classificação dos seres vivos</p>
<p>Reino Monera</p>
<p>Reino Protista</p>
<p>Reino Vegetal</p>
<p>Reino Animal</p>
<p>Parasitologia - doenças</p>
<p>Saúde</p>
<p>Genética
<p>Evolução</p>
<p>Ecologia</p>
<p>Meio Ambiente</p>
<br>

<h2>Matemática</h2>
<p>Conjuntos Numéricos - os Tipos de Números</p>
<p>Teoria dos Conjuntos</p>
<p>Aritmética</p>
<p>Expressões Numéricas</p>
<p>Potências</p>
<p>Raízes</p>
<p>Equações</p>
<p>Inequações</p>
<p>Razão, Proporção e Porcentagem</p>
<p>Tipos de Função</p>
<p>Função Quadrática</p>
<p>Funções Exponenciais e Logarítmicas</p>
<p>Progressão Aritmética e Geométrica</p>
<p>Probabilidade - Conceito e Exercícios</p>
<p>Análise Combinatória - Permutações, Arranjos e Combinações</p>
<p>Introdução aos Sistemas de Equações Lineares</p>
<p>Matrizes</p>
<p>Trigonometria</p>
<p>Gráficos de Retas em um Sistema Ortogonal de Coordenadas</p>
<p>Geometria Analítica</p>
<p>Geometria Plana</p>
<p>Geometria Espacial</p>
<p>Estatística</p>
<p>Raciocínio Matemático</p>
<br>

<h2>Física</h2>
<p>Introdução à Física</p>
<p>Mecânica - Cinemática</p>
<p>Mecânica - Dinâmica</p>
<p>Termologia</p>
<p>Corrente Elétrica</p>
<p>Eletrostática</p>
<p>Magnetismo</p>
<p>Optica</p>
<p>Ondulatória</p>
<br>

<h2>Química</h2>
<p>Estados físicos da matéria</p>
<p>Densidade</p>
<p>Substâncias Químicas</p>
<p>Separação de misturas químicas</p>
<p>Ar atmosférico</p>
<p>Atomística</p>
<p>Classificação periódica dos elementos</p>
<p>Ligações Químicas</p>
<p>Ligação Covalente, Molecular ou Homopolar</p>
<p>Forças Intermoleculares</p>
<p>Fórmulas Químicas</p>
<p>Massas Atômicas - Massas Moleculares - Mol</p>
<p>Funções Inorgânicas - Ácidos</p>
<p>Bases ou Hidróxidos de Arrhenius</p>
<p>Sais na Química</p>
<p>Oxidos</p>
<p>Potencial Hidrogeniônico (pH)</p>
<p>Leis Ponderais</p>
<p>Soluções</p>
<p>Diluição das Soluções</p>
<p>Solubilidade</p>
<p>Fenômenos Físicos e Químicos</p>
<p>Termoquímica - Entalpia</p>
<p>Cinética Química</p>
<p>Propriedades Coligativas</p>
<p>Equilíbrio Químico</p>
<p>Radioatividade</p>
<p>Eletroquímica</p>
<p>Eletrólise</p>
<p>Química Orgânica </p>
<p>Funções Orgânicas - Nomenclatura</p>
<p>Principais Funções Orgânicas</p>
<p>Ácidos Carboxílicos</p>
<p>Álcoois</p>
<p>Petróleo</p>
<p>Gás Metano</p>
<p>Polímeros</p>
<p>Isomeria Plana e Espacial</p>
<p>Teorias Modernas de Ácido e Base</p>
<p>Reações Orgânicas</p>
<p>Gases</p>
<p>Bioquímica: as proteínas e os aminoácidos</p>
<br>

<h2>Filosofia</h2>
<p>Introdução à Filosofia</p>

        </article>

        <h3 id="abrirMaterial2" onclick="toggle_visibility('parteConteudoSobreExame');">Sobre o exame</h3>
        <article id="parteConteudoSobreExame" class="menu2" style="display:none;">

            <p>O Exame Nacional do Ensino Médio, instituido pelo MEC em 1998, ganhou uma grande importância em 2009 com a criação do 
                SiSU. A partir desta data, a prova vem ajudando cada vez mais alunos a ingressar na faculdade desejada. O ENEM acontece
                anualmente, normalmente no mês de outubro ou novembro, em todo o Brasil e não é obrigatório.</p>
<br>

            <h2>Acesso ao ensino superior</h2>            
            <p>Universidades públicas:<br>
                O Sistema de Seleção Unificada(SiSU) utiliza da nota do ENEM para garantir vagas no ensino superior público. Através
                desse programa, surgem oportunidades para que estudantes de qualquer estado do país possam ingressar em universidades
                ou institutos federais de qualquer região. Existem certas faculdades federais que, mesmo não participando do SiSU, 
                utilizam parcialmente da nota do ENEM para selecionar universitários.</p>
<br>

            <p>Universidades particulares:<br>
                O Programa Universidade para Todos(ProUni) foi criado em 2004 para tornar possível que alunos utilizassem da nota do ENEM
                para obter bolsas integrais ou parciais em universidades particulares. Com o passar do tempo, cada vez mais bolsas integrais
                são fornecidas aos estudantes, chegando a um total de 69% das bolsas.</p>
<br>

            <h2>Certificação do Ensino Médio</h2>
            <p>Desde 2012, jovens maiores de 18 que não concluiram o ensino médio podem obter a certificação de conclusão do mesmo
                utilizando da nota do ENEM. Segundo dados do MEC, 780 mil inscritos prestaram o exame com o objetivo de concluir o 
                estágio médio de ensino.</p>
<br>

            <h2>Intercâmbio</h2>
            <p>O programa Ciência sem Fronteiras foi implementado em 2011, com o objetivo de inserir alunos brasileiros em diversas
                universidades de outros países. O programa usa da nota do ENEM como requisito para participar. É uma ótima oportunidade
                para universitários que planejam seguir o ramo da ciência e pesquisa.</p>
<br>

            <h2>Sobre o teste</h2>
            <p>O ENEM é composto de 180 questões de múltipla escolha com cinco alternativas. Estas questões são divididas em quatro
                nichos de conhecimento: Ciências da Natureza, Ciências Humanas, Linguagens e Matemática, cada um com 45 questões.</p>
            <p>O exame também conta com uma redação, que deve ser escrita como um texto dissertativo-argumentativo, sobre um tema
                relacionado à cultura, política, sociedade ou ciência.</p>
            <p>As provas são sempre aplicadas em dois dias(sábado e domingo), sendo o 1º dia realizada as áreas de Ciências da Natureza
                e Ciências Humanas, enquanto no 2º dia são feitas Linguagens, Matemática e a redação.</p>
<br>

            <h2>Inscrição</h2>
            <p>A inscrição para o ENEM é feita pelo site do mesmo, em um período determinado pelo Inep. Qualquer pessoa de qualquer
                parte do Brasil pode realizar o ENEM. Até pessoas em reclusão, porém estas tem a prova feita em unidades prisionais em
                uma data diferente da prova padrão.</p>
            <p>Para mais informações sobre o ENEM, acesse: <a href="http://enem.inep.gov.br/">www.enem.inep.gov.br</a></p>
        </article>

        <h3 id="abrirMaterial3" onclick="toggle_visibility('cursos');">Cursos</h3>
        <article id="cursos" class="menu3" style="display:none;">
            <p>O vestibular do ENEM possibilita o acesso a uma enorme quantidade de universidades em todo o Brasil. Para descobrir se a sua faculdade desejada aceita nota do ENEM ou conferir a lista completa de faculdades, acesse: <a href="http://www.mundovestibular.com.br/articles/127/1/Faculdades-que-aceitam-a-nota-do-Enem/Paacutegina1.html">www.mundovestibular.com.br</a></p>
        </article>
    </article>

    <article id="enem">
        <article class="parteTitulo">
            <h2 class="tituloVestibular">FUVEST</h2>
        </article>

        <h3 id="abrirMaterial1" onclick="toggle_visibility('parteLinks2');">Material de Estudos</h3>
        <article id="parteLinks2" class="menu1">
   
<h2>História</h2>
<p>Grandes civilizações: Egito, Grécia, Roma</p>
<p>Feudalismo</p>
<p>Guerras Mundiais</p>
<p>História do Brasil (da colonização ao cenário atual)</p>
<p>Política (história e contextos mundiais)</p>
<br>

<h2>Biologia</h2>
<p>Botânica e Ecologia</p>
<p>Estrutura e Funções da Célula</p>
<p>Corpo Humano (geral)</p>
<p>Genética e Evolução</p>
<p>Microbiologia</p>
<br>

<h2>Física</h2>
<p>Dinâmica (Movimento, Leis de Newton)</p>
<p>Termodinâmica</p>
<p>Eletrodinâmica</p>
<p>Estática</p>
<p>Ondulatória</p>
<br>

<h2>Química</h2>
<p>Química Orgânica</p>
<p>Cálculo Estequiométrico</p>
<p>Química Inorgânica</p>
<p>Tabela Periódica</p>
<p>Equilíbrio Químico</p>
<br>

<h2>Matemática</h2>
<p>Geometria Plana e Analítica</p>
<p>Funções  (Polinômios, Funções de 1 e 2 graus)</p>
<p>Combinatória, Probabilidade e Estatística</p>
<p>Progressões</p>
<p>Trigonometria</p>
<br>

<h2>Geografia</h2>
<p>Geografia Geral</p>
<p>Geopolítica</p>
<p>Meio Ambiente e Atividade Econômica</p>
<p>Geografia Brasileira</p>
<p>População</p>
<br>

<h2>Língua Portuguesa</h2>
<p>Coerência e Coesão</p>
<p>Concordância Verbal e Nominal</p>
<p>Interpretação de texto</p>
<p>Intertextualidade</p>
<p>Gramática e Ortografia</p>
<br>

<h2>Literatura</h2>
<p>Modernismo</p>
<p>Romantismo</p>
<p>Realismo</p>
<p>Naturalismo</p>
<br>

<h2>Redação (apenas na segunda fase)</h2>
<p>Estrutura do texto dissertativo</p>
<p>Norma culta da língua portuguesa</p>
<p>Argumento</p>
<p>Recursos de texto</p>
<br>

<h2>Inglês ou Espanhol</h2>
<p>Interpretação de texto</p>
<p>Vocabulário</p>
<p>Gramática</p>
<br>

<h2>Obras de leitura obrigatória</h2>
<p>A cidade e as serras (Eça de Queirós)</p>
<p>Capitães da areia (Jorge Amado)</p>
<p>Memórias de um sargento de milícias (Manuel Antônio de Almeida)</p>
<p>Memórias póstumas de Brás Cubas (Machado de Assis)</p>
<p>O cortiço (Aluísio Azevedo)</p>
<p>Sentimento do mundo (Carlos Drummond de Andrade)</p>
<p>Til (José de Alencar)</p>
<p>Viagens na minha terra (Almeida Garrett)</p>
<p>Vidas secas (Graciliano Ramos)</p>

        </article>

        <h3 id="abrirMaterial2" onclick="toggle_visibility('parteConteudoSobreExame2');">Sobre o exame</h3>
        <article id="parteConteudoSobreExame2" class="menu2" style="display:none;">
            
            <p>O exame da Fundação Universitária para o Vestibular(Fuvest) é um dos vestibulares mais importantes do Brasil.
            Seleciona alunos para estudar na Universidade de São Paulo(USP) e na Faculdade de Ciências Médicas da Santa Casa de 
            São Paulo(FCMSC-SP).</p>
            <br>

            <h4>Sobre o teste</h4>
            <p>A Fuvest é dividida em duas fases, separadas em quatro dias.</p>
            <p>1ª fase:<br>
            A primeira fase é composta por uma prova de 90 questões objetivas, cinco opções em cada questão. Esta fase abrange
            oito disciplinas: biologia, química, língua portuguesa, matemática, história, física, geografia e inglês.</p>
            <p>2ª fase:<br>
            A segunda fase conta com três provas, cada uma em um dia diferente. As respostas devem ser todas dissertativas, 
            escritas obrigatoriamente com caneta azul ou preta. Confira o conteúdo de cada um dos dias:</p>
            <p>1º dia: Possui duas partes, a primeira contendo 10 questões dissertativas de Língua Portuguesa, isso inclui:
            interpretação de textos, gramática e literatura. A segunda parte é uma redação com tema secreto.</p>
            <p>2º dia: Este dia possui 16 questões dissertativas de diversas disciplinas: biologia, geografia, história, química, 
            física, inglês e matemática.</p>
            <p>3º dia: O terceiro dia tem 12 questões dissertativas sobre duas ou três disciplinas. A quantidade e quais
            disciplinas serão exigidas depende da carreira escolhida pelo candidato.</p>
            <br>

            <p>Para mais informações sobre a Fuvest, acesse: <a href="http://www.fuvest.br/">www.fuvest.br</a></p>
               
        </article>

        <h3 id="abrirMaterial3" onclick="toggle_visibility('cursos2');">Cursos</h3>
        <article id="cursos2" class="menu3" style="display:none;">
            
            <p>Ao se inscrever na Fuvest, você pode escolher dentre os seguintes cursos:</p>
            <br>

            <h4>Cursos da USP:</h4>
            
          
             <p>Administração</p>
             <p>Arquitetura e urbanismo</p>
             <p>Artes cênicas</p>
             <p>Artes visuais</p>
             <p>Astronomia</p>
             <p>Audiovisual</p>
             <p>Biblioteconomia</p>
           <p> Ciências agrárias</p>
           <p> Ciências Atuariais</p>
             <p>Ciências biológicas</p>
             <p>Ciências Biomédicas</p>
             <p>Ciências Contábeis</p>
             <p>Ciências da Computação</p>
             <p>Ciências da Natureza</p>
             <p>Ciências dos Alimentos</p>
             <p>Ciências Exatas</p>
             <p>Ciências Físicas e Biomoleculares</p>
             <p>Ciências Sociais</p>
             <p>Design</p>
            <p>Direito</p>
            <p> Economia</p>
            <p> Economia Empresarial e Controladoria</p>
             <p>Editoração</p>
             <p>Educação Física e Esporte</p>
            <p> Educomunicação</p>
            <p> Enfermagem</p>
            <p> Engenharia Aeronáutica</p>
            <p> Engenharia Agronômica</p>
            <p> Engenharia Ambiental</p>
            <p> Engenharia Bioquímica</p>
            <p> Engenharia Civil</p>
            <p> Engenharia de Alimentos</p>
           <p>  Engenharia de Biossistemas</p>
           <p>  Engenharia de Computação</p>
            <p> Engenharia de Materiais</p>
            <p> Engenharia de Materiais e Manufatura</p>
            <p> Engenharia de Minas</p>
            <p> Engenharia de Petróleo</p>
            <p> Engenharia de Produção</p>
             <p>Engenharia Elétrica</p>
             <p>Engenharia Física</p>
             <p>Engenharia Florestal</p>
            <p> Engenharia Mecânica</p>
             <p>Engenharia Mecatrônica</p>
            <p> Engenharia Metalúrgica</p>
            <p> Engenharia Naval</p>
            <p> Engenharia Química</p>
            <p> Estatística</p>
            <p> Farmácia-bioquímica</p>
            <p> Filosofia</p>
            <p> Física</p>
            <p> Física Computacional</p>
            <p> Física Médica</p>
            <p> Fisioterapia</p>
            <p> Fonoaudiologia</p>
            <p> Geociências e Educação Ambiental</p>
            <p> Geofísica</p>
            <p> Geografia</p>
            <p> Geologia</p>
             <p>Gerontologia</p>
             <p>Gestão Ambiental</p>
             <p>Gestão de Políticas Públicas</p>
            <p> História</p>
             <p>Informática Biomédica</p>
             <p>Jornalismo</p>
            <p> Lazer e Turismo</p>
            <p> Letras</p>
            <p> Marketing</p>
            <p> Matemática</p>
           <p> Matemática Aplicada</p>
            <p> Matemática Aplicada a Negócios</p>
             <p>Matemática Aplicada e Computação Científica</p>
             <p>Matemática Aplicada e Computacional</p>
            <p> Medicina</p>
            <p> Medicina Veterinária</p>
            <p> Meteorologia0</p>
            <p> Música</p>
             <p>Nutrição</p>
             <p>Nutrição e Metabolismo</p>
            <p> Obstetrícia</p>
            <p> Oceanografia</p>
             <p>Odontologia</p>
             <p>Pedagogia</p>
            <p> Psicologia</p>
            <p> Publicidade e Propaganda</p>
            <p> Química</p>
            <p> Relações Internacionais</p>
            <p> Relações Públicas</p>
            <p> Saúde Pública</p>
            <p> Sistemas de Informação</p>
             <p>Terapia Ocupacional</p>
             <p>Têxtil e Moda</p>
            <p> Turismo</p>
            <p> Zootecnia</p>
           
            <br>

            <h4>Cursos da FCMSC-SP:</h4>
           
            <p>Medicina</p>
             <p>Enfermagem </p>
             <p>Fonoaudiologia</p>
            <p> Tecnologia em Radiologia</p>
             <p>Tecnologia em Sistemas Biomédicos</p>


        </article>
    </article>
</main>	